============================
Eclipse IDE Survival Guide
============================



The IDE provided with ModusToolbox software is built on the Eclipse IDE.
For those unfamiliar with an Eclipse IDE, this FAQ answers questions
about how to get common tasks done. Most information specific to
ModusToolbox (for example: How do I install or get started?) is in the
ModusToolbox documentation, especially the `Quick Start
Guide <http://www.cypress.com/ModusToolboxQSG>`__ and the `User
Guide <http://www.cypress.com/ModusToolboxUserGuide>`__.

If you are already familiar with an Eclipse IDE in general, you will
find some useful answers here on unique IDE features such as the Quick
Panel and how to build an application. However, most information unique
to the Eclipse IDE for ModusToolbox (such as the Project Creator and
Library Manager) is in the User Guide or other documentation.

Questions are organized into the following categories:

-  `Project Management <#project-management>`__

-  `Editor <#editor>`__

-  `Build System <#build-system>`__

-  `Program/Debug <#program-debug>`__

In this document, the term **"**\ Application\ **"** means deployable
firmware for the target hardware. To build an application, you require a
collection of one or more Eclipse projects.

A "Project\ **"** is a compilation unit. An application may consist of
several Eclipse projects but typically is a single project. 

Project Management
==================

For general information about how the Eclipse IDE for ModusToolbox
creates and implements projects, see the “Getting Started” chapter of
the `Eclipse IDE for ModusToolbox User
Guide <http://www.cypress.com/ModusToolboxUserGuide>`__\ *.*

Parts of the code example’s readme file are hard to read in the IDE. How do I fix that?
---------------------------------------------------------------------------------------

Cypress uses GitHub to deliver examples and libraries. We use
GitHub-flavored markdown, which has some extensions (like support for
tables) that are not understood by the Eclipse markdown editor. There
are a few things you can do. You can look at the readme file on GitHub.
You can right-click the document in the project explorer and choose
“Open With…” to pick a different reader application. You can get a more
capable markdown editor and install that. You can also modify the
Eclipse IDE to open a markdown document in that editor.

How do I set up the IDE to use my preferred markdown editor?
------------------------------------------------------------

If a document does not open the editor you want, use **Window** >
**Preferences** > **Editors** > **File Associations** and set up the
file association. For the \*.md file type, assign it to your preferred
markdown viewer. When you double-click a .md file in the Eclipse project
explorer, it opens in your external editor.

How do I find the right Eclipse panel or option when I don’t know where anything is?
------------------------------------------------------------------------------------

Use the Eclipse IDE Quick Access feature. This is a search engine that
helps you find things in an Eclipse IDE. The IDE is a complex
application with significant resources located in various menus, panels,
windows, and so forth. The Quick Access field solves the problem of not
knowing where things are.

Type a search term into the Quick Access field, and you are presented
with hits. Each item is a link. Clicking a link either executes the
command or takes you to the location in the Eclipse UI.

   |image0|

How do I rename an application?
-------------------------------

In the project explorer, right-click the project name and choose
**Rename**. However, launch configurations (used for programming and
debugging the target) are linked to the original project name, so they
disappear when you rename the project. See `My launch configurations
disappeared, what do I
do? <#my-launch-configurations-disappeared-what-do-i-do>`__.

How do I share a project?
-------------------------

With an Eclipse IDE, there are two ways to do this: via an archive file,
and from the file system.

-  Export an archive file: Select **File** > **Export** >\ **General** >
   **Archive File**. Select the project that represents the application.
   Send the archive to the person who needs the application. That person
   imports via **File** > **Import** > **General** >\ **Existing
   projects into workspace** and sets options to import the archive
   file.

-  If you have a folder that contains an Eclipse project, you can use
   **File** > **Import** > **General** > **Existing projects into
   workspace;** point to the root directory that contains the project(s)
   you want to import.

**Pro Tip**: When you archive a project, do not include the *build*
folder. The IDE regenerates this information, you don’t need to include
it. You will save many megabytes of space excluding the *build* folder.

|image1|

How do I create a new/empty workspace?
--------------------------------------

Choose **File** > **Switch Workspace** > **Other...** and then create
the new workspace. You can do this by just entering a new name, or
browsing and creating a folder.

How do I change workspaces?
---------------------------

Choose **File** > **Switch Workspace**, and then pick an
existing workspace.

What is a perspective and how does the IDE use them?
----------------------------------------------------

In Eclipse, a perspective is the collection of panes and views visible
in the user interface. The Eclipse IDE for ModusToolbox has its own
perspective, which is what appears when you launch the IDE for the first
time. This is called the ModusToolbox perspective. It contains views
that enable project management, code creation, and debugging all in the
same perspective. It also contains the Quick Panel, which contains
one-click shortcuts to the most common tasks. By default, the IDE does
not use the standard Eclipse C/C++ or Debug perspectives.

How can I change to a different perspective?
--------------------------------------------

There are at least three ways.

Select **Window** > **Perspective** > **Open Perspective** and choose
the one you want. You can also click the **Open Perspective** button in
the toolbar. The icon for any perspective you open appears as a button
on the toolbar.

|image2|

To the right of that is a button for each of the perspectives you have
used, such as the ModusToolbox perspective. Just click the button
corresponding to the perspective you want to use.

How can I make my own perspective?
----------------------------------

This is one of the most powerful features of an Eclipse IDE. You can
open, close, or relocate any view in any panel. In effect, you can build
your own UI. When the UI is set up to your liking, use **Window** >
**Perspective** > **Save Perspective As**. Give it a name, and it is
added to the list of available perspectives.

How do I remove or delete a project from the project explorer?
--------------------------------------------------------------

In the project explorer, select the project(s) you want to remove. Then
press the **Delete** key, or right-click and choose **Delete**. The
Delete Resources dialog appears.

To remove a project from the explorer, but keep the actual project, make
sure that **Delete project contents on disk** is **disabled**. This is
often the right thing to do, for example, when you point to a project
outside your workspace that should continue to exist.

To delete the project permanently, enable the **Delete project contents
on disk** option. Project deletion cannot be undone. This is often the
right thing to do when you want to permanently remove the project from
the workspace.

   |image3|

What are workspace settings?
----------------------------

Workspace settings are preferences that apply to the entire workspace.
In general, these are set by changing options in **Window >**
**Preferences**.

How do I share workspace settings across workspaces?
----------------------------------------------------

You can export preferences from one workspace and import them into
another. Select **File** > **Export** > **General** > **Preferences**.
This creates a preferences file. You can use the file as a backup of
your preferences. You can switch to any workspace and import the
preferences.

When creating a new workspace, expand the **Copy Settings** option and
specify which preferences from the current workspace you want to use in
the new workspace.

|image4|

What is a working set and why would I want to use one?
------------------------------------------------------

A working set is a collection of Eclipse projects. Fundamentally it is
just a group, but you can perform various operations that will affect
all the projects in the group. For example, you can show, hide, or build
a working set. This can make it easier to manage multiple related
projects.

How do I create a working set?
------------------------------

To make things easy, do the following:

1. In the project explorer, first select all the projects you want in the working set.

2. In the project explorer, use the **View** drop-down menu and choose **Select Working Set.**

|image5|

3. Click **New**, and then select **C/C++** and click **Next**. If you
   selected the projects first, they appear already selected in the
   **New Working Set** dialog. Otherwise, you can pick them one by
   one.

**Pro Tip:** In the **View** menu you can also make the working set the
top-level element. Only selected working sets are visible. You can see
projects that aren’t in the working set in the **Other Projects**
collection.

|image6|

How do I set up dependencies between projects?
----------------------------------------------

When working with a set of applications, you may wish to ensure that if
you build one, the projects it depends upon build first.

In the project explorer, right-click a project and choose
**Properties.** Click the **Project References** item. In the panel,
select the projects on which this project depends.

|image7|

What does it mean to close a project, and how do I do it?
---------------------------------------------------------

A closed project requires no resources and is not affected by any
workspace command (e.g., **Build All**).

To close projects, select them, right-click, and choose **Close**.

You can also select one you want to keep open, right-click, and choose
**Close Unrelated Projects**. Every project not related to your selected
project will close.

What is the difference between a local file and a linked file, and how do I tell which is which?
------------------------------------------------------------------------------------------------

A local file is a file that exists inside the project directory. It is
unique to the project that owns the file. Any change you make to this
file has local effect only. You are changing the file in the project.

A linked file is a file that the project refers to, but it does not
maintain a local copy. Most commonly, linked files are library source
files. However, it can be a file anywhere that is used by reference. The
project points to the original file and does not have a local copy. That
means, if you change the linked file, you change every project that uses
that file.

How can you tell which is which? This is a particularly subtle part of
the Eclipse UI. The icon for a linked file has a tiny arrow. Like this.

|image8|

How do I link to files instead of duplicating files in each project that uses them?
-----------------------------------------------------------------------------------

This is easy, but obscure. You can link to a single file, or an entire
folder full of files.

Use **File** > **New** > **Other,** select **Folder** (or file) and
click **Next**.

In the resulting dialog, specify the **parent folder** that will hold
the new linked folder in the Project explorer. Click **Advanced**. Then
Click **Link to alternate location (Linked Folder)**. Click the
**Browse** button and navigate to the folder you want to link to the
project. Instead of an absolute path, you can use path **Variables** to
specify the location. The folder name is set automatically, but you can
rename it whatever you want.

|image9|

Click **Finish** and you’re done. The project now has a link to the
folder and its contents. All the files in the folder appear, you do not
need to add them individually. If you change a file, any project that
uses that file is affected. In the project explorer it looks like this:

|image10|

The folder icon has a tiny arrow, which means this folder and its
contents are linked, and not in the project folder.

How do I launch multiple instances of Eclipse on macOS?
-------------------------------------------------------

By design, macOS allows one instance of an app. There is a way around
this from the terminal. This is a macOS issue, not an Eclipse problem.

From the terminal, do the following:

1. Navigate to the application directory, for example: cd
   /Applications/eclipse/

2. Open the app: open -n Eclipse.app &

The **&** immediately sends the app to the background, and you now have
two instances of Eclipse running.

Can I use an Eclipse IDE with a version control system?
-------------------------------------------------------

The short answer is yes. However, you need to be careful about what gets
checked in. The IDE does not restrict what it checks in. The IDE can add
everything in a project, not just the files you want. As a result way
too many files, including binaries, can get checked in. This can cause
grief.

Although the Eclipse egit plugin typically manages this automatically,
even then you can have problems. If the project builds with an error,
inappropriate files will be checked in when you add files to source code
control.

For starters, never commit the entire workspace into a version control
system. Any Eclipse IDE maintains workspace metadata that is easily
corrupted if you revert changes. So, do not check in your workspace, but
do check in some of the content in your project folder.

You do not need to check in any files created in the build process. For
example, anything in the final *Build* folder. You need to manage your
version control system to exclude these files. For example, ensure that
your .gitignore file lists any file or directory that should not be
included.

Editor
======

How do I show line numbers?
---------------------------

Select **Window** > **Preferences** > **General** > **Editors** > **Text
Editors** and find the **Show line numbers** option.

How do I search across all files in a project or workspace?
-----------------------------------------------------------

Use the **Search** menu. When the dialog opens, control the scope of the
search with the options near the bottom of the dialog.

How do I go from a file listed in the project explorer to the actual file in the file system?
---------------------------------------------------------------------------------------------

In the project explorer, right-click the file, choose **Properties**. In
the dialog, note the **Location** field. Click the button to the right
of the location.

   |image11|

I have multiple files of the same name open in the editor. How do I tell which project each one belongs to?
-----------------------------------------------------------------------------------------------------------

There are at least two ways to do this.

-  Hover over the tab in the editor. The project and path within the
   project to the file appear.

|image12|

-  Enable the **Link with Editor** option in the explorer pane. When
   this is enabled, the file with focus in the editor is highlighted in
   the project explorer.

..

   |image13|

Build System
============

How do I build an application?
------------------------------

In the project explorer, select the project. In the **Quick Panel**
click **Build <appname> Application**. You can also right click the
project and choose **Build Project**. If there are dependent projects
those build as well.

What does **Build** **All** build?
----------------------------------

The **Project** > **Build All** command builds every open project in a
workspace. You probably don’t want to use this command if you have more
than one application in a workspace.

In addition, open projects can be hidden in the project explorer. If you
hide an application and its projects (using working sets for example),
those projects will still be built if they are open.

How do I add middleware or a library to a project?
--------------------------------------------------

In the project explorer, select the project. Then in the **Quick**
**Panel**, click the **Library Manager** link. Click the **Libraries**
tab to see all available libraries. You can select the version you want,
as well as the library.

How do I set up an application to run on a different kit?
---------------------------------------------------------

In the project explorer, select the project. Then in the **Quick**
**Panel**, click the **Library Manager** link. Click the **BSPs** tab to
see all available kits. If you are working with a code example, make
sure that the example supports the kit you want to use.

What is the project’s makefile and what does it do?
---------------------------------------------------

For ModusToolbox, every application has a makefile. That file fully
describes the application, including what board or kit to use, what tool
chain to use, what files to use, what libraries to include, what
compiler and linker flags control the build, and so on. See `Running
ModusToolbox from the Command
Line <http://www.cypress.com/ModusToolboxCommandLine>`__ for detailed
documentation. You can also go to a `community
KBA <https://community.cypress.com/docs/DOC-18994>`__ on how to manage
the makefile.

Where do I find build settings?
-------------------------------

In ModusToolbox, compiler and linker settings are controlled by the
makefile that describes the application. They are not set in the Eclipse
UI.

There are many settings in the project’s properties that you may want to
adjust. To see a project’s properties, use **Project** > **Properties**
or right-click a project and choose **Properties**. Then navigate to the
panel you need.

   |image14|

What is a build configuration?
------------------------------

The ModusToolbox makefile has two default build configurations,
**Debug** and **Release**. Use the CONFIG variable in the makefile to
choose which to use. For example, **CONFIG**\ =Debug.

In an Eclipse IDE, a build configuration is a collection of all the
project properties and build settings. Because ModusToolbox uses a
makefile to specify build options, changing the Eclipse build
configuration has no effect on how the ModusToolbox application builds.

Where do I specify compiler symbols and defines?
------------------------------------------------

In the makefile that defines the application. See `Running ModusToolbox
from the Command
Line <http://www.cypress.com/ModusToolboxCommandLine>`__ for detailed
documentation. You can also go to a `community
KBA <https://community.cypress.com/docs/DOC-18994>`__ on how to manage
the makefile.

How can I tell if a build was successful?
-----------------------------------------

The easy way is to look in the **Problems** tab of the console pane. The
console window shows any errors, but they can be hard to find in the
console log.

   |image15|

How do I terminate a build?
---------------------------

When you start a build, a progress bar appears in the bottom-right
corner of the window. There is a little button to the right.

|image16|

Click the button; the Progress tab opens in the Console area. It looks
like this.

|image17|

Click the red terminate button; the build stops.

**Advanced Tip**: Some builds are very quick, and every time a build
ends, focus shifts back to the Console tab, foiling your attempt to
cancel the build. You can change that behavior. Go to **Window** >
**Preferences** > **C/C++** > **Build** > **Console** and disable
**Bring console to top when building (if present)**. The Progress tab
will now keep focus when it's open.

I have an error that a symbol (e.g. bool or uint32_t) could not be resolved. What’s going on and what do I do about it?
-----------------------------------------------------------------------------------------------------------------------

The odds are this is a false positive. Other symbols defined in the file
are fine, but one shows up as an error. The code builds despite this
error.

|image18|

This is a problem with the Eclipse code indexer. You can force a
reindexing of the application. Right-click a project in the project
explorer and choose **Index** > **Rebuild**. The error may persist, even
if you rebuild the index.

It is possible that the file that defines the symbol may not be in the
#include hierarchy. This is a real problem. In this case you would see
many errors, not just one or two, because every symbol in the header
file would be unresolved. Make sure the header is included and the
problem goes away.

Program/Debug
=============

What are run and debug configurations?
--------------------------------------

Each of these is a collection of settings that control how the target is
programmed and debugged. A run configuration programs the target and
begins execution. A debug configuration does the same, but launches the
debugger and (typically) stops execution at the first line of main(). A
launch configuration can execute a combination of run and debug
configurations.

A ModusToolbox project uses these configurations. In the Eclipse IDE for
ModusToolbox, an application typically includes configurations named
Attach, Erase, Program, and Debug. For details on how to program or
debug, see the *“*\ Program and Debug” chapter in the `Eclipse IDE for
ModusToolbox User
Guide <http://www.cypress.com/ModusToolboxUserGuide>`__.

My launch configurations disappeared, what do I do?
---------------------------------------------------

This happens when a project gets a new name, either when importing or by
renaming. There are two ways to solve this problem.

The easy way: in the **Quick Panel**, click the **Generate Launches**
command to regenerate launch configurations. This creates a new set of
default launch configurations linked to your project.

If you have custom launch configurations, you can re-link the project to
existing launch configurations. This is a classic example of well-hidden
functionality in the Eclipse IDE. Here’s how to get it done. In the
screenshots that follow, we renamed MyCoolApp to MyHelloWorld, so launch
configurations vanished.

In the **Run** menu, choose **Debug Configurations**. In the dialog,
open the **Filter** menu and uncheck **Filter Deleted/Unavailable
Projects**. This allows you to see the launch configurations for the
original project.

|image19|

Then open one of the folders in the left column that now contains the
formerly-filtered configurations. Select the debug configuration you
want to associate with the renamed project. On the right side of the
window, click **Browse**, and then select the project you want to use.

|image20|

When you are done, the launch configuration appears in the Quick Panel
and is available to your project.

|image21|

I copied my project, and now it has two sets of launch configurations. How do I fix that?
-----------------------------------------------------------------------------------------

In the Eclipse Project Explorer you can copy and paste to duplicate a
project. When you do, a bug in Eclipse results in your original project
having a duplicate set of launch configurations. The new copy (which is
renamed in the process) has none.

To solve the problem with the new copy not having any launch
configurations, see **My launch configurations disappeared, what do I
do?** That answer also tells you how to connect the new copy to a custom
launch configuration, if you have one.

As for the original project, the duplicate configurations appear in the
**Quick Panel** Launches area. They have the same names, and you cannot
easily tell which is which.

The simple solution is to go to the Eclipse workspace folder. Open the
*<new copy name>/.mtbLaunchConfigs* folder. Delete the contents. Problem
solved. This will wipe out any custom configuration as well, so be
careful if you have one.

You can accomplish the same result using the Eclipse UI, but it is more
tedious. In the Eclipse Project Explorer, select the original project
(with the duplicate launch configurations). Use **Run** > Debug
**Configurations** to open the Debug Configurations dialog. Select the
**Common** tab. Select a debug configuration on the left of the dialog,
the path to that configuration appears. Delete those configurations that
exist in the new copy. Do this for each duplicate – select to find out
if it is in the new copy, and delete it when it is. Be aware that this
process deletes the launch configuration file in the new copy. So be
careful if you have a custom launch configuration that you want to keep.

|image22|

How do I start a debug session?
-------------------------------

The simple way is to scroll to the **Launches** section of the **Quick
Panel**. Then click the desired configuration. See the *“*\ Program and
Debug” chapter in the `Eclipse IDE for ModusToolbox User
Guide <http://www.cypress.com/ModusToolboxUserGuide>`__ for details.

When I try to program or debug I get a connection error. What do I do?
----------------------------------------------------------------------

Sometimes the error is self-explanatory, sometimes not. Here’s a fairly
common error where you might need a little help understanding what’s
going on.

Error: unable to find CMSIS-DAP device

Error: No Valid JTAG Interface Configured

The most common cause, you don’t have a board connected, or the board
doesn’t have power. Connect or power up the board, and you should be
fine.

Another error you may see is:

Error: KitProg firmware is out of date, please update to the latest
version using fw-loader tool

Many Cypress boards come from the factory with the older KitProg2
installed. ModusToolbox does not work with KitProg2. You need to upgrade
the communication firmware on the kit to KitProg3, and the fw-loader
tool does that.

In a nutshell, the firmware loader is a command line tool that comes
with ModusToolbox software. You can use it to determine what KitProg is
on your kit, and upgrade to KitProg3. You can also go back to KitProg2
if you need to.

The firmware loader tool is installed with the IDE. You can get the
latest directly from the `Firmware-loader GitHub
repository <https://github.com/cypresssemiconductorco/Firmware-loader>`__.

How do I step through assembly code?
------------------------------------

The ability to do this is not visible in the ModusToolbox perspective.
Use the Debug perspective, which has this capability enabled by default.
Perspective icons are at the top right of the IDE’s window. If the icon
is not visible, use **Window** > **Perspective** > **Open Perspective**.

|image23|

Then, in the Debug perspective’s **Debug** panel, enable the Instruction
Stepping Mode button.

|image24|

**Pro Tip:** You can modify the ModusToolbox perspective to make this
option visible. Use **Window** > **Perspective** > **Customize
Perspective**. Click the **Action Set Availability** tab. Turn on
**C/C++ Debug**. The Instruction Stepping icon now appears in the
ModusToolbox perspective

I like the Debug perspective better. How do I use it by default when I debug?
-----------------------------------------------------------------------------

Use **Window** > **Preferences** > **Run/Debug** > **Perspectives**. In
that panel, locate the **GDB OpenOCD Debugging** launcher, and change to
the Debug perspective.

|image25|

When debugging, how do I see variables, registers, and memory?
--------------------------------------------------------------

All this information appears in various views within the IDE. In the
default ModusToolbox perspective:

Variables, Expressions, Breakpoints: near the Quick Panel - |image26|

Registers: near the Project Explorer - |image27|

Memory: near the Console - |image28|

If a view is not visible, select **Window** > **Show View**, and pick
the view that you want. You can drag views to new locations, maximize or
minimize, and manage the arrangement of views as you wish.

How do I see local variables?
-----------------------------

The Variables view shows local variables. Any variable that is in scope
should appear automatically in this view. This view does not and cannot
display global variables.

How do I see global variables?
------------------------------

Use the Expressions view to see global variables. You can drag the
variable name into this view or click **Add new expression** and type in
the name. You can also use this view to create and evaluate complex
expressions.

When I start a debug session, an editor window appears that tells me there is a break at an address with no debug information available. But the debug session starts fine. What's going on and what do I do about it?
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Here's a typical warning with this "error." The address mentioned will
vary.

|image29|

This is the warning that appears when a source file can't be found.
Sometimes that's an actual problem, although in this case (*the debug
session starts fine*) it isn't.

You can configure Eclipse preferences to show this warning less
frequently. Click the **Preferences** button in this window. That takes
you to the general debug settings panel in Eclipse preferences, as shown
below. The default is to show this window all the time.

Enable the **Only if source file name is known but not found** option.
Then the warning appears when, in fact, a source file can't be found.

   |image30|

When I terminate a debug session, the processor stops. How do I stop debugging but leave the processor running?
---------------------------------------------------------------------------------------------------------------

In the Debug view, right-click the openocd process, and select
**Terminate**. This shuts down the debug connection and leaves the
processor running.

|image31|

You can also select the openocd thread, and then click the **Terminate**
button in the toolbar.

How do I remove terminated debug sessions from the Debug view?
--------------------------------------------------------------

A terminated debug session remains visible in the Debug view.

|image32|

You don't need to do anything. It is removed automatically when you
start the next debug session. However, if you want to remove terminated
debug sessions manually:

-  Select one or more and press the **Delete** key.

-  Right-click and choose **Remove All Terminated**.

How do I start debugging without downloading the executable again?
------------------------------------------------------------------

In **Run** > **Debug Configurations**, choose an **Attach**
configuration.

**Related Categories:**

**Keywords:** ModusToolbox Eclipse Import Code Example

**Product Family:** PSoC 6 MCU

**Related Tags:** [Select the Tags by clicking the checkbox associated
to the Tags]

.. |SQUAR|  raw:: html
   
   &#9744;
   
.. |CRSQR|  raw:: html
   
   &#9746;

**Clocks & Buffers Tags**

+------------------+----------------------+------------------+------------------+------------------+--------------------+
| |SQUAR| Adapter  | |SQUAR| Algorithm    | |SQUAR| Bitmap   | |SQUAR| Buffer   | |SQUAR| Bypass   | |SQUAR| CLKMAKER   |
+------------------+----------------------+------------------+------------------+------------------+--------------------+
| |SQUAR| CML      | |SQUAR| CY3670       | |SQUAR| CY3672   | |SQUAR| CY3675   | |SQUAR| CY36800  | |SQUAR| Cascade    |
+------------------+----------------------+------------------+------------------+------------------+--------------------+
| |SQUAR| Charge   | |SQUAR| Clock        | |SQUAR| Clocks   | |SQUAR| Clocks   | |SQUAR| ComLink  | |SQUAR| Crystal    |
| Pump             | Tree                 |                  | and              |                  | Osc                |
|                  |                      |                  | Buffers          |                  | illators           |
+------------------+----------------------+------------------+------------------+------------------+--------------------+
| |SQUAR| CyClock  | |SQUAR| CyClo        | |SQUAR| Cyb      | |SQUAR| Cyb      | |SQUAR| Cycle to | |SQUAR| DCXO       |
|                  | ckWizard             | erClocks         | erClocks         | Cycle            |                    |
|                  |                      |                  | Online           |                  |                    |
+------------------+----------------------+------------------+------------------+------------------+--------------------+
| |SQUAR| Delay    | |SQUAR| Deter        | |SQUAR| Diff     | |SQUAR| Divider  | |SQUAR| Duty     | |SQUAR| EMI        |
|                  | ministic             | erential         |                  | Cycle            |                    |
|                  | Jitter               |                  |                  |                  |                    |
+------------------+----------------------+------------------+------------------+------------------+--------------------+
| |SQUAR| Enhanced | |SQUAR| Ent          | |SQUAR| Entrant  | |SQUAR| FNOM     | |SQUAR| Factory  | |SQUAR| Failsafe   |
| Per              |                      |                  |                  |                  |                    |
| formance         |                      |                  |                  |                  |                    |
+------------------+----------------------+------------------+------------------+------------------+--------------------+
| |SQUAR| Fanout   | |SQUAR| FastEdge     | |SQUAR| Feedback | |SQUAR| Field    | |SQUAR| Flexo    | |SQUAR| F          |
|                  |                      |                  |                  |                  | requency           |
+------------------+----------------------+------------------+------------------+------------------+--------------------+
| |SQUAR| F        | |SQUAR| Function     | |SQUAR| Ge       | |SQUAR| Ground   | |SQUAR| HPB      | |SQUAR| HSTL       |
| requency         | Select               | nerators         | Bounce           |                  |                    |
| M                |                      |                  |                  |                  |                    |
| argining         |                      |                  |                  |                  |                    |
+------------------+----------------------+------------------+------------------+------------------+--------------------+
| |SQUAR| Hershey  | |SQUAR| I2C          | |SQUAR| IBIS     | |SQUAR| I        | |SQUAR| Input    | |SQUAR| In         |
| Kiss             |                      | Model            | mpedance         |                  | staClock           |
+------------------+----------------------+------------------+------------------+------------------+--------------------+
| |SQUAR| Inverted | |SQUAR| Jed          | |SQUAR| Jedec    | |SQUAR| Jitter   | |SQUAR| LVCMOS   | |SQUAR| LVDS       |
+------------------+----------------------+------------------+------------------+------------------+--------------------+
| |SQUAR| LVPECL   | |SQUAR| Layout       | |SQUAR| Lexmark  | |SQUAR| Loop     | |SQUAR| MoBL     | |SQUAR| Mo         |
|                  |                      | Profile          | B                |                  | dulation           |
|                  |                      |                  | andwidth         |                  | Rate               |
+------------------+----------------------+------------------+------------------+------------------+--------------------+
| |SQUAR| Mu       | |SQUAR| NZDB         | |SQUAR| Non-     | |SQUAR| OTP      | |SQUAR| Offset   | |SQUAR| Os         |
| ltiplier         |                      | Volatile         |                  |                  | cillator           |
|                  |                      | Memory           |                  |                  |                    |
+------------------+----------------------+------------------+------------------+------------------+--------------------+
| |SQUAR| Output   | |SQUAR| Ove          | |SQUAR| PCI      | |SQUAR| PLL      | |SQUAR| PPM      | |SQUAR| PREMIS     |
|                  | rvoltage             | Express          |                  |                  |                    |
+------------------+----------------------+------------------+------------------+------------------+--------------------+
| |SQUAR| Period   | |SQUAR| Phase        | |SQUAR| Phase    | |SQUAR| Power    | |SQUAR| Program  | |SQUAR| Prog       |
|                  |                      | Noise            | Supply           |                  | rammable           |
|                  |                      |                  | Noise            |                  |                    |
+------------------+----------------------+------------------+------------------+------------------+--------------------+
| |SQUAR| Pro      | |SQUAR| Pul          | |SQUAR| RMS      | |SQUAR| Rambus   | |SQUAR| Random   | |SQUAR| R          |
| pagation         | lability             |                  |                  | Jitter           | eference           |
+------------------+----------------------+------------------+------------------+------------------+--------------------+
| |SQUAR| Rise     | |SQUAR| R            | |SQUAR| S        | |SQUAR| Serial   | |SQUAR| Signal   | |SQUAR| Skew       |
| Fall             | oboClock             | chematic         |                  | I                |                    |
| Time             |                      |                  |                  | ntegrity         |                    |
+------------------+----------------------+------------------+------------------+------------------+--------------------+
| |SQUAR| Socket   | |SQUAR| S            | |SQUAR| Spread % | |SQUAR| Spread   | |SQUAR| Spread   | |SQUAR| Spread     |
|                  | pecialty             |                  | Aware            | Profile          | Spectrum           |
|                  | Clocks               |                  |                  |                  |                    |
+------------------+----------------------+------------------+------------------+------------------+--------------------+
| |SQUAR| Syn      | |SQUAR| TCXO         | |SQUAR| TIE      | |SQUAR| Ter      | |SQUAR| Thermal  | |SQUAR| Tr         |
| thesizer         |                      |                  | mination         |                  | anslator           |
+------------------+----------------------+------------------+------------------+------------------+--------------------+
| |SQUAR| Unde     | |SQUAR| UniClock     | |SQUAR| Unit     | |SQUAR| VCO      | |SQUAR| VCXO     | |SQUAR| Volatile   |
| rvoltage         |                      | Interval         |                  |                  | Memory             |
+------------------+----------------------+------------------+------------------+------------------+--------------------+
| |SQUAR| Voltage  | |SQUAR| XCG          | |SQUAR| XDR      | |SQUAR| XO       | |SQUAR| Xtal     | |SQUAR| ZDB        |
| Droop            |                      |                  |                  |                  |                    |
+------------------+----------------------+------------------+------------------+------------------+--------------------+
| |SQUAR| Zepto    |                      |                  |                  |                  |                    |
+------------------+----------------------+------------------+------------------+------------------+--------------------+

**Lighting & Power Control Tags**

+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Board    | |SQUAR| Boost    | |SQUAR| Buck     | |SQUAR| CY3261   | |SQUAR| CY3267   | |SQUAR| CY3268   |
| Layout           | R                | R                |                  |                  |                  |
|                  | egulator         | egulator         |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| CY3269   | |SQUAR| Color    | |SQUAR| Current  | |SQUAR| DALI     | |SQUAR| DMX      | |SQUAR| FN Pins  |
|                  | Mixing           | Sense            |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| HB LEDs  | |SQUAR| Hy       | |SQUAR| MOSFETs  | |SQUAR| MPPT     | |SQUAR| Mo       | |SQUAR| PrISM    |
|                  | steretic         |                  |                  | dulators         |                  |
|                  | Co               |                  |                  |                  |                  |
|                  | ntroller         |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Pro      | |SQUAR| SSDM     | |SQUAR| Sc       | |SQUAR| S        | |SQUAR| Trip     | |SQUAR| Voltage  |
| gramming         |                  | hematics         | witching         |                  | R                |
|                  |                  |                  | Re               |                  | egulator         |
|                  |                  |                  | gulators         |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+

**Wireless/RF Tags**

+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| 2.4 GHz  | |SQUAR| 8DR      | |SQUAR| Antenna  | |SQUAR| Audio    | |SQUAR| AgileHID | |SQUAR| Bridge   |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| CY3271   | |SQUAR| CY3630   | |SQUAR| CY4636   | |SQUAR| CY4672   | |SQUAR| CYFI     | |SQUAR| CYFISNP  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| CYRF6936 | |SQUAR| CYRF7936 | |SQUAR| Channel  | |SQUAR| DDR      | |SQUAR| DSSS     | |SQUAR| GFSK     |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| HID      | |SQUAR| HUB      | |SQUAR| IRQ      | |SQUAR| Inte     | |SQUAR| Keyboard | |SQUAR| Link     |
|                  |                  |                  | rference         |                  | Budget           |
|                  |                  |                  | A                |                  |                  |
|                  |                  |                  | voidance         |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Mouse    | |SQUAR| Node     | |SQUAR| PMU      | |SQUAR| PN Code  | |SQUAR| PRoC     | |SQUAR| PRoC-CS  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| PRoC-EMB | |SQUAR| PRoC-TT  | |SQUAR| PRoC-USB | |SQUAR| PRoC-UI  | |SQUAR| Power    | |SQUAR| Preamble |
|                  |                  |                  |                  | A                |                  |
|                  |                  |                  |                  | mplifier         |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Pseudo   | |SQUAR| RF       | |SQUAR| RSSI     | |SQUAR| Range    | |SQUAR| Remote   | |SQUAR| SCD      |
| Noise            |                  |                  |                  |                  |                  |
| code             |                  |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| SDR      | |SQUAR| SOP      | |SQUAR| SPIM     | |SQUAR| Star     | |SQUAR| S        | |SQUAR| Trackpad |
|                  |                  |                  | -Network         | treaming         |                  |
|                  |                  |                  | Protocol         |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Wireless | |SQUAR| Wireless | |SQUAR| Wireless | |SQUAR| Wireless | |SQUAR| Wireless | |SQUAR| Wireless |
|                  | USB              | USB LP           | USB LS           | USB LR           | USB NL           |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Wireless |                  |                  |                  |                  |                  |
| Ca               |                  |                  |                  |                  |                  |
| pacitive         |                  |                  |                  |                  |                  |
| Touch            |                  |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+

**Memory Tags**

+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Address  | |SQUAR| Async    | |SQUAR| Au       | |SQUAR| A        | |SQUAR| BHE /    | |SQUAR| BUSY     |
| Bus              |                  | tomotive         | utostore         | BLE              |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Battery  | |SQUAR| Burst    | |SQUAR| Bus      | |SQUAR| Clock    | |SQUAR| DDR      | |SQUAR| Data Bus |
| Backup           |                  | Co               |                  |                  |                  |
|                  |                  | ntention         |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Data     | |SQUAR| Data     | |SQUAR| D        | |SQUAR| Dual     | |SQUAR| Echo     | |SQUAR| FIFO     |
| I                | R                | atasheet         | Port             | Clocks           |                  |
| ntegrity         | etention         |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| FPGA     | |SQUAR| Flags    | |SQUAR| Flo      | |SQUAR| Fullflex | |SQUAR| FRAM     | |SQUAR| HOLD     |
|                  |                  | wthrough         |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| HD-FIFO  | |SQUAR| HSB      | |SQUAR| I2C      | |SQUAR| INT      | |SQUAR| I        | |SQUAR| I        |
|                  |                  |                  |                  | mpedance         | nterface         |
|                  |                  |                  |                  | Matching         |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Inte     | |SQUAR| JTAG     | |SQUAR| Junction | |SQUAR| Low      | |SQUAR| MSL      | |SQUAR| M        |
| rleaving         |                  | Tem              | Power            |                  | igration         |
|                  |                  | perature         |                  |                  | Path             |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| MoBL     | |SQUAR| Models   | |SQUAR| NoBL     | |SQUAR| ODT      | |SQUAR| Obsolete | |SQUAR| PCB      |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| PLL /    | |SQUAR| P        | |SQUAR| Parallel | |SQUAR| Parity   | |SQUAR| Part     | |SQUAR| Part     |
| DLL              | ackaging         |                  |                  | Decoder          | Di               |
|                  |                  |                  |                  |                  | fference         |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Password | |SQUAR| Pin      | |SQUAR| Pipeline | |SQUAR| Power    | |SQUAR| Power On | |SQUAR| P        |
| Pr               | Confi            |                  | Con              | State            | rocessor         |
| otection         | guration         |                  | sumption         |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| QDR      | |SQUAR| Quali    | |SQUAR| Quality  | |SQUAR| RECALL   | |SQUAR| RTC      | |SQUAR| Race     |
|                  | fication         |                  |                  |                  | C                |
|                  | Reports          |                  |                  |                  | ondition         |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Read     | |SQUAR| SER      | |SQUAR| SPCM     | |SQUAR| SPI      | |SQUAR| SRAM     | |SQUAR| Serial   |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Signal   | |SQUAR| Sync     | |SQUAR| Te       | |SQUAR| Ter      | |SQUAR| Timing   | |SQUAR| Vcap     |
| I                |                  | chnology         | mination         |                  |                  |
| ntegrity         |                  |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Vddq     | |SQUAR| Voltage  | |SQUAR| Vref     | |SQUAR| Width /  | |SQUAR| Write    | |SQUAR| nvSRAM   |
|                  | Levels           |                  | Depth            |                  |                  |
|                  |                  |                  | E                |                  |                  |
|                  |                  |                  | xpansion         |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| uPower   |                  |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+

**Interface Tags**

+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| 10B/8B   | |SQUAR| 3 Level  | |SQUAR| ALDEC    | |SQUAR| ATM      | |SQUAR| Altera   | |SQUAR| BIST     |
| Decoder          | Inputs           |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| BSDL     | |SQUAR| Biasing  | |SQUAR| CDR      | |SQUAR| CML      | |SQUAR| CPLD     | |SQUAR| CY3900i  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| CY3950i  | |SQUAR| CY       | |SQUAR| Cable    | |SQUAR| Cable    | |SQUAR| Channel  | |SQUAR| Clock    |
|                  | USBISRPC         |                  | Driver           | Bonding          | Mu               |
|                  |                  |                  |                  |                  | ltiplier         |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Clocking | |SQUAR| Coaxial  | |SQUAR| Copper   | |SQUAR| Coupling | |SQUAR| Crystal  | |SQUAR| Current  |
| Modes            | Cable            | Cable            |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| DVB      | |SQUAR| Data     | |SQUAR| Data     | |SQUAR| Delta39K | |SQUAR| Des      | |SQUAR| Dese     |
|                  | C                | rate             |                  | erialize         | rializer         |
|                  | haracter         |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Dual     | |SQUAR| EPLD     | |SQUAR| EPROM    | |SQUAR| ESCON    | |SQUAR| El       | |SQUAR| E        |
| Channel          |                  |                  |                  | asticity         | qualizer         |
|                  |                  |                  |                  | Buffer           |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Error    | |SQUAR| Ev       | |SQUAR| FPGA     | |SQUAR| Fiber    | |SQUAR| F        | |SQUAR| Framer   |
|                  | aluation         |                  | Optics           | lash370i         |                  |
|                  | Board            |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Framing  | |SQUAR| Framing  | |SQUAR| Gigabit  | |SQUAR| Hex      | |SQUAR| High     | |SQUAR| Hotlink  |
| C                | Mode             | Ethernet         |                  | Speed            |                  |
| haracter         |                  |                  |                  | Serial           |                  |
|                  |                  |                  |                  | Links            |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| HotlinkI | |SQUAR| H        | |SQUAR| IP       | |SQUAR| Isr      | |SQUAR| Jtag     | |SQUAR| LFI      |
|                  | otlinkII         |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| LVTTL    | |SQUAR| Logic    | |SQUAR| Loop     | |SQUAR| M        | |SQUAR| Max340   | |SQUAR| Model    |
|                  | Level            | Back             | acrocell         |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Non-     | |SQUAR| OC-1     | |SQUAR| OC-2     | |SQUAR| OC-3     | |SQUAR| O        | |SQUAR| PAL      |
| Volatile         |                  |                  |                  | perating         |                  |
|                  |                  |                  |                  | System           |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| PECL     | |SQUAR| PLD      | |SQUAR| PLL      | |SQUAR| Parallel | |SQUAR| Parallel | |SQUAR| Parity   |
|                  |                  |                  | Input            | Output           |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Phase    | |SQUAR| PoF      | |SQUAR| Point To | |SQUAR| Point To | |SQUAR| Power    | |SQUAR| Pro      |
| Align            |                  | Multi            | Point            | Supply           | gramming         |
| Buffer           |                  | Point            |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Quad     | |SQUAR| Qu       | |SQUAR| Receiver | |SQUAR| R        | |SQUAR| R        | |SQUAR| R        |
| Channel          | antum38K         |                  | ecovered         | edundant         | eference         |
|                  |                  |                  | Clock            | Outputs          | Clock            |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Report   | |SQUAR| Reprog   | |SQUAR| SMA      | |SQUAR| SMPTE    | |SQUAR| SM       | |SQUAR| SM       |
| File             | rammable         |                  |                  | PTE-259M         | PTE-292M         |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| SONET    | |SQUAR| SPICE    | |SQUAR| SPLD     | |SQUAR| STAPL    | |SQUAR| SVF      | |SQUAR| Serial   |
|                  |                  |                  |                  |                  | Input            |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Serial   | |SQUAR| Se       | |SQUAR| S        | |SQUAR| Size     | |SQUAR| Special  | |SQUAR| Speci    |
| Output           | rializer         | imulator         |                  | C                | fication         |
|                  |                  |                  |                  | haracter         |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| TTL      | |SQUAR| Tem      | |SQUAR| Third    | |SQUAR| Tra      | |SQUAR| Tra      | |SQUAR| USBISRPC |
|                  | perature         | Party            | nsceiver         | nsmitter         | Cable            |
|                  |                  | Tool             |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Ultra37K | |SQUAR| User     | |SQUAR| VHDL     | |SQUAR| Verilog  | |SQUAR| Voltage  | |SQUAR| Warp     |
|                  | Guide            |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Word     |                  |                  |                  |                  |                  |
| Sync             |                  |                  |                  |                  |                  |
| Sequence         |                  |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+

**PSoC 1 Tags**

+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| ADC      | |SQUAR| ADCINC   | |SQUAR| Analog   | |SQUAR| Analog   | |SQUAR| API      | |SQUAR| Assembly |
|                  |                  |                  | Bus              |                  | Language         |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Boot.asm | |SQUAR| Bo       | |SQUAR| BSDL     | |SQUAR| Build    | |SQUAR| Cal      | |SQUAR| Capsense |
|                  | otloader         | Files            | Errors           | ibration         |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Clock    | |SQUAR| Clock    | |SQUAR| Cloning  | |SQUAR| Column   | |SQUAR| Commu    | |SQUAR| Co       |
|                  | Synchro          |                  | Clock            | nication         | mparator         |
|                  | nization         |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Compiler | |SQUAR| Counter  | |SQUAR| CPU      | |SQUAR| Crystal  | |SQUAR| CT Block | |SQUAR| DAC      |
|                  |                  | Speed            |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| D        | |SQUAR| D        | |SQUAR| Delta    | |SQUAR| Dev      | |SQUAR| Digital  | |SQUAR| Dynamic  |
| ebugging         | ecimator         | Sigma            | elopment         |                  | Reconfi          |
|                  |                  | ADC              | Kit              |                  | guration         |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| ECO      | |SQUAR| EEPROM   | |SQUAR| Errata   | |SQUAR| Filter   | |SQUAR| Flash    | |SQUAR| Flash    |
|                  |                  |                  |                  |                  | Security         |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Global   | |SQUAR| GPIO     | |SQUAR| Hex File | |SQUAR| I2C      | |SQUAR| I2C-USB  | |SQUAR| ICE Cube |
| R                |                  |                  |                  | Bridge           |                  |
| esources         |                  |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Inst     | |SQUAR| Internet | |SQUAR| I        | |SQUAR| ISR      | |SQUAR| ISSP     | |SQUAR| Large    |
| allation         | Explorer         | nterrupt         |                  |                  | Memory           |
|                  |                  |                  |                  |                  | Model            |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| LCD      | |SQUAR| License  | |SQUAR| MAC      | |SQUAR| M        | |SQUAR| M        | |SQUAR| Mux      |
|                  |                  |                  | iniProg1         | iniProg3         |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Mux Bus  | |SQUAR| OCD      | |SQUAR| Offset   | |SQUAR| Pod      | |SQUAR| Port and | |SQUAR| Power    |
|                  |                  |                  |                  | Pins             | Ma               |
|                  |                  |                  |                  |                  | nagement         |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Pr       | |SQUAR| PWM      | |SQUAR| RAM      | |SQUAR| RTC      | |SQUAR| SAR      | |SQUAR| SC Block |
| oduction         |                  |                  |                  |                  |                  |
| Pr               |                  |                  |                  |                  |                  |
| ogrammer         |                  |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| SMP      | |SQUAR| SPI      | |SQUAR| System   | |SQUAR| Timer    | |SQUAR| UART     | |SQUAR| USB      |
|                  |                  | Level            |                  |                  |                  |
|                  |                  | Design           |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| USBUART  | |SQUAR| Watchdog | |SQUAR| A        | |SQUAR| Analog   | |SQUAR| Analog   | |SQUAR| Build    |
|                  |                  | mplifier         | R                | UM               | Tools            |
|                  |                  | UM               | eference         |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Commu    | |SQUAR| CRC UM   | |SQUAR| CYFI     | |SQUAR| Device   | |SQUAR| Digital  | |SQUAR| DTMF     |
| nication         |                  |                  | Pro              | UM               |                  |
| UM               |                  |                  | gramming         |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Fan      | |SQUAR| Firmware | |SQUAR| FMEA     | |SQUAR| Port     | |SQUAR| PSoC     | |SQUAR| Voltage  |
| Co               | UM               |                  | Expander         | Power            | S                |
| ntroller         |                  |                  |                  | System           | equencer         |
| UM               |                  |                  |                  | Arch             | UM               |
|                  |                  |                  |                  | itecture         |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+

**PSoC 3/4/5 Tags**

+-------------------+------------------+------------------+------------------+------------------+------------------+
|**Component        |                  |                  |                  |                  |                  |
|tags**             |                  |                  |                  |                  |                  |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Analog    | |SQUAR| Analog   | |SQUAR| Boost    | |SQUAR| Bo       | |SQUAR| CAN      | |SQUAR| CapS     |
| Hardware          | Mux              | C                | otloader         |                  | ense_CSD         |
| Mux               |                  | onverter         | /                |                  |                  |
|                   |                  |                  | Boot             |                  |                  |
|                   |                  |                  | loadable 	     |                  |                  |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| C         | |SQUAR| Clock    | |SQUAR| Co       | |SQUAR| Control  | |SQUAR| Counter  | |SQUAR| CRC      |
| haracter          |                  | mparator         | / Status         |                  |                  |
| LCD               |                  |                  | Register         |                  |                  |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| DAC       | |SQUAR| D        | |SQUAR| Delta    | |SQUAR| DFB      | |SQUAR| DFB      | |SQUAR| Die      |
|                   | ebouncer         | Sigma            |                  | A                | Tem              |
|                   |                  | ADC              |                  | ssembler         | perature         |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Digital   | |SQUAR| Digital  | |SQUAR| DMA      | |SQUAR| EEPROM   | |SQUAR| emFile   | |SQUAR| EMIF     |
| Co                | Mul              |                  |                  | SPI Mode         |                  |
| mparator          | tiplexer         |                  |                  |                  |                  |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| EzI2C     | |SQUAR| Fan      | |SQUAR| Filter   | |SQUAR| F        | |SQUAR| Glitch   | |SQUAR| Global   |
| Slave             | Co               |                  | requency         | Filter           | Signal           |
|                   | ntroller         |                  | Divider          |                  | R                |
|                   |                  |                  |                  |                  | eference         |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Graphic   | |SQUAR| I2C /I   | |SQUAR| iAP      | |SQUAR| I        | |SQUAR| LIN      | |SQUAR| Logic    |
| LCD               | 2S               |                  | nterrupt         |                  | Gates            |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Lookup    | |SQUAR| Manual   | |SQUAR| MDIO     | |SQUAR| Mixer    | |SQUAR| Opamp    | |SQUAR| PGA      |
| Table             | Routing          |                  |                  |                  |                  |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Ports     | |SQUAR| Power    | |SQUAR| PRS      | |SQUAR| PWM      | |SQUAR| Qu       | |SQUAR| R        |
| and Pins          | Monitor          |                  |                  | adrature         | esistive         |
|                   |                  |                  |                  | Decoder          | Touch            |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| RTC       | |SQUAR| RTD      | |SQUAR| Sample / | |SQUAR| SAR ADC  | |SQUAR| SAR      | |SQUAR| Segment  |
|                   | Ca               | Track            |                  | S                | LCD              |
|                   | lculator         | and Hold         |                  | equencer         |                  |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| SGPIO     | |SQUAR| Shift    | |SQUAR| Sleep    | |SQUAR| SM /     | |SQUAR| SPDIF    | |SQUAR| SPI      |
|                   | Register         | Timer            | PMBus            |                  |                  |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Sync      | |SQUAR| Th       | |SQUAR| Ther     | |SQUAR| TIA      | |SQUAR| Timer    | |SQUAR| TMP05    |
|                   | ermistor         | mocouple         |                  |                  | I                |
|                   | Ca               | Ca               |                  |                  | nterface         |
|                   | lculator         | lculator         |                  |                  |                  |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Tr        | |SQUAR| UART     | |SQUAR| UDBClkEn | |SQUAR| USBFS    | |SQUAR| USBMIDI  | |SQUAR| USBUART  |
| imMargin          |                  |                  |                  |                  | (CDC             |
|                   |                  |                  |                  |                  | In               |
|                   |                  |                  |                  |                  | terface)         |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Voltage   | |SQUAR| Voltage  | |SQUAR| Vref     | |SQUAR| WaveDAC  |                  |                  |
| Fault             | S                |                  |                  |                  |                  |
| Detector          | equencer         |                  |                  |                  |                  |
+-------------------+------------------+------------------+------------------+------------------+------------------+
|**General          |                  |                  |                  |                  |                  |
|Tags**             |                  |                  |                  |                  |                  |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Analog    | |SQUAR| Analog   | |SQUAR| Analog   | |SQUAR| API      | |SQUAR| App      | |SQUAR| Assembly |
| Bus               | Global           | Mux Bus          |                  | lication         | Language         |
|                   | Bus              |                  |                  | Specific         |                  |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Bo        | |SQUAR| Boundary | |SQUAR| Bridge   | |SQUAR| Build    | |SQUAR| Clock    | |SQUAR| Compiler |
| otloader          | Scan /           | Control          | Settings         |                  | - GCC            |
| Host              | BSDL             | Panel            |                  |                  |                  |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Compiler  | |SQUAR| Compiler | |SQUAR| Compiler | |SQUAR| C        | |SQUAR| C        | |SQUAR| Creator  |
| - KEIL            | - MDK            | - RVDS           | ortex-M0         | ortex-M3         | Regi             |
|                   |                  |                  |                  |                  | stration         |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Custom    | |SQUAR| Datapath | |SQUAR| D        | |SQUAR| DMA      | |SQUAR| DVK      | |SQUAR| ECO      |
| C                 | Confi            | ebugging         | Wizard           |                  |                  |
| omponent          | guration         |                  |                  |                  |                  |
| Inte              | Tool             |                  |                  |                  |                  |
| rconnect          |                  |                  |                  |                  |                  |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Errata    | |SQUAR| Error    | |SQUAR| Flash    | |SQUAR| Hex File | |SQUAR| Inst     | |SQUAR| ISSP /   |
|                   | Message          |                  |                  | allation         | HSSP             |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| KEIL      | |SQUAR| Linux    | |SQUAR| Low      | |SQUAR| LVD /    | |SQUAR| MFi      | |SQUAR| M        |
| Regi              | Platform         | Power            | HVD              |                  | iniProg3         |
| stration          |                  | Modes            |                  |                  |                  |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Opti      | |SQUAR| Pr       | |SQUAR| PSoC     | |SQUAR| PSoC     | |SQUAR| Reset    | |SQUAR| RTOS     |
| mization          | ogrammer         | Creator          | Pr               |                  |                  |
|                   | COM              |                  | ogrammer         |                  |                  |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| S         | |SQUAR| Silicon  | |SQUAR| Software | |SQUAR| STA      | |SQUAR| Supply   | |SQUAR| System   |
| chematic          |                  | Download         |                  | Voltage          | R                |
|                   |                  |                  |                  |                  | eference         |
|                   |                  |                  |                  |                  | Guide            |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Verilog   | |SQUAR| Watchdog | |SQUAR| Windows  |                  |                  |                  |
|                   |                  | Platform         |                  |                  |                  |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| **Kit             |                  |                  |                  |                  |                  |
| Tags**            |                  |                  |                  |                  |                  |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| CAN /     | |SQUAR| CapSense | |SQUAR| CY8      | |SQUAR| CY8      | |SQUAR| CY8      | |SQUAR| Digital  |
| LIN EBK           | E                | CKIT-001         | CKIT-030         | CKIT-042         | Audio            |
|                   | xpansion         | Kit              | / 050            | Kit              | EBK              |
|                   | EBK              |                  | Kit              |                  |                  |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| First     | |SQUAR| LCD      | |SQUAR| MFI EBK  | |SQUAR| Power    | |SQUAR| PSoC     | |SQUAR| Thermal  |
| Touch             | Segment          |                  | Su               | 3/4/5            | Ma               |
| Kit               | Drive            |                  | pervisor         | P                | nagement         |
|                   | EBK              |                  | EBK              | rocessor         | EBK              |
|                   |                  |                  |                  | Module           |                  |
+-------------------+------------------+------------------+------------------+------------------+------------------+

**Touch Sensing Tags**

+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| ADC      | |SQUAR| Air gap  | |SQUAR| Back     | |SQUAR| Bleeder  | |SQUAR| Bo       | |SQUAR| CMOD     |
|                  |                  | lighting         | Resistor         | otloader         |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| CSA      | |SQUAR| CSD      | |SQUAR| CSD      | |SQUAR| CSD2X    | |SQUAR| CSDADC   | |SQUAR| CSDAUTO  |
|                  |                  | Pa               |                  |                  |                  |
|                  |                  | rameters         |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| CY3203A  | |SQUAR| CY3213A  | |SQUAR| CY3214   | |SQUAR| CY3218   | |SQUAR| CY32     | |SQUAR| CY32     |
|                  |                  |                  |                  | 80-20x34         | 80-20xx6         |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| CY32     | |SQUAR| C        | |SQUAR| C        | |SQUAR| C        | |SQUAR| CY       | |SQUAR| C        |
| 80-21x34         | Y8C20x34         | Y8C20xx6         | Y8C21x34         | 8C21xxx-         | Y8C24x94         |
|                  |                  |                  |                  | CapSense         |                  |
|                  |                  |                  |                  | Express          |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| CY8      | |SQUAR| CapSense | |SQUAR| Circuit  | |SQUAR| Co       | |SQUAR| Confi    | |SQUAR| D        |
| CMBR2044         | Express          | Housing          | nductive         | guration         | iplexing         |
|                  |                  |                  | Objects          |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Dynamic  | |SQUAR| EEPROM   | |SQUAR| EMI      | |SQUAR| ESD      | |SQUAR| Errors   | |SQUAR| FR4      |
| Reconfi          |                  |                  |                  |                  |                  |
| guration         |                  |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Filters  | |SQUAR| Finger   | |SQUAR| Flex PCB | |SQUAR| I2C      | |SQUAR| I2C-USB  | |SQUAR| IDAC     |
|                  | T                |                  |                  | Bridge           |                  |
|                  | hreshold         |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| IMO and  | |SQUAR| ITO      | |SQUAR| Layout   | |SQUAR| Metal    | |SQUAR| Noise    | |SQUAR| Overlay  |
| P                |                  | Gu               |                  |                  |                  |
| rescaler         |                  | idelines         |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| PSoC3    | |SQUAR| P        | |SQUAR| Pa       | |SQUAR| Power    | |SQUAR| P        | |SQUAR| SNR      |
| CapSense         | arasitic         | thfinder         | Con              | roximity         |                  |
|                  | Cap              |                  | sumption         |                  |                  |
|                  | acitance         |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| SPI      | |SQUAR| Scanning | |SQUAR| S        | |SQUAR| Sensors  | |SQUAR| Shield   | |SQUAR| Sliders  |
|                  | Te               | chematic         |                  |                  |                  |
|                  | chniques         |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Sm       | |SQUAR| Tuning   | |SQUAR| UART     | |SQUAR| Water    | |SQUAR| Water    |                  |
| artSense         |                  |                  |                  | Proofing         |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+

**USB Controllers Tags**

+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| 8051     | |SQUAR| AN2131   | |SQUAR| AT2LP    | |SQUAR| ATA /    | |SQUAR| ATA      | |SQUAR| Asyn     |
|                  |                  |                  | ATAPI            | Commands         | chronous         |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Auto     | |SQUAR| B        | |SQUAR| Blaster  | |SQUAR| Bulk     | |SQUAR| Bus      | |SQUAR| C#       |
| Mode             | andwidth         |                  | Transfer         | Power            |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| C++      | |SQUAR| CAT5     | |SQUAR| CF Card  | |SQUAR| CY3216   | |SQUAR| CY3649   | |SQUAR| CY3654   |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| CY3655   | |SQUAR| CY3660   | |SQUAR| CY3662   | |SQUAR| CY3664   | |SQUAR| CY3674   | |SQUAR| CY3681   |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| CY3684   | |SQUAR| CY3685   | |SQUAR| CY3686   | |SQUAR| CY4605   | |SQUAR| CY4606   | |SQUAR| CY4611B  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| CY4615   | |SQUAR| CYUSB    | |SQUAR| Clock    | |SQUAR| Co       | |SQUAR| Control  | |SQUAR| Control  |
|                  |                  |                  | mpliance         | Center           | Transfer         |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Crystal  | |SQUAR| C        | |SQUAR| DLL      | |SQUAR| Debug    | |SQUAR| Des      | |SQUAR| Driver   |
|                  | yConsole         |                  |                  | criptors         |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| EEPROM   | |SQUAR| EZ-HOST  | |SQUAR| EZ-OTG   | |SQUAR| EZ-USB   | |SQUAR| E        | |SQUAR| EnCoreII |
|                  |                  |                  |                  | mulation         |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| E        | |SQUAR| EnCoreV  | |SQUAR| Encore   | |SQUAR| Endpoint | |SQUAR| Enu      | |SQUAR| Errata   |
| nCoreIII         |                  |                  |                  | meration         |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| FIFO     | |SQUAR| FX       | |SQUAR| FX1      | |SQUAR| FX2      | |SQUAR| FX2LP    | |SQUAR| Firmware |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Firmware | |SQUAR| Flags    | |SQUAR| F        | |SQUAR| Full     | |SQUAR| GPIF     | |SQUAR| HDD      |
| Debug            |                  | ramework         | Speed            |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| HID      | |SQUAR| HX2      | |SQUAR| HX2LP    | |SQUAR| Hi-Lo    | |SQUAR| High     | |SQUAR| Host     |
|                  |                  |                  | Pr               | Speed            | App              |
|                  |                  |                  | ogrammer         |                  | lication         |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Hub      | |SQUAR| I2C      | |SQUAR| ICE      | |SQUAR| IN       | |SQUAR| I        | |SQUAR| In       |
|                  |                  |                  | Transfer         | nterrupt         | terrupts         |
|                  |                  |                  |                  | Transfer         |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Iso      | |SQUAR| Keil     | |SQUAR| Keyboard | |SQUAR| Layout   | |SQUAR| Library  | |SQUAR| Loader   |
| chronous         |                  |                  |                  |                  |                  |
| Transfer         |                  |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Low      | |SQUAR| M8A      | |SQUAR| M8B      | |SQUAR| Manual   | |SQUAR| Mass     | |SQUAR| Memory   |
| Speed            |                  |                  | Mode             | Storage          |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Mouse    | |SQUAR| NX2LP    | |SQUAR| NX       | |SQUAR| Nand     | |SQUAR| Nand     | |SQUAR| OTP      |
|                  |                  | 2LP-Flex         | Flash            | Manuf            |                  |
|                  |                  |                  |                  | acturing         |                  |
|                  |                  |                  |                  | Utility          |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| OUT      | |SQUAR| Port IO  | |SQUAR| Register | |SQUAR| Renu     | |SQUAR| Report   | |SQUAR| Reset    |
| Transfer         |                  |                  | meration         |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| SFR      | |SQUAR| SIE      | |SQUAR| SL811HS  | |SQUAR| SPI      | |SQUAR| SX2      | |SQUAR| S        |
|                  |                  |                  |                  |                  | chematic         |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| S        | |SQUAR| Screamer | |SQUAR| Script   | |SQUAR| Self     | |SQUAR| Slave    | |SQUAR| S        |
| chematic         |                  |                  | Power            | FIFO             | treaming         |
| Review           |                  |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| SuiteUSB | |SQUAR| Syn      | |SQUAR| TX2      | |SQUAR| TX2UL    | |SQUAR| Tetra    | |SQUAR| Th       |
|                  | chronous         |                  |                  | Hub              | roughput         |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Timer    | |SQUAR| UART     | |SQUAR| UDMA     | |SQUAR| USBFS    | |SQUAR| USBUART  | |SQUAR| U        |
|                  |                  |                  |                  |                  | SBSerial         |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Vendor   | |SQUAR| Video    | |SQUAR| WHQL     | |SQUAR| WLK      | |SQUAR| cyapi    | |SQUAR| uVision  |
| Command          | Class            |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| **Super          |                  |                  |                  |                  |                  |
| Speed**          |                  |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| FX3      | |SQUAR| ADMux    | |SQUAR| ARM926EJ | |SQUAR| Bo       | |SQUAR| DMA      | |SQUAR| Eclipse  |
|                  |                  | -S               | otloader         |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| FX3 GPIO | |SQUAR| FX3      | |SQUAR| FX3      | |SQUAR| FX3 SDK  | |SQUAR| GPIF II  | |SQUAR| HS-OTG   |
|                  | Power            | Power            |                  |                  |                  |
|                  | Man              | supply           |                  |                  |                  |
|                  | agement          |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| JTAG     | |SQUAR| LPP      | |SQUAR| MSC      | |SQUAR| Os       | |SQUAR| RTOS     | |SQUAR| SD Card  |
|                  |                  |                  | cillator         |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| S        | |SQUAR| USB 3.0  | |SQUAR| USB      | |SQUAR| USB Host | |SQUAR| UVC      |                  |
| lavefifo         |                  | Co               |                  |                  |                  |
|                  |                  | mpliance         |                  |                  |                  |
|                  |                  | Test             |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| FX3S     | |SQUAR| eMMC     | |SQUAR| SDIO     | |SQUAR| S-Ports  | |SQUAR| Bay      | |SQUAR| Benicia  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| HX3      | |SQUAR| ASSP     | |SQUAR| Battery  | |SQUAR| Bo       | |SQUAR| C        | |SQUAR| EEPROM   |
|                  |                  | charging         | otloader         | ortex-M0         |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| GPIOs    | |SQUAR| H        | |SQUAR| H        | |SQUAR| I2C      | |SQUAR| I        | |SQUAR| Os       |
|                  | X3 Power         | X3 Power         |                  | n-system         | cillator         |
|                  | Man              | supply           |                  | pro              |                  |
|                  | agement          |                  |                  | gramming         |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| USB 3.0  | |SQUAR| USB      |                  |                  |                  |                  |
| hub              | Shared           |                  |                  |                  |                  |
|                  | Link™            |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+

**Automotive MCU Tags (16FX/FR/FCR4/Traveo)**

+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Traveo   | |SQUAR| S6J3110  | |SQUAR| S6J3120  | |SQUAR| S6J3200  | |SQUAR| S6J3300  | |SQUAR| S6J3310  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| S6J3320  | |SQUAR| S6J3330  | |SQUAR| S6J3340  | |SQUAR| S6J3350  | |SQUAR| S6J3400  | |SQUAR| S6J3510  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| S6J326C  | |SQUAR| S6J324C  | |SQUAR| S6J327C  | |SQUAR| S6J328C  | |SQUAR| S6J32DA  | |SQUAR| S6J32BA  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| S6J3360  | |SQUAR| S6J3370  | |SQUAR| S6J32G0  | |SQUAR| MB9D560  | |SQUAR| CY9D560  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| FR81S    | |SQUAR| MB91520  | |SQUAR| MB91550  | |SQUAR| MB91580  | |SQUAR| MB91570  | |SQUAR| MB91590  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| CY91520  | |SQUAR| CY91550  | |SQUAR| CY91580  | |SQUAR| CY91570  | |SQUAR| CY91590  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| FCR4     | |SQUAR| MB9DF125 | |SQUAR| MB9DF126 | |SQUAR| MB9EF226 | |SQUAR| CY9DF125 | |SQUAR| CY9DF126 |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| CY9EF226 |                  |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| 16FX     | |SQUAR| MB96600  | |SQUAR| CY96600  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Power    | |SQUAR| Current  | |SQUAR| PSS      | |SQUAR| Low      | |SQUAR| Mode     | |SQUAR| Clock    |
|                  |                  |                  | Power            |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Reset    | |SQUAR| External | |SQUAR| DDR      | |SQUAR| Hyper    |                  |                  |
|                  | Bus              | HSSPI            | Bus              |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| CPU      | |SQUAR| MPU      | |SQUAR| PPU      | |SQUAR| TPU      |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Flash    | |SQUAR| RAM      | |SQUAR| Backup   | |SQUAR| DMA      | |SQUAR| I        |                  |
|                  |                  | RAM              |                  | nterrupt         |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Port     | |SQUAR| A/D      | |SQUAR| D/A      | |SQUAR| Timer    | |SQUAR| Base     | |SQUAR| UDC/QPRC |
|                  | C                | C                |                  | Timer            |                  |
|                  | onverter         | onverter         |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Real     | |SQUAR| Watchdog |                  |                  |                  |                  |
| Time             |                  |                  |                  |                  |                  |
| clock            |                  |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| SHE      | |SQUAR| Security | |SQUAR| BootROM  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Multi-   | |SQUAR| UART     | |SQUAR| CSIO     | |SQUAR| I2C      | |SQUAR| CAN      | |SQUAR| CAN FD   |
| Function         |                  |                  |                  |                  |                  |
| Serial           |                  |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| LIN      | |SQUAR| FlexRay  | |SQUAR| Ethernet |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| CRC      |                  |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Graphics | |SQUAR| Sound    | |SQUAR| SMC      | |SQUAR| LCDC     | |SQUAR| Media LB | APIX             |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Audio    | |SQUAR| I2S      | |SQUAR| PCMPWM   |                  |                  |                  |
| DAC              |                  |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Motor    | |SQUAR| Waveform | |SQUAR| RDC      | |SQUAR| MVA      |                  |                  |
| Control          | G                |                  |                  |                  |                  |
|                  | enerator         |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| AUTOSAR  | |SQUAR| Safety   |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Ev       | |SQUAR| Flash    | |SQUAR| Tool     | |SQUAR| Document | |SQUAR| Board    |                  |
| aluation         | Pr               |                  |                  |                  |                  |
| Kit              | ogrammer         |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+

**Automotive MCU Tags (Traveo II)**

+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Traveo   |                  |                  |                  |                  |                  |
| II               |                  |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Body     | |SQUAR| Gateway  | |SQUAR| Chassis  | |SQUAR| Cluster  |                  |                  |
| Control          |                  |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| CYT2B7   |                  |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Power    | |SQUAR| Power    | |SQUAR| Chip     | |SQUAR| Clock    | |SQUAR| Reset    |                  |
|                  | mode             | o                |                  |                  |                  |
|                  |                  | peration         |                  |                  |                  |
|                  |                  | mode             |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| CPU      | |SQUAR| MPU      | |SQUAR| SMPU     | |SQUAR| PPU      | |SQUAR| SWPU     | |SQUAR| IPC      |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Flash    | |SQUAR| SRAM     | |SQUAR| ROM      | |SQUAR| DMA      | |SQUAR| I        |                  |
|                  |                  |                  |                  | nterrupt         |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| IO       | |SQUAR| ADC      | |SQUAR| TCPWM    | |SQUAR| WDT      | |SQUAR| RTC      | |SQUAR| Trigger  |
| S                |                  |                  |                  |                  | Mul              |
| ubsystem         |                  |                  |                  |                  | tiplexer         |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| CRYPTO   | |SQUAR| EVTGEN   | |SQUAR| EFUSE    | |SQUAR| Boot     |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| SCB      | |SQUAR| LIN      | |SQUAR| CAN/CAN  | |SQUAR| CXPI     |                  |                  |
|                  |                  | FD               |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Fault    | |SQUAR| Program  |                  |                  |                  |                  |
|                  | and              |                  |                  |                  |                  |
|                  | Debug IF         |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| AUTOSAR  | |SQUAR| Safety   |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Ev       | |SQUAR| Flash    | |SQUAR| Tool     | |SQUAR| Document | |SQUAR| Board    |                  |
| aluation         | Pr               |                  |                  |                  |                  |
| Kit              | ogrammer         |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+

**Automotive Power Management ICs (PMICs)Tags**

+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| PMIC     | |SQUAR| DC/DC    | |SQUAR| LDO      | |SQUAR| Buck     | |SQUAR| Boost    | |SQUAR| Bu       |
|                  | c                |                  |                  |                  | ck-boost         |
|                  | onverter         |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| S6BP201A | |SQUAR| S6BP202A | |SQUAR| S6BP203A | |SQUAR| S6BP401A | |SQUAR| S6BP501A | |SQUAR| S6BP502A |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Channel  | |SQUAR| Input    | |SQUAR| Output   | |SQUAR| Load     | |SQUAR| Q        | |SQUAR| Ef       |
|                  | voltage          | voltage          | current          | uiescent         | ficiency         |
|                  |                  |                  |                  | current          |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| A        | |SQUAR| Fixed    | |SQUAR| Syn      | |SQUAR| Current  | |SQUAR| S        | |SQUAR| So       |
| utomatic         | PWM              | chronous         | mode             | witching         | ft-start         |
| PWM/PFM          |                  |                  |                  | f                |                  |
|                  |                  |                  |                  | requency         |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Cold     | |SQUAR| MOSFET   | |SQUAR| Sc       | |SQUAR| PCB      | |SQUAR| Current  | |SQUAR| Line     |
| Crank            |                  | hematics         | Layout           | Sense            | Re               |
|                  |                  |                  |                  |                  | gulation         |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Load     | |SQUAR| Watchdog | |SQUAR| Power    | |SQUAR| UVLO     | |SQUAR| Thermal  | |SQUAR| SSCG     |
| re               | timer            | good             |                  | re               |                  |
| gulation         |                  |                  |                  | sistance         |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| OCP      | |SQUAR| OVP      | |SQUAR| Thermal  | |SQUAR| Ripple   | |SQUAR| Gain     | |SQUAR| Phase    |
|                  |                  | shutdown         |                  | margin           | margin           |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Osc      | |SQUAR| Junction | |SQUAR| Ambient  | |SQUAR| BOM      | |SQUAR| Phase    | |SQUAR| Tem      |
| illation         | tem              | tem              |                  | comp             | perature         |
|                  | perature         | perature         |                  | ensation         | rise             |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| EMI      | |SQUAR| R        | |SQUAR| Co       | |SQUAR| Snubber  |                  |                  |
|                  | adiation         | nduction         |                  |                  |                  |
|                  | noise            | noise            |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+

**Automotive CXPI Tags**

+----------------------------+--------------------+------------+------------+------------+
| |SQUAR| CXPI Transceiver   |                    |            |            |            |
+----------------------------+--------------------+------------+------------+------------+
| |SQUAR| S6BT112A01         | |SQUAR| S6BT112A02 |            |            |            |
+----------------------------+--------------------+------------+------------+------------+

**Knowledge Base Article Type:** [Select the category by clicking the
checkbox associated]

+------------------------------------+------------------------------------------+
| |SQUAR| **KB Type-1**              | |SQUAR| **KB Type-2**                    |
+====================================+==========================================+
| |SQUAR| **Compiler**               | |SQUAR| **ByteCraft**                    |
|                                    +------------------------------------------+
|                                    | |SQUAR| **Imagecraft**                   |
|                                    +------------------------------------------+
|                                    | |SQUAR| **Keil**                         |
|                                    +------------------------------------------+
|                                    | |SQUAR| **HiTech**                       |
+------------------------------------+------------------------------------------+
| |SQUAR| **Component Development**  | |SQUAR| **Training/Things you should     |
|                                    | know**                                   |
|                                    +------------------------------------------+
|                                    | |SQUAR| **Component Architecture**       |
|                                    +------------------------------------------+
|                                    | |SQUAR| **Component/Project management** |
|                                    +------------------------------------------+
|                                    | |SQUAR| **Datapath**                     |
|                                    +------------------------------------------+
|                                    | |SQUAR| **Analog components**            |
|                                    +------------------------------------------+
|                                    | |SQUAR| **Digital components**           |
|                                    +------------------------------------------+
|                                    | |SQUAR| **Component software/tools**     |
|                                    +------------------------------------------+
|                                    | |SQUAR| **Component Testing**            |
+------------------------------------+------------------------------------------+
| |SQUAR| **Development Kits**       | |SQUAR| **FirstTouch Kit**               |
+------------------------------------+------------------------------------------+
| |CRSQR| **Development Tools**      |                                          |
+------------------------------------+------------------------------------------+
| |SQUAR| **Device Drivers**         | |SQUAR| **Pullability**                  |
|                                    +------------------------------------------+
|                                    | |SQUAR| **Mass Storage**                 |
+------------------------------------+------------------------------------------+
| |SQUAR| **Device Programming**     | |SQUAR| **PSoC Programmer**              |
+------------------------------------+------------------------------------------+
| |SQUAR| **Documentation**          |                                          |
+------------------------------------+------------------------------------------+
| |SQUAR| **Firmware**               |                                          |
+------------------------------------+------------------------------------------+
| |SQUAR| **General**                |                                          |
+------------------------------------+------------------------------------------+
| |SQUAR| **Hardware**               | |SQUAR| **Digital**                      |
|                                    +------------------------------------------+
|                                    | |SQUAR| **Specifications**               |
|                                    +------------------------------------------+
|                                    | |SQUAR| **Analog**                       |
+------------------------------------+------------------------------------------+
| |SQUAR| **Known Problems and       |                                          |
| Solutions**                        |                                          |
+------------------------------------+------------------------------------------+
| |SQUAR| **Microcontrollers**       | |SQUAR| **8051**                         |
|                                    +------------------------------------------+
|                                    | |SQUAR| **M8**                           |
|                                    +------------------------------------------+
|                                    | |SQUAR| **M8C**                          |
|                                    +------------------------------------------+
|                                    | |SQUAR| **Traveo**                       |
|                                    +------------------------------------------+
|                                    | |SQUAR| **Traveo II**                    |
|                                    +------------------------------------------+
|                                    | |SQUAR| **FCR4**                         |
|                                    +------------------------------------------+
|                                    | |SQUAR| **FR**                           |
|                                    +------------------------------------------+
|                                    | |SQUAR| **16FX**                         |
+------------------------------------+------------------------------------------+
| |SQUAR| **Modules**                | |SQUAR| **Automotive Power Management    |
|                                    | ICs (PMICs)**                            |
|                                    +------------------------------------------+
|                                    | |SQUAR| **Automotive CXPI**              |
+------------------------------------+------------------------------------------+
| |SQUAR| **Platforms**              | |SQUAR| **MacOS X**                      |
|                                    +------------------------------------------+
|                                    | |SQUAR| **Windows**                      |
+------------------------------------+------------------------------------------+
| |SQUAR| **Protocols**              | |SQUAR| **HID**                          |
|                                    +------------------------------------------+
|                                    | |SQUAR| **I2C**                          |
+------------------------------------+------------------------------------------+
| |SQUAR| **Quality**                |                                          |
+------------------------------------+------------------------------------------+
| |SQUAR| **Reference Designs**      |                                          |
+------------------------------------+------------------------------------------+
| |SQUAR| **Software**               | |SQUAR| **PSoC Designer**                |
|                                    +------------------------------------------+
|                                    | |SQUAR| **PSoC Creator**                 |
+------------------------------------+------------------------------------------+
| |SQUAR| **User Modules**           | |SQUAR| **Analog**                       |
|                                    +------------------------------------------+
|                                    | |SQUAR| **Digital**                      |
+------------------------------------+------------------------------------------+

Document History 
================

Document Title: Eclipse IDE Survival Guide - KBA225399

Document Number: 002-25399

+------+---------+-----------------+------------------------+
| Rev. | ECN No. | Orig. of Change | Description of Change  |
+------+---------+-----------------+------------------------+
| \*\* | 6347912 | JETT            | Initial release        |
+------+---------+-----------------+------------------------+
| \*A  | 6361330 | JETT            | New                    |
|      |         |                 |                        |
|      |         |                 | -  A symbol (e.g. bool |
|      |         |                 |    or uint32_t) is     |
|      |         |                 |    unresolved, what    |
|      |         |                 |    do I do             |
|      |         |                 |                        |
|      |         |                 | -  Can I change build  |
|      |         |                 |    settings at the     | 
|      |         |                 |    application         |
|      |         |                 |    level               |
|      |         |                 |                        |
|      |         |                 | Updated                |
|      |         |                 |                        |
|      |         |                 | -  How do I get        |
|      |         |                 |    started building    |
|      |         |                 |    an application      |
|      |         |                 |                        |
|      |         |                 | -  How do I pick a     |
|      |         |                 |    compiler > my       |
|      |         |                 |    version of the      |
|      |         |                 |    GNU ARM             |
|      |         |                 |    toolchain           |
|      |         |                 |                        |
|      |         |                 | -  Several questions   |
|      |         |                 |    involving access    |
|      |         |                 |    to the project      |
|      |         |                 |    Properties          |
|      |         |                 |    window              |
|      |         |                 |                        |
|      |         |                 | -  How do I stop       |
|      |         |                 |    debugging but       |
|      |         |                 |    leave the           |
|      |         |                 |    processor           |
|      |         |                 |    running –           |
|      |         |                 |    updated the         |
|      |         |                 |    screenshot          |
+------+---------+-----------------+------------------------+
| \*B  | 6374261 | JETT            | New                    |
|      |         |                 |                        |
|      |         |                 | -  What is the         |
|      |         |                 |    difference          |
|      |         |                 |    between a local     |
|      |         |                 |    file and a          |
|      |         |                 |    linked file,        |
|      |         |                 |                        |
|      |         |                 | -  When I try to       |
|      |         |                 |    program or debug    |
|      |         |                 |    I get a             |
|      |         |                 |    connection          |
|      |         |                 |    error.              |
|      |         |                 |                        |
|      |         |                 | -  Can I change the    |
|      |         |                 |    build               |
|      |         |                 |    configuration       |
|      |         |                 |    for every           |
|      |         |                 |    project in the      |
|      |         |                 |    application?        |
|      |         |                 |                        |
|      |         |                 | ..                     |
|      |         |                 |                        |
|      |         |                 |    Updated             |
|      |         |                 |                        |
|      |         |                 | -  Relocated some      |
|      |         |                 |    questions for       |
|      |         |                 |    better flow         |
|      |         |                 |                        |
|      |         |                 | -  Can I change build  |
|      |         |                 |    settings at the     |
|      |         |                 |    application         |
|      |         |                 |    level               |
|      |         |                 |                        |
|      |         |                 | -  Added reminder to   |
|      |         |                 |    apply build         |
|      |         |                 |    settings changes    |
|      |         |                 |    to all build        |
|      |         |                 |    configurations      |
|      |         |                 |                        |
|      |         |                 | -  How do I start a    |
|      |         |                 |    debug session       |
+------+---------+-----------------+------------------------+
| \*C  | 6379442 | JETT            | New                    |
|      |         |                 |                        |
|      |         |                 | -  Can I use an        |
|      |         |                 |    Eclipse IDE with    |
|      |         |                 |    a version           |
|      |         |                 |    control system?     |
|      |         |                 |                        |
|      |         |                 | Updated                |
|      |         |                 |                        |
|      |         |                 | -  Removed             |
|      |         |                 |    WICED-related       |
|      |         |                 |    content and         |
|      |         |                 |    references          |
+------+---------+-----------------+------------------------+
| \*D  | 6386724 | JETT            | Updated                |
|      |         |                 |                        |
|      |         |                 | -  When I try to       |
|      |         |                 |    program or debug    |
|      |         |                 |    I get a             |
|      |         |                 |    connection          |
|      |         |                 |    error.              |
|      |         |                 |                        |
|      |         |                 | ..                     |
|      |         |                 |                        |
|      |         |                 |    Added the KitProg   |
|      |         |                 |    firmware is out of  |
|      |         |                 |    date error, and how |
|      |         |                 |    to fix it           |
+------+---------+-----------------+------------------------+
| \*E  | 6436263 | JETT            | New                    |
|      |         |                 |                        |
|      |         |                 | -  How do I step       |
|      |         |                 |    through assembly    |
|      |         |                 |    code?               |
|      |         |                 |                        |
|      |         |                 | -  I like the Debug    |
|      |         |                 |    perspective         |
|      |         |                 |    better. How do I    |
|      |         |                 |    use it by           |
|      |         |                 |    default?            |
|      |         |                 |                        |
|      |         |                 | Updated                |
|      |         |                 |                        |
|      |         |                 | -  How do I share an   |
|      |         |                 |    application and     |
|      |         |                 |    its projects?       |
+------+---------+-----------------+------------------------+
| \*F  | 6480310 | JETT            | Changes for            |
|      |         |                 | ModusToolbox 1.1       |
|      |         |                 |                        |
|      |         |                 | Updated                |
|      |         |                 |                        |
|      |         |                 | -  screenshots where   |
|      |         |                 |    UI changes          |
|      |         |                 |    require             |
|      |         |                 |                        |
|      |         |                 | -  question on “what   |
|      |         |                 |    do I build” to      |
|      |         |                 |    reflect simpler     |
|      |         |                 |    project             |
|      |         |                 |    structure.          |
|      |         |                 |                        |
|      |         |                 | -  questions that      |
|      |         |                 |    mention multiple    |
|      |         |                 |    projects, where     |
|      |         |                 |    use case still      |
|      |         |                 |    applies             |
|      |         |                 |                        |
|      |         |                 | Removed questions on   |
|      |         |                 |                        |
|      |         |                 | -  changing device or  |
|      |         |                 |    platform            |
|      |         |                 |                        |
|      |         |                 | -  working sets and    |
|      |         |                 |    working with        |
|      |         |                 |    multiple            |
|      |         |                 |    projects            |
|      |         |                 |                        |
|      |         |                 | -  multiple instances  |
|      |         |                 |    of                  |
|      |         |                 |    GenerateSource      |
|      |         |                 |                        |
|      |         |                 | -  building multiple   |
|      |         |                 |    projects – no       |
|      |         |                 |    longer a common     |
|      |         |                 |    use case.           |
|      |         |                 |                        |
|      |         |                 | -  handling the        |
|      |         |                 |    console log for     |
|      |         |                 |    multiple            |
|      |         |                 |    projects.           |
|      |         |                 |                        |
|      |         |                 | -  dual core debugging |
+------+---------+-----------------+------------------------+
| \*G  | 6504782 | JETT            | **Updated**            |
|      |         |                 |                        |
|      |         |                 | -  How do I see        |
|      |         |                 |    variables,          |
|      |         |                 |    registers, and      |
|      |         |                 |    memory              |
|      |         |                 |                        |
|      |         |                 | **New**                |
|      |         |                 |                        |
|      |         |                 | -  How do I see local  |
|      |         |                 |    variables           |
|      |         |                 |                        |
|      |         |                 | -  How do I see global |
|      |         |                 |    variables           |
|      |         |                 |                        |
|      |         |                 | -  How do I set up     |
|      |         |                 |    dependencies        |
|      |         |                 |    between projects    |
|      |         |                 |                        |
|      |         |                 | **Restored & updated   |
|      |         |                 | (from Rev \*E)**       |
|      |         |                 |                        |
|      |         |                 | -  What is a working   |
|      |         |                 |    set and why         | 
|      |         |                 |    would I want to     |
|      |         |                 |    use one             |
|      |         |                 |                        |
|      |         |                 | -  How do I create a   |
|      |         |                 |    working set         |
+------+---------+-----------------+------------------------+
| \*H  | 6529185 | JETT            | **New**                |
|      |         |                 |                        |
|      |         |                 | -  How do I start      |
|      |         |                 |    debugging           |
|      |         |                 |    without             |
|      |         |                 |    downloading the     |
|      |         |                 |    executable again    |
+------+---------+-----------------+------------------------+
| \*I  | 6703812 | JETT            | **New**                |
|      |         |                 |                        |
|      |         |                 | -  How do I link to    |
|      |         |                 |    files instead of    |
|      |         |                 |    duplicating         |
|      |         |                 |    files               |
|      |         |                 |                        |
|      |         |                 | -  Project Creator     |
|      |         |                 |    questions           |
|      |         |                 |                        |
|      |         |                 | -  Library manager     |
|      |         |                 |    questions           |
|      |         |                 |                        |
|      |         |                 | **Updates throughout   |
|      |         |                 | for MTB 2.0**          |
|      |         |                 |                        |
|      |         |                 | -  how to find and     |
|      |         |                 |    instantiate code    |
|      |         |                 |    examples and        |
|      |         |                 |    applications        |
|      |         |                 |                        |
|      |         |                 | -  eliminate the       |
|      |         |                 |    concept of          |
|      |         |                 |    multiple            |
|      |         |                 |    projects and a      |
|      |         |                 |    main app            |
|      |         |                 |                        |
|      |         |                 | -  relocated some      |
|      |         |                 |    questions to        |
|      |         |                 |    keep the same       |
|      |         |                 |    concepts            |
|      |         |                 |    together            |
|      |         |                 |                        |
|      |         |                 | -  renamed the         |
|      |         |                 |    question on the     |
|      |         |                 |    Quick Access        |
|      |         |                 |    panel               |
|      |         |                 |                        |
|      |         |                 | -  eliminate most      |
|      |         |                 |    discussion of       |
|      |         |                 |    build               |
|      |         |                 |    configurations –    |
|      |         |                 |    handled in the      |
|      |         |                 |    makefile            |
|      |         |                 |                        |
|      |         |                 | -  eliminate           |
|      |         |                 |    discussion of       |
|      |         |                 |    “no rule to make    |
|      |         |                 |    target” because     |
|      |         |                 |    the Eclipse         |
|      |         |                 |    auto-generated      |
|      |         |                 |    makefile is no      |
|      |         |                 |    longer created      |
|      |         |                 |    or used             |
+------+---------+-----------------+------------------------+
| \*J  | 6731619 | JETT            | -  minor fixes to UI   |
|      |         |                 |    terminology and     |
|      |         |                 |    grammar             |
|      |         |                 |                        |
|      |         |                 | -  added info on how   |
|      |         |                 |    to find Library     |
|      |         |                 |    Manager, and        |
|      |         |                 |    link to             |
|      |         |                 |    documentation       |
|      |         |                 |                        |
|      |         |                 | -  updated “How to     |
|      |         |                 |    create a working    |
|      |         |                 |    set” to make it     |
|      |         |                 |    more clear          |
+------+---------+-----------------+------------------------+
| \*K  | 6834521 | JETT            | -  removed most info   |
|      |         |                 |    unique to           |
|      |         |                 |    Eclipse IDE for     |
|      |         |                 |    ModusToolbox and    |
|      |         |                 |    point to User       |
|      |         |                 |    Guide as single     |
|      |         |                 |    source of truth.    |
|      |         |                 |                        |
|      |         |                 | -  Updated question on |
|      |         |                 |    renaming            |
|      |         |                 |                        |
|      |         |                 | -  added some very     |
|      |         |                 |    general guidance    |
|      |         |                 |    on using a          |  
|      |         |                 |    version control     |
|      |         |                 |    system              |
|      |         |                 |                        |
|      |         |                 | -  Added question on   |
|      |         |                 |    what to do when     |
|      |         |                 |    launch              |
|      |         |                 |    configurations      |
|      |         |                 |    disappear.          |
|      |         |                 |                        |
|      |         |                 | -  Minor wording       |
|      |         |                 |    enhancements and    |
|      |         |                 |    updates             |
|      |         |                 |    throughout for      |
|      |         |                 |    ModusToolbox 2.1    |
+------+---------+-----------------+------------------------+
| \*L  | 6885560 | JETT            | -  Substituted         |
|      |         |                 |    “ModusToolbox”      |
|      |         |                 |    for PSoC 6 MCU      |
|      |         |                 |    in one place,       |
|      |         |                 |    MTB will soon       |
|      |         |                 |    support PSoC 4.     |
|      |         |                 |    Trivial change.     |
|      |         |                 |                        |
|      |         |                 | -  Corrected a typo    |
|      |         |                 |    under “How do I     |
|      |         |                 |    share a             |
|      |         |                 |    project?”, In       |
|      |         |                 |    the first           |
|      |         |                 |    bullet, "file"      |
|      |         |                 |    was spelled as      |
|      |         |                 |    “fie”.              |
+------+---------+-----------------+------------------------+
| \*M  | 6897500 | JETT            | -  Added               |
|      |         |                 |    question/answer     |
|      |         |                 |    on duplicate        |
|      |         |                 |    launch              |
|      |         |                 |    configurations      |
+------+---------+-----------------+------------------------+

.. |image0| image:: ../_static/image/tool-guide/Eclipse-IDE-Survival-Guide/image1.png
.. |image1| image:: ../_static/image/tool-guide/Eclipse-IDE-Survival-Guide/image2.png
.. |image2| image:: ../_static/image/tool-guide/Eclipse-IDE-Survival-Guide/image3.png
.. |image3| image:: ../_static/image/tool-guide/Eclipse-IDE-Survival-Guide/image4.png
.. |image4| image:: ../_static/image/tool-guide/Eclipse-IDE-Survival-Guide/image5.png
.. |image5| image:: ../_static/image/tool-guide/Eclipse-IDE-Survival-Guide/image6.png
.. |image6| image:: ../_static/image/tool-guide/Eclipse-IDE-Survival-Guide/image7.png
.. |image7| image:: ../_static/image/tool-guide/Eclipse-IDE-Survival-Guide/image8.png
.. |image8| image:: ../_static/image/tool-guide/Eclipse-IDE-Survival-Guide/image9.png
.. |image9| image:: ../_static/image/tool-guide/Eclipse-IDE-Survival-Guide/image10.png
.. |image10| image:: ../_static/image/tool-guide/Eclipse-IDE-Survival-Guide/image11.png
.. |image11| image:: ../_static/image/tool-guide/Eclipse-IDE-Survival-Guide/image12.png
.. |image12| image:: ../_static/image/tool-guide/Eclipse-IDE-Survival-Guide/image13.png
.. |image13| image:: ../_static/image/tool-guide/Eclipse-IDE-Survival-Guide/image14.png
.. |image14| image:: ../_static/image/tool-guide/Eclipse-IDE-Survival-Guide/image15.png
.. |image15| image:: ../_static/image/tool-guide/Eclipse-IDE-Survival-Guide/image16.png
.. |image16| image:: ../_static/image/tool-guide/Eclipse-IDE-Survival-Guide/image17.png
.. |image17| image:: ../_static/image/tool-guide/Eclipse-IDE-Survival-Guide/image18.png
.. |image18| image:: ../_static/image/tool-guide/Eclipse-IDE-Survival-Guide/image19.png
.. |image19| image:: ../_static/image/tool-guide/Eclipse-IDE-Survival-Guide/image20.png
.. |image20| image:: ../_static/image/tool-guide/Eclipse-IDE-Survival-Guide/image21.png
.. |image21| image:: ../_static/image/tool-guide/Eclipse-IDE-Survival-Guide/image22.png
.. |image22| image:: ../_static/image/tool-guide/Eclipse-IDE-Survival-Guide/image23.png
.. |image23| image:: ../_static/image/tool-guide/Eclipse-IDE-Survival-Guide/image24.png
.. |image24| image:: ../_static/image/tool-guide/Eclipse-IDE-Survival-Guide/image25.png
.. |image25| image:: ../_static/image/tool-guide/Eclipse-IDE-Survival-Guide/image26.png
.. |image26| image:: ../_static/image/tool-guide/Eclipse-IDE-Survival-Guide/image27.png
.. |image27| image:: ../_static/image/tool-guide/Eclipse-IDE-Survival-Guide/image28.png
.. |image28| image:: ../_static/image/tool-guide/Eclipse-IDE-Survival-Guide/image29.png
.. |image29| image:: ../_static/image/tool-guide/Eclipse-IDE-Survival-Guide/image30.png
.. |image30| image:: ../_static/image/tool-guide/Eclipse-IDE-Survival-Guide/image31.png
.. |image31| image:: ../_static/image/tool-guide/Eclipse-IDE-Survival-Guide/image32.png
.. |image32| image:: ../_static/image/tool-guide/Eclipse-IDE-Survival-Guide/image33.png

