<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="indexpage" kind="group">
    <compoundname>index</compoundname>
    <title>CYBLE-416045-EVAL BSP</title>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
<para><heading level="2">Overview</heading>
</para><para>The EZ-BLE Arduino Evaluation Board (CYBLE-416045-EVAL) enables you to evaluate and develop applications on the CYBLE-416045-02 EZ-BLE Creator Module. CYBLE-416045-EVAL can be used as a standalone evaluation kit or can be combined with Arduino compatible shields. <image name="board.png" type="html" />
</para><para>To use code from the BSP, simply include a reference to cybsp.h.</para><para><heading level="2">Features</heading>
</para><para><bold>Kit Features:</bold>
</para><para><itemizedlist>
<listitem><para>BLE 5.0 certified Cypress CYBLE-416045-02 EZ-BLE module with onboard crystal oscillators, trace antenna, passive components and PSoC 63 MCU</para></listitem><listitem><para>Up to 36 Arduino-compatible GPIO headers</para></listitem><listitem><para>Supports digital programmable logic, a PDM-PCM digital microphone interface, high-performance analog-to-digital converter (ADC), low-power comparators, and standard communication and timing peripherals.</para></listitem></itemizedlist>
</para><para><bold>Kit Contents:</bold>
</para><para><itemizedlist>
<listitem><para>EZ-BLE Arduino Evaluation Board (CYBLE-416045-EVAL) with on-board EZ-BLE Creator Module (CYBLE-416045-02)</para></listitem><listitem><para>USB Type-A to Micro-USB cable</para></listitem><listitem><para>Quick Start Guide</para></listitem></itemizedlist>
</para><para><heading level="2">BSP Configuration</heading>
</para><para>The BSP has a few hooks that allow its behavior to be configured. Some of these items are enabled by default while others must be explicitly enabled. Items enabled by default are specified in the CYBLE-416045-EVAL.mk file. The items that are enabled can be changed by creating a custom BSP or by editing the application makefile.</para><para>Components:<itemizedlist>
<listitem><para>Device specific HAL reference (e.g.: PSOC6HAL) - This component, enabled by default, pulls in the version of the HAL that is applicable for this board.</para></listitem><listitem><para>BSP_DESIGN_MODUS - This component, enabled by default, causes the Configurator generated code for this specific BSP to be included. This should not be used at the same time as the CUSTOM_DESIGN_MODUS component.</para></listitem><listitem><para>CUSTOM_DESIGN_MODUS - This component, disabled by default, causes the Configurator generated code from the application to be included. This assumes that the application provides configurator generated code. This should not be used at the same time as the BSP_DESIGN_MODUS component.</para></listitem></itemizedlist>
</para><para>Defines:<itemizedlist>
<listitem><para>CYBSP_WIFI_CAPABLE - This define, disabled by default, causes the BSP to initialize the interface to an onboard wireless chip.</para></listitem><listitem><para>CY_USING_HAL - This define, enabled by default, specifies that the HAL is intended to be used by the application. This will cause the BSP to include the applicable header file and to initialize the system level drivers.</para></listitem></itemizedlist>
</para><para><bold>Clock Configuration</bold>
</para><para><verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Clock  &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Source  &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Output Frequency   &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CLK_HF0  &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;CLK_PATH0  &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;100 MHz   &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim></para><para><bold>Power Configuration</bold>
</para><para><itemizedlist>
<listitem><para>System Active Power Mode: LP</para></listitem><listitem><para>System Idle Power Mode: Deep Sleep</para></listitem><listitem><para>VDDA Voltage: 3300 mV</para></listitem><listitem><para>VDDD Voltage: 3300 mV</para></listitem></itemizedlist>
</para><para><heading level="2">API Reference Manual</heading>
</para><para>The CYBLE-416045-EVAL Board Support Package provides a set of APIs to configure, initialize and use the board resources.</para><para>See the <ulink url="modules.html">BSP API Reference Manual</ulink> for the complete list of the provided interfaces.</para><para><heading level="2">More information</heading>
</para><para><itemizedlist>
<listitem><para><ulink url="modules.html">CYBLE-416045-EVAL BSP API Reference Manual</ulink></para></listitem><listitem><para><ulink url="https://www.cypress.com/documentation/development-kitsboards/cyble-416045-eval-ez-ble-arduino-evaluation-board">CYBLE-416045-EVAL Documentation</ulink></para></listitem><listitem><para><ulink url="http://www.cypress.com">Cypress Semiconductor, an Infineon Technologies Company</ulink></para></listitem><listitem><para><ulink url="https://github.com/cypresssemiconductorco">Cypress Semiconductor GitHub</ulink></para></listitem><listitem><para><ulink url="https://www.cypress.com/products/modustoolbox-software-environment">ModusToolbox</ulink></para></listitem></itemizedlist>
</para><para><hruler />
 © Cypress Semiconductor Corporation, 2019-2020. </para>    </detaileddescription>
  </compounddef>
</doxygen>