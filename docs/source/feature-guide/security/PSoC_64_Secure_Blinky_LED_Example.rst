==================================
PSoC 64 Secure Blinky LED Example
==================================

This code example demonstrates the implementation of a simple FreeRTOS task, which toggles an LED periodically using PSoC :sup:`®` 64 MCU. 
 
.. raw:: html

   <a href="https://github.com/cypresssemiconductorco/mtb-example-psoc6-secure-blinkyled-freertos" target="_blank">Click here to be taken to the project on GitHub.</a><br><br>
