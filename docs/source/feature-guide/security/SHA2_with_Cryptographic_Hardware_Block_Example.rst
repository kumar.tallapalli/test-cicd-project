===============================================
SHA2 with Cryptographic Hardware Block Example
===============================================

This code example shows how to generate a 32-byte hash value or message digest for an arbitrary user input message with the SHA2 algorithm using the Cryptographic hardware block in PSoC :sup:`®` 6 MCU. The example further shows that any change in the message results in a unique hash value for the message. The hash value generated for the message is displayed on a UART terminal emulator. 

.. raw:: html

   <a href="https://github.com/cypresssemiconductorco/mtb-example-psoc6-crypto-sha" target="_blank">Click here to be taken to the project on GitHub.</a><br><br>
