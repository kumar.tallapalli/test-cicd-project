==========================
Secure Boot SDK User Guide
==========================

Introduction
==============
Cypress provides the Secure Boot SDK to simplify using the PSoC 64
Secure MCU line of devices. This SDK includes all required libraries,
tools, and sample code to provision and develop applications for PSoC 64
MCUs.

The Secure Boot SDK provides provisioning scripts with sample keys and
policies, a pre-built Cypress Secure Bootloader image, and post-build
tools for signing firmware images. It uses the Python programming
language.

Where to Get the Secure Boot SDK
--------------------------------

The main component of the SDK is a Python package called CySecureTools.
It is available for download here:

   https://github.com/cypresssemiconductorco/cysecuretools

There are other components as well, and they are described in later
sections of this document.

Using this Guide
-----------------

This guide provides a high-level overview of the Secure Boot SDK,
including details on how the provisioning process works, as well as
descriptions of the provided scripts and tools. In addition, this guide
provides a reference to the tokens/JSON structures used in the SDK.

This guide assumes you are familiar with the concept of public key
cryptography, public/private key pairs, and digital signatures. An
overview of these ideas is available here:

   https://en.wikipedia.org/wiki/Public-key_cryptography

Definition of Terms
-------------------

-  **Root-of-Trust (RoT):** This is an immutable process or identity
   used as the first entity in a trust chain. No ancestor entity can
   provide a trustable attestation (in digest or other form) for the
   initial code and data state of the RoT.

-  **Hardware Security Module (HSM):** A physical computing device that
   safeguards and manages digital keys for strong authentication, and
   that provides cryptographic processing. In the context of the PSoC 64
   Secure MCU, the HSM is a device programming engine placed in a
   physically secure facility.

-  **Provisioning:** The process by which keys, policies and secrets are
   injected into the device. Once provisioned, the device can be
   accessed or modified only with the keys injected adhering to the
   relevant policies.

-  **JSON:** JavaScript Object Notation (JSON) is an open-standard file
   format that uses human-readable text to transmit data objects
   consisting of attribute–value pairs and array data types (or any
   other serializable value).

-  **JWT:** JSON Web Token (JWT) is an open, industry standard RFC 7519
   method to securely represent claims between two parties.

-  **JWK:** JSON Web Key (JWK) is a RF7517 compliant data structure that
   represents a cryptographic key.

-  **Policies:** Policies are a collection of pre-defined (name, value)
   pairs that describe what is and is not allowed on the device. Most
   policies are enforced during boot-time by the RoT firmware in the
   device, some can be interpreted and enforced by higher layers of
   software like Cypress Secure Bootloader.

-  **Secure Boot:** Refers to a bootup process where the firmware being
   run by the chip is trusted by using strong cryptographic schemes and
   an immutable RoT.

-  **Immutable Boot Code:** Refers to the first piece of code which is
   run after chip power-on before any user application is run. In the
   context of the PSoC6 family, it refers to the ROM and Flash code
   which is programmed at Cypress manufacturing and made immutable by
   transitioning life-cycle stages.

-  **SWD:** Single Wire Debug, a two wire debug port defined for Arm
   Cortex CPU’s

-  **CMSIS-DAP:** CMSIS-DAP is a specification and a implementation of a
   Firmware that supports access to the CoreSight Debug Access Port
   (DAP).

-  **DAPLink:** Arm Mbed DAPLink is an open-source software project that
   enables programming and debugging application software running on Arm
   Cortex CPUs.

-  **KitProg3:** This is Cypress’ low-level communication firmware for
   programming and debugging. It runs on a PSoC 5LP device. It is a is a
   multi-functional system, which uses SWD for programming and
   debugging, and provides a USB-I2C bridge, and USB-UART bridge. It
   supports CMSIS-DAP and DAPLink.

-  **SMIF:** Serial Memory interface. In the context of this user guide,
   it refers to the highspeed Quad-SPI interface on PSoC 6.

-  **Rollback Counter:** Special counter accessed by secure boot code
   that holds the value of the latest valid image and used in an
   anti-rollback protection mechanism. The goal of anti-rollback
   protection is to prevent downgrading the device to an older version
   of its software that has been deprecated due to security concerns.

Revision History
----------------

+------------------------------------------------------------------------------+
|| **Document Title: PSoC\ ® 64 Secure MCU Secure Boot SDK User Guide**        |   
|| **Document Number: 002-27860**                                              |
+-----------------+-------------+----------------------------------------------+
|    **Revision** |    **Date** |    **Description of Change**                 |     
+=================+=============+==============================================+
|    \*\*         |    7/19/19  |    New document.                             |  
+-----------------+-------------+----------------------------------------------+
|    \*A          |    9/18/19  |    Updates to change to CySecureTools flow.  |    
+-----------------+-------------+----------------------------------------------+
|    \*B          |    12/4/19  |    Updates to include ModusToolbox 2.0 flow. |   
+-----------------+-------------+----------------------------------------------+
|    \*C          |    05/18/20 |    Updated for production silicon            |     
+-----------------+-------------+----------------------------------------------+
|    \*D          |    07/23/20 |    Added Mbed OS flow, general cleanup       |   
+-----------------+-------------+----------------------------------------------+

2. Overview
============

The PSoC 64 Secure MCU line, based on the PSoC 6 MCU platform, features
out-of-box security functionality. The line provides an isolated RoT
with true attestation and provisioning services. In addition, these MCUs
deliver a pre-configured secure execution environment which supports
system software for various IoT platforms and provides:

-  Secure provisioning 

-  Secure storage

-  Secure firmware management

To develop with a PSoC 64 Secure MCU, you first provision the MCU with
keys and policies. You then program the device with signed firmware.
Otherwise the device will not boot up correctly. The Secure Boot SDK
provides development tools to demonstrate the provisioning and signing
flow.

In addition, Cypress Secure Bootloader enables Secure Boot and Firmware
updates.

Secure Boot SDK Components
--------------------------

The Secure Boot SDK is organized as a stand-alone python CySecureTools
package, which contains all the required scripts, default provisioning
packets, and the default policy file, as follows:

+----------------------------------+----------------------------------+
|    **Component**                 |    **Purpose**                   |
+==================================+==================================+
|    Command line tool             |    Allows using CySecureTools as |
|                                  |    command-line utility to       |
|                                  |    perform all required          |
|                                  |    operations                    |
+----------------------------------+----------------------------------+
|    Provisioning Scripts          |    Python scripts for            |
|                                  |    provisioning the PSoC 64      |
|                                  |    Secure MCU. Scripts are based |
|                                  |    on Python.                    |
+----------------------------------+----------------------------------+
|    Entrance Exam Scripts         |    Runs an entrance exam on the  |
|                                  |    PSoC 64 Secure MCU to ensure  |
|                                  |    no tampering has occurred.    |
+----------------------------------+----------------------------------+
|    Cypress Secure Bootloader     |    The first stage bootloader    |
|    Image                         |    based on an open source       |
|                                  |    MCUBoot :sup:`[1]` library.   |
+----------------------------------+----------------------------------+
|    Sample Provisioning Policies  |    Examples to be used as        |
|                                  |    templates for forming         |
|                                  |    provisioning tokens.          |
+----------------------------------+----------------------------------+


How Does the Secure Boot SDK Work?
----------------------------------

The goal for a developer creating a design using a secure device is to
ensure that the software running on it is authorized and unchanged. The
CySecureTools package provides the tools you use to make that happen.
This section describes what you do and how it works at the highest
level. Subsequent sections in this User Guide provide details.

CySecureTools provides a reference implementation for the patent-pending
process developed by Cypress for securing the software on a device. It
is based on industry-best practices in public-key infrastructure (PKI),

cryptography, and digital signing. From factory to bootup to remote
updates, every step in the process is signed and verified.

Every secure MCU is embedded with a Cypress-owned Root of Trust (RoT)
when it comes out of the factory. This RoT is based on a public key that
is owned by Cypress.

When an OEM customer purchases devices, there is a secure process to
transfer the RoT to the OEM. This process replaces the Cypress public
key with the OEM public key. From that point on, the OEM “owns” the
device and the secure device will only accept any further
security-related interaction if it is appropriately signed by the OEM.

For Secure Boot functionality, Cypress provides immutable boot code
programmed on factory that launches a Cypress Secure bootloader. That
bootloader is itself signed, so that the boot code can verify the
integrity of the bootloader. This bootloader then verifies the OEM
application firmware that should run on the device before launching that
code. Cypress provides a secure bootloader as part of CySecureTools, but
a customer can use their own.

To work with Secure devices, the OEM provides three “things”:

-  A set of cryptographic keys, the public key of which will be used for
   validating OEM application firmware 
   
-  A set of security policies that define how the secure chip should behave
   
-  Certificates (optional) used to bind device identity or provide a
   chain-of-trust to a higher certifying authority

This information, along with the bootloader, is securely injected into
the device before firmware is programmed. This process is called
**provisioning**. This information provides the rules that the ROM boot
code follows when launching the bootloader, and provides the resources
required to verify the authenticity of the code.

The OEM develops the software to run on the device and digitally signs
the application using a private key that corresponds to the keys
provisioned in the device. With policies and keys in place, the ROM boot
code verifies the bootloader, which verifies the signature and the
integrity of any code before launching it.

|image1|

CySecureTools includes:

   Provisioning scripts (in Python) that provide an API for tasks such
   as

   -  Creating the required keys

   -  Specifying security policies and debug access

   -  Provisioning the device

   -  Forming device identity certificate

   -  Enabling secure debug

   The Cypress Secure Bootloader (as a binary image with an associated
   certificate)
   
   An optional entrance exam step to ensure device
   integrity

Sample provisioning policies

What is Provisioning?
----------------------

Provisioning is a process whereby secure assets like keys and security
policies are injected into the device. During development, a software
team can manage this. During production, this step typically occurs in a
secure manufacturing environment that has a Hardware Security Module
(HSM). For the PSoC 64 Secure MCU, provisioning involves the following
steps:

-  `Transferring the RoT <#transferring-rot>`__ from Cypress to the development
   user (called OEM in this document).

-  Injecting user assets such as image-signing keys, device security
   policies, and certificates into the device.

Transferring RoT
*****************

Every PSoC 64 Secure MCU has a Cypress public key placed in the part
during manufacturing. This Cypress public key acts as the initial RoT
for the device after it is manufactured.

The RoT transfer process can be represented as a series of trust claims;
exchanged between the following entities:

-  Cypress – The owner of the Cypress Root private key.

-  Secure Manufacturing environment HSM – The entity authorized to
   provision and program the PSoC 64 Secure MCU.

-  OEM/Developer – The user/code developer of the part.

-  PSoC 64 Secure MCU – The holder of the Cypress Root public key.


The following illustration shows a high-level view:

|image2|

The series of steps to transfer the root-of-trust include:

1. Cypress authorizes the HSM to provision a part.

2. The OEM/User authorizes the same HSM to provision the part with
   credentials and firmware.

3. The HSM then presents the above authorization objects to the PSoC 64
   Secure MCU.

4. The PSoC 64 Secure MCU verifies authorization signatures and claims.
   If all are valid, the chip accepts the OEM RoT public key and
   allows the HSM to send provisioning packets.

The end result of this RoT transfer process can be represented as
follows:

-  The PSoC 64 Secure MCU now uses the OEM RoT public key as the root
   key used to validate any OEM asset (image keys, policies etc.). This
   permanently and irrevocably replaces the Cypress RoT.

-  The PSoC 64 Secure MCU now trusts the HSM public key and expects all
   further provisioning packets to be signed by the corresponding HSM
   private key.



The actual authorization objects for the PSoC 64 Secure MCU are
represented using the JSON Web Token (JWT) format. A simplified view of
the flow of the Cypress and the OEM authorizing a HSM is shown in the
following diagram:

|image3|



The final output of this process generates the following JWTs:

-  **cy_auth JWT:** Contains the public key of the HSM to be trusted.
   Additional fields such as an expiration date can be specified to
   limit this token’s use.

-  **rot_auth JWT:** Contains the public key of the HSM to be trusted as
   well as the OEM RoT public key to which the RoT must be transferred.

The HSM then presents these tokens to the chip, as shown in the
following diagram:

|image4|

After the root-of-trust packet is sent, the device also generates a
unique device key pair and exports the generated device public key and
it’s unique ID. This combination can be used to chain the identity of
the chip to a Certifying authority trusted by the OEM.

Injecting User Assets
**********************

After the RoT is transferred to the OEM RoT public key, the user can
inject several assets into the device. These include:

-   Public Keys

    -  Image public key – Used by the Bootloader to check the next image
       signature. 
	   
-   Device Policies

    -  Boot & Upgrade policy – Specifies which regions of flash constitute a
       bootloader and launch image, as well as the key associated when
       validating the flash area.

    -  Debug policy – Specifies the behavior of the device debug ports
       (CM0+/CM4/SYSAP). Also, specifies the device behavior when
       transitioning into RMA mode.

-   Chain-of-Trust Certificates

    -  Any certificates needed on the device; for example, device
       certificate for TLS or Identity.

Both public keys and device policies are present in a JWT token called
‘prov_req.JWT.’ They are signed by the OEM RoT private key.

The certificates present in the chain-of-trust may be signed by the same
key, but no restrictions are placed on this field’s contents and the
chain-of-trust is considered an opaque object.

|image5|

In addition to the OEM assets, Cypress Secure Bootloader is programmed
at this stage, along with the Bootloader Certificate (called
'image_cert.JWT’) that has the signature of the Cypress Secure
Bootloader binary. This ensures that the bootloader itself can be
verified and trusted. For more details on Cypress Secure Bootloader, see
the `Cypress Secure Bootloader <#cypress-secure-bootloader>`__ section.

For more details on the exact provisioning packets, see the
`Provisioning script flow details <#modustoolbox-provisioning-flow>`__ section.

Re-provisioning User Assets
****************************

The PSoC 64 Secure MCU’s also allow some user assets to be
re-provisioned, if allowed by the initial policy provisioned into the
device.

The following assets are allowed to be re-provisioned

-  Cypress Secure Bootloader

-  Public keys, Policies and Chain-of-trust certificate

All other assets such as the OEM RoT public key, HSM public key and
device unique keys cannot be replaced using re-provisioning.

Cypress Secure Bootloader
-------------------------

The Cypress Secure Bootloader is included as a pre-built hex image. This
image acts as the first image securely launched by the PSoC 64 Secure
MCU boot code. The Cypress Secure Bootloader is based on an open source
library MCUBoot and is capable of parsing the provisioned Boot&Upgrade
policy and launch next image if all required checks pass. For more
details about this open source library, refer to the `MCUBoot Bootloader
design <https://juullabs-oss.github.io/mcuboot/design.html>`__ website.

.. note::
   The current version of Cypress Secure Bootloader does not
   support the swap-based images as documented in the MCUBoot design
   document.

The Cypress Secure Bootloader supports external memory over the PSoC64
Serial Memory Interface (SMIF). The bootloader currently only supports
external memory vendors who support the Serial Flash Discovery Protocol
(SFDP).

The Cypress Secure Bootloader is capable of independently managing up to
two user images for use cases where the Secure Processing Environment
(SPE) code such as Trusted Firmware-M and Non-Secure Processing (NSPE)
code needs to be independently updated with individual Boot and Upgrade
slots.

The Cypress Secure Bootloader also enforces the protection contexts for
the bootloader code, so code running on another protection context
cannot overwrite/tamper with the boot code. The following diagram shows
the launch code sequence of Cypress Secure Bootloader:

|image6|

During a normal bootup, the Cypress Secure Bootloader performs the
following operations:

-  Reads the policies and parses them for further use

-  Checks if the upgrade slot is located in external memory and performs
   SMIF initialization correspondingly
   
-  Checks if the Boot Area (Slot 0) contains an image to boot
 
-  Verifies that this image has the valid format

-  Verifies the image's digital signature

-  Verifies that the image `rollback counter <#definition-of-terms>`__ is greater than
   or equal to the value saved in the rollback protection counter of the
   boot code data

-  Checks if the Staging Area (Slot 1) has an image for upgrade 

-  Boots Slot 0 if no correct image is found in the Staging Area

If Staging area (Slot 1) has a new image, the Cypress Secure Bootloader
performs the following operations:

-  Verifies the digital signature of the image located in Slot 1

-  Decrypts the image’s body and verifies the digital signature of the
   decrypted image (optional for the encrypted image support)

-  Checks that the corresponding policies allow upgrade

-  Checks that the image metadata matches the image in Slot 0, then
   upgrades it 
   
-  Rewrites Slot 0 with the decrypted (if needed) Slot 1
   image

-  Invalidates Slot 1 by erasing the header and trailer (hash and
   signature) sections, so that at the next reset Slot 1 is ignored

The following diagram shows a typical application update scenario using
the Cypress Secure Bootloader:

|image7|


CySecureTools Installation and Documentation
---------------------------------------------

A stand-alone python package, CySecureTools contains all necessary
scripts, default provisioning packets, and a set of default policy
files. It implements most of the Secure Boot SDK functionality.
CySecureTools is written in the Python language and requires interpreter
versions higher than 3.7.

CySecureTools source code is available on GitHub. Full details about the
operations, commands, and APIs available can be found on:

   https://github.com/cypresssemiconductorco/cysecuretools/blob/master/README.md/

Use these instructions to install and configure CySecureTools:

1. Install Python 3.7.4 or later on your computer. You download it from
   https://www.python.org/downloads/ or install it using the packet
   manager of host system.


   .. note::
      Python 3.8.x has known compatibility issues with Mbed OS
      5.x. Cypress recommends to use Python 3.7.x versions to ensure the
      best compatibility across all development flows

2. Set up the appropriate environment variable(s) for your operating
   system.

   **Windows**

   a. Open the Control Panel, go to **System > Advanced System Settings >
   Environment Variables**.

   b. Find “PATH” in the list of user variables, and click **Edit**.

   c. Add the folder that contains the *python.exe* file to the system
   variable “Path.” For example: *C:\Python37*

   d. Add the Python *Scripts* subfolder to the system variable “Path.” For
   example: *C:\Python37\Scripts*

   If Python 2.7 is also installed, make sure that Python37 and
   Python37\Scripts have higher priority in the PATH than C:\Python27.
   Just move them closer to the top of the list.

   **Linux**

   Most distributions of Linux should already have python2 and python3
   installed. To verify that python by default points to python3 run:

   
      python --version

   If python3 is not set as default, run the following commands. The
   number at the end of each command denotes a priority:

      update-alternatives --install /usr/bin/python python/usr/bin/python2.7 1 

      update-alternatives --install /usr/bin/python python /usr/bin/python3.7 2

   **macOS**

   By default, 'python' points to /usr/bin/python, which is python2. To
   make 'python' and 'pip' resolve to python3 versions, execute the
   following from command line:

       echo 'alias python=python3' >> ~/.bash_profile

       echo 'alias pip=pip3' >> ~/.bash_profile

       source ~/.bash_profile

       python --version

       Python 3.7.4

       pip --version

       pip 19.0.3 from/Library/Frameworks/Python.framework/Versions/3.7/lib/python3.7/site-packages/pip(python 3.7)
   
   .. note::
      If you use a shell other than bash, update its profile file
      accordingly. For example ~/.zshrc if you use zsh instead of
      ~/.bash_profile.


3. Install the CySecureTools package (part of the Secure Boot SDK). Run
   the following command in your terminal window:

       python -m pip install cysecuretools

   .. note::
      During installation, you may see errors when installing
      colorama, protobuf and jsonschema. These can be safely ignored.

   You can use the following command to show the path to the installed
   package 
	  
      python -m pip show cysecuretools

4. CySecureTools use the pyOCD package, which has a dependency on
   libusb. Follow the latest instructions in the `pyOCD
   readme. <https://github.com/mbedmicro/pyOCD#libusb-installation>`__


   The following are instructions for the currently recommended version:

   **Windows**

    a. Download and unzip libusb-1.0.21.7z from
    https://github.com/libusb/libusb/releases/tag/v1.0.21

    b. Copy libusb-1.0.dll file into the Python root folder (in same folder
    with python.exe). Use the 64-bit version of DLL for the 64-bit
    Python and the 32-bit version of DLL for the 32-bit Python.

   **Linux**

   Use the host system packet manager to install the driver using a
   terminal. For example,runt he following for

   Ubuntu:

        apt-get install libusb

   This command requires sudo.

   **Mac OS**

   Use the `homebrew <https://brew.sh/>`__ packet manager to install the
   driver using terminal:

        homebrew install libusb



3. Mbed OS – Provisioning Flow
===============================

This section shows how to provision the CY8CKIT-064B0S2-4343W kit in
Mbed OS using CySecureTools.

Prerequisites
-------------

-  CySecureTools: Refer to the instructions in `CySecureTools
   Installation and Documentation. <#cysecuretools-installation-and-documentation>`__

-  Mbed Installation: Install Mbed CLI tools using the instructions at
   the `Mbed OS website. <https://os.mbed.com/docs/mbed-os/latest/tools/manual-installation.html>`__

Device Provisioning
-------------------

For evaluation purposes, the device provisioning flow can be done on
your local development environment. For evaluation, a pre-signed
development token is available in the SDK that authorizes a HSM key-pair
provided in the SDK.

|image8|

Execute all the following processes (A through D) from a command line.

The path to the policy file can be relative to the current working
directory or absolute. All paths to key files inside the policy file are
relative to the path mentioned in the policy. For a detailed description
of what the default policy file looks like, refer to the `Understanding
the Default policy <#understanding-the-default-policy>`__ section.


A. Set Up Mbed OS Example Project Folder:
*****************************************

1. Open native command-line application, for example on Windows 10:

   |image9|

2. Import the mbed-os-example-blinky project using the following command: 
   :: 

     mbed import mbed-os-example-blinky

3. Navigate to the *mbed-os\targets\TARGET_Cypress\TARGET_PSOC6\TARGET_CY8CKIT_064B0S2_4343W* 
   directory. For example on Windows 10:

   |image10|

4. Run the following command:

   ::

     \*Example usage for the CY8CKIT-064B0S2-4343W kit\*

     cysecuretools -t cyb06xxa init

**What does this step do?**

CySecureTools provides default policies and other secure assets that
can be used to quickly set up the chip with development parameters,
like leaving the CM4 DAP (Debug Access Port) open to reprogram the
chip.

Based on the selected target, this step sets up all the necessary
files in your workspace that are used for subsequent steps.

.. note::
   CySecureTools is developed to have a chip family level of
   abstraction. In this particular case, the cyb06xxa is a family of PSoC
   64 with 2 megabytes of flash. This chip is installed on the
   CY8CKIT-064B0S2-4343W.

B. Generate New Keys:
**********************

1. Ensure you are in the *mbed-*os\targets\TARGET_Cypress\TARGET_PSOC6\TARGET_CY8CKIT_064B0S2_4343W*
   directory.

2. Type (or copy) the following command:

   ::

     \*Example usage for the CY8CKIT-064B0S2-4343W kit\*

     cysecuretools -t cyb06xxa -p policy\policy_single_CM0_CM4.json
     create-keys

**What does this step do?**

CySecureTools reads the provided policy and generates the keys
defined.

Depending on the policy chosen, there can be multiple keys generated
under the /keys/ folder. By default only one key, the
USERAPP_CM4_KEY, a P-256 Elliptic curve key-pair is generated.

CySecureTools generates keys in two formats, PEM and JSON. Both the
PEM and JSON files represent the same key.

C. Run Entrance Exam (Optional)
*******************************

1. Connect the kit to your PC.

   .. attention::

      The KitProg3 must be in DAPLink mode for performing
      this step. Press the ‘Mode’ button on the kit until the Status LED
      blinks fast. For more details, refer to the `KitProg3 User
      Guide. <https://www.cypress.com/file/452701/download>`__


2. Type (or copy) the following command:

   ::

     \*Example usage for the CY8CKIT-064B0S2-4343W kit\*

     cysecuretools -t cyb06xxa entrance-exam


**What does this step do?**

The Entrance exam is a test routine that does the following things:

-  Verify that the Device is in the correct lifecycle stage
 
-  Verify that Boot Code has not been modified/tampered

-  Verify that User flash is empty and no code is running before any
   provisioning takes place

   Failing the entrance exam returns an error in the command line. If
   there is any firmware running on the device, CySecureTools will give
   an option to erase the chip. Existing firmware can be erased using
   tools like Cypress Programmer.

   Note that the entrance exam is also run automatically before
   performing provisioning, so you can skip this step if needed.

D. Perform provisioning
************************

.. attention::

   KitProg3 must be in DAPLink mode. **The kit supply voltage
   must be 2.5 V to perform this step**.

Refer to the relevant kit user guide to find out how to change the
supply voltage of your kit.

1. Ensure you are in the *mbed-*
   *os\targets\TARGET_Cypress\TARGET_PSOC6\TARGET_CY8CKIT_064B0S2_4343W*
   directory.

2. Type (or copy) the following command:

   ::

      \*Example usage for the CY8CKIT-064B0S2-4343W kit\*

      cysecuretools -t cyb06xxa -p policy\policy_single_CM0_CM4.json
      provision-device

**What does this step do?**

The CySecureTools provision-device API does the following steps:

1) Reads the provided policy and forms the final provisioning packet,
   named prov_cmd.jwt

2) Performs the entrance exam

3) Provisions the device by sending the prov_cmd.jwt to the PSoC64
   Secure MCU.

Before running this step, you can modify the default policy to match
your end use-case. For most development use-cases, you don’t need to
modify it. Please see `Understanding the Default policy. <#understanding-the-default-policy>`__

Device Re-provisioning
-----------------------

The default device policy templates provided with the CySecureTools
package allows you to re-provision a device after running through the
provisioning steps.

To re-provision a device, follow the steps for the normal provisioning
flow (see `Device Provisioning <#device-provisioning>`__), then run this command:

::

   \*Example usage to re-provision the CY8CKIT-064B0S2-4343W kit\*

   cysecuretools -t cyb06xxa -p policy\policy_single_CM0_CM4.json
   re-provision-device

When re-provisioning a device, the Entrance exam step is not run again.

In case of failure at re-provisioning, see `Re-provisioning After
Failure. <#re-provisioning-after-failure>`__

Building the Mbed OS Blinky Application
---------------------------------------

1. Navigate back to your mbed-os-example-blinky root folder and build
   the application using the following command:

   ::

      \*Example usage for the CY8CKIT-064B0S2-4343W kit\* 

      mbed compile -m CY8CKIT_064B0S2_4343W -t GCC_ARM

   .. note::
      On Windows, there is a known issue related to the command
      line buffer size limit of 32k, causing a build to fail. If this
      occurs, use the following command:

   ::

      \*Example usage for the CY8CKIT-064B0S2-4343W kit\*

      mbed compile –-build build/CY8CKIT_064B0S2_4343W -m
      CY8CKIT_064B0S2_4343W -t GCC_ARM

   .. note::
      The *./mbed-os/build* folder will be used as the output
      directory instead of the default *./mbed-os/BUILD*.

2. Program the built hex file located as follows:
 
      .. rst-class:: nocss

      *<Example Project>\BUILD\CY8CKIT_064B0S2_4343W\<project name>.hex*

   Or run this command:

   ::

      mbed compile -m CY8CKIT_064B0S2_4343W -t GCC_ARM -f

3. Reset the chip and observe blinking LED.

Debugging Application Code
--------------------------

Currently, Mbed OS export to the Eclipse IDE for ModusToolbox is not
fully supported for PSoC 64 based kits. This section describes how to
use the compiled ELF file and PyOCD GDB server to debug the code.

Prerequisites
**************

The following are required in order to debug application code:

-  Application image is compiled and programmed into the device. The
   **mbed-os-example-blinky** is used as an example
   `(https://github.com/ARMmbed/mbed-os-example-blinky) <https://github.com/ARMmbed/mbed-os-example-blinky>`__.

-  ModusToolbox IDE Version 2.0 or higher is installed on your machine.

-  libusb driver is installed.

   -  For **Windows**:

      -  Download and unzip libusb-1.0.21.7z from
         `https://github.com/libusb/libusb/releases/tag/v1.0.21. <https://github.com/libusb/libusb/releases/tag/v1.0.21>`__

      -  Copy libusb-1.0.dll file into Python 3.7 folder (use 64-bit
         version of the DLL for 64-bit Python and 32-bit version of the
         DLL for 32-bit Python).

      -  Make sure Python path located at the beginning of Path environment
         variable.

   -  For **Linux / mac OS**: Use package manager to install libusb.

Create Empty C/C++ Application
*******************************

1. Open the Eclipse IDE for ModusToolbox.

2. Go to **File->New->Project**.

3. Under **C/C++** select **C/C++ Project** and press **Next**.

4. Under **New C/C++ Project** select **C Managed Build** and press
   **Next**.

5. Fill in **Project name** and press **Next**, then **Next**, and
   **Finish**.

Configure PyOCD GDB Server Path
********************************

On the **Window** menu item, choose **Preferences > MCU > Global pyOCD
Path** and set:

-  **Executable**: pyocd-gdbserver

-  **Folder**: C:\Python37-32\Scripts (align the path to Scripts
   directory in your python installation)

Setting up PyOCD Configuration for Debugging
*********************************************

1. Open **Run > Debug Configurations**.

2. Right click on GDB PyOCD Debugging and select **New**.

3. Configure the **Main** tab:

   a. Set Project to **EmptyProject**.

   b. Ensure the C/C++ Application points to the .elf file of the
      application.

   c. Select **Disable auto build** check box to prevent the app
      building in the IDE.

      |image12|

4. Configure the **Debugger** tab:

   a. Ensure **GDB port** is **3333.**

   b. Ensure **semihosting port** is **4444.**

   c. **Set Flash mode:** to **"Sector erase"**

      .. warning::
   
         Setting any other option breaks the silicon

   d. Add two commands in **Other options** text edit:

      **-p 3332**

      **-T 4443**

      |image13|


5. Configure the **Startup** tab: Unselect the **Load executable** check
   box.

   |image14|

6. Debug configuration is ready. Click **Apply**, then **Debug**.


Mbed OS Secure Image Generation
--------------------------------

In Mbed OS, PSoC 64 based kit targets have post-build signing scripts
set up so the output binary is formatted and signed automatically
according to:

-  The provisioned policy file; for example, policy_single_CM0_CM4.json.

-  The targets.json file in <mbed-os>/targets/ folder.

The following diagram shows the flow for signing and encryption using
Mbed OS.

|image15|


The build process outputs two binaries:

-  Signed boot image hex file. This is the exact binary which can be
   programmed to Slot#0 for the PSoC 64 to securely launch the
   application.

-  Signed and encrypted (policy dependent) update image hex file. This
   is the exact binary that can be programmed to Slot#1 for the PSoC 64
   to perform a secure update and then launch the application.

The following are descriptions of the *targets.json* fields:

-  " hex_filename”: Specifies name of CM0+ hex file to be used

-  “cm0_img_id”: Specifies image-id to be passed to the CySecureTools
   Sign() API when signing the CM0+ image

-  “cm4_img_id”: Specifies image-id to be passed to the CySecureTools
   Sign() API when signing the CM4 Application image

-  “boot_scheme”:

   -  single_image – Embeds the CM0+ Image into the mbed-os CM4 application
      on compilation stage, then signs single combined image

   -  multi_image – Signs the CM0+ Image and mbed-os CM4 application image
      separately and then merges them into one

-  “policy_file”: Specifies path to the policy file to be used

Signing Generic Images
----------------------

A generic HEX file (for example one that is produced by Mbed OS or any
other build system) can also be converted into a signed/encrypted image
by executing the following command:

::

   \*Sign (and encrypt) image:\*

   cysecuretools -t cyb06xxa sign-image --hex example-blinky.hex
   \-\-image\-type BOOT


4. ModusToolbox – Provisioning Flow
====================================

This section shows how to provision the CY8CKIT-064B0S2-4343W kit in
ModusToolbox using CySecureTools.

Prerequisites
-------------

ModusToolbox Installation
**************************

Install the ModusToolbox software. Refer to the `ModusToolbox
Installation Guide. <https://www.cypress.com/file/492936/download>`__

.. note:: 
   On Linux machines after installing ModusToolbox, run the
   *ModusToolbox/tools_2.x/modus-shell/postinstall* script.

CySecureTools Installation
***************************

Follow the instructions in the `CySecureTools Installation and
Documentation <#cysecuretools-installation-and-documentation>`__ section.

Device Provisioning
-------------------

For evaluation, device provisioning can be done in your local
development environment, rather than in a secure manufacturing facility.
For evaluation, a pre-signed development token is available in the SDK
which authorizes a HSM key-pair provided in the SDK.

|image16|


All the following steps should be executed from command line. The path
to the policy file (if using a custom policy) can be relative to current
working directory, or absolute. All paths to keys files inside policy
file are absolute or relative to the policy path.

A. Create Blinky LED FreeRTOS Application Project
**************************************************

1. Launch the Eclipse IDE for ModusToolbox.

2. Open an existing workspace or create a new workspace

3. Click on **File > New > ModusToolbox IDE Application**.

4. In the Project Creator, select CY8CKIT-064B0S2-4343W kit and click on
   **Next >**.

5. Select the "Secure Blinky LED FreeRTOS" application and click
   **Create**. This may take a while as it pulls all required sources
   from respective repositories.

B. Set Up CySecureTools Workspace
**********************************

Open a native command-line application and navigate to the
*%WORKSPACE%/Secure_Blinky_LED_FreeRTOS\\* directory. For example in
Windows 7:

|image17|

Run the following command:

::

   \*Example usage for the CY8CKIT-064B0S2-4343W kit\*

   cysecuretools -t cyb06xxa init


**What does this step do?**

CySecureTools provides default policies and other secure assets that
can be used to quickly set up the chip with development parameters,
like leaving the CM4 DAP (Debug Access Port) open to reprogram the
chip.

Based on the selected target, this step sets up all the necessary
files in your workspace that are used for subsequent steps.

After running this step, you will have a choice of multiple default
policies you can use to provision the chip. You can choose which policy
you want to use by the \-\-policy/-p flag in the CySecureTools CLI.

For details on what each default policy means, see `Understanding the
Default policy. <#understanding-the-default-policy>`__

.. note:: 
   Ensure you use the same policy file when running through steps
   C, D, and E.


C. Generate new keys
*********************

Ensure you are in the “%WORKSPACE%/Secure_Blinky_LED_FreeRTOS/”
directory command-line copy/paste the below command,

::

   \*Example usage for the CY8CKIT-064B0S2-4343W kit\*

   cysecuretools -t cyb06xxa 4343w -p policy\policy_single_CM0_CM4.json
   create-keys

**What does this step do?**

CySecureTools reads the provided policy and generates the keys
defined.

Depending on the policy chosen, there can be multiple keys generated
under the /keys/ folder. By default only one key, the
USERAPP_CM4_KEY, a P-256 Elliptic curve key-pair is generated.

CySecureTools generates keys in two formats, PEM and JSON. Both the
PEM and JSON files represent the same key.

D. (Optional) Run Entrance Exam
*******************************

Connect the kit to your PC.

.. attention::

   The KitProg3 must be in DAPLink mode for performing this
   step. Press the ‘Mode’ button on the kit until the Status LED blinks
   fast. For more details, refer to the `KitProg3 User
   Guide. <https://www.cypress.com/file/452701/download>`__

In your command-line copy/paste the below command,

::

   \*Example usage for the CY8CKIT-064B0S2-4343W kit\*

   cysecuretools -t cyb06xxa entrance-exam


**What does this step do?**

The Entrance exam is a test routine that does the following things:

Verify that the Device is in the correct lifecycle stage Verify that
Boot Code has not been modified/tampered

Verify that User flash is empty and no code is running before any
provisioning takes place

Failing the entrance exam returns an error in the command line. If
there is any firmware running on the device, CySecureTools will give
an option to erase the chip. Existing firmware can be erased using
tools like Cypress Programmer.

Note that the entrance exam is also run automatically before
performing provisioning, so you can skip this step if needed.

E. Perform Provisioning
************************

.. attention::

   KitProg3 must be in DAPLink mode. **The kit supply voltage
   must be 2.5 V to perform this step**. Refer to the relevant 
   kit user guide to find out how to change the supply voltage 
   of your kit.

Ensure you are in the *%WORKSPACE%/Secure_Blinky_LED_FreeRTOS/*
directory. In your command-line copy/paste the below command for device
provisioning:

::

   \*Example usage for the CY8CKIT-064B0S2-4343W kit\*

   cysecuretools -t cyb06xxa -p policy\policy_single_CM0_CM4.json
   provision-device

**What does this step do?**

The CySecureTools provision-device API does the following steps:

Reads the provided policy and forms the final provisioning packet,
named prov_cmd.jwt Performs the entrance exam

Provisions the device by sending the prov_cmd.jwt to the PSoC64
Secure MCU.

Before running this step, you can modify the default policy to match
your end use-case. For most development use-cases, you don’t need to
modify it. Please see `Understanding the Default policy. <#understanding-the-default-policy>`__

Device Re-provisioning
-----------------------

The default device policy templates provided in CySecureTools allows you
to reprovision a device after running through the provisioning steps.

To reprovision a device, follow the steps in the normal provisioning
flow (see `Device Provisioning) <#device-provisioning>`__ and run the following
command:

::

   \*Example usage to re-provision the CY8CKIT-064B0S2-4343W kit\*

   cysecuretools -t cyb06xxa -p policy\policy_single_CM0_CM4.json
   re-provision-device

When re-provisioning a device, the Entrance exam step is not run again.

In case of failure at re-provisioning check paragraph.

ModusToolbox Secure Image Generation
-------------------------------------

In ModusToolbox, PSoC 64-based kit targets have post-build signing
scripts set up in the makefile so the output binary is formatted and
signed automatically according to the provisioned policy file; for
example, *policy_single_CM0_CM4.json*.

The post-build signing is part of the .mk file located in the target;
for example, *\\libs\TARGET_CY8CKIT-064B0S2-4343w\\
CY8CKIT-064B0S2-4343W.mk*

The following diagram shows the flow for signing and encryption using
ModusToolbox:

|image18|

The build process outputs two binaries:

-  Signed boot image hex file. This is the exact binary that can be
   programmed to Slot#0 for the PSoC 64 to securely launch the
   application.

-  Signed and encrypted(policy dependent) update image hex file. This is
   the exact binary that can be programmed to Slot#1 for the PSoC 64 to
   perform a secure update and then launch the application.

Build and Run the Application
------------------------------

1. In the Project Explorer, right-click on the Blinky LED FreeRTOS
   project and select **Build Project**.

2. Connect device to the computer over USB.

3. Right-click on Blinky LED FreeRTOS project and select **Run As > Run
   Configurations…**

4. On the dialog, select **GDB OpenOCD Debugging >** **Blinky LED
   FreeRTOS Program (KitProg3)** and click the **Run** button.

Debug the application
----------------------

1. Right-click on Blinky LED FreeRTOS project and select **Run As >
   Debug Configurations…**

2. On the dialog, select **GDB OpenOCD Debugging >** **Blinky LED
   FreeRTOS Program (KitProg3)** and click the **Debug** button.

A Breakpoint is set at main function with default launch configurations.
After the first step debugger breaks at main function.

Re-provisioning After Failure
------------------------------

Perform this step as needed for the following scenario:

-  You provision the PSoC 64 device, build, sign, and program the
   application.

-  The application is verified and started by the Cypress Secure
   Bootloader, but it does not work correctly and puts the device into a
   hard fault.

-  You try to re-provision the device, using the default re-provisioning
   command.

   If the Slot 0 address and user keys were not changed, the Cypress
   Secure Bootloader starts the bad application during re-provisioning
   process, the device becomes nonresponsive (the application does not
   produce a synchronization event for the external programming tool)
   and the re-provisioning process fails after timeout.

To address this failure, erase the boot slot manually before
re-provisioning, or use the following option in the re-provisioning
command:

::

   cysecuretools -t cyb06xxa -p policy\policy_single_CM0_CM4.json
   re-provision-device –-erase-boot

5. CySecureTools Design
========================

This section provides an overview of the CySecureTools python package
design and details on the default policy

CySecureTools Component Diagram
-------------------------------

The following diagram shows the high-level components of CySecureTools:

|image19|

CySecureTools Design
--------------------

CySecureTools provides a Command-line interface over stand-alone scripts
that simplifies calling them with minimum number of arguments. Advanced
users can use the scripts without wrapper and configure each argument as
they need.

Creating a Provisioning Packet
-------------------------------

The final prov_cmd.jwt which is required for provisioning a Secure MCU
device requires several pieces of information,

As input arguments, the tools(specifically the create-provisioning-packet API) takes:

-  OEM key file

-  HSM key file

-  Cypress Bootloader image certificate

-  Provisioning authorization certificate

-  Policy file

-  Output directory (*packet* by default)

-  User’s keys to be used for image signing

-  Chain of trust certificates

The output of the script will be added to the folder *packet*.

The *prov_identity.jwt* packet is used for giving the identity to the
device during provisioning. The data provisioned with this packet cannot
be changed during re-provisioning.

The *prov_cmd.jwt* file is used during provisioning and re-provisioning.
This packet contains the data that can be changed during
re-provisioning.

|image20|

Understanding the Default Policy
--------------------------------

There can be up to four default policies provided in CySecureTools for
each target.

+----------------+-------------------------------+----------------+--------------------------+
|**Policy Name** | **Description**               |    **Debug**   |**Application Memory Map**|
+================+===============================+================+==========================+
|policy_single_CM|| Policy for applications which|CM0P/CM4/SysAP  |Internal Flash only       |
|0_CM4.json      |  need to have single signature|are enabled     |                          |
|                |  for 2 combined applications –|                |                          |
|                |  Secure CM0p and User App on  |                |                          |
|                |  CM4.                         |                |                          |
|                || Typical use-case for simple  |                |                          |
|                |  secure boot and upgrade syste|                |                          |
|                |  ms.                          |                |                          |
+----------------+-------------------------------+----------------+--------------------------+
|policy_single_CM|Similar to previous policy,    |CM0P/CM4/SysAP  |External memory support   |
|0_CM4_smif.json |Enables external memory support|are enabled     |for upgrade image         |
|                |for upgrade image location     |                |                          |
+----------------+-------------------------------+----------------+--------------------------+
|policy_multi_CM || Policy for applications which|CM0P/CM4/SysAP  |Internal Flash only       |
|0_CM4.json      |  need independently updateable|are enabled     |                          |
|                |  SPE and NSPE code.           |                |                          |
|                || Typical use-case for IoT syst|                |                          |
|                |  ems which maintain Secure cod|                |                          |
|                |  e independent of application |                |                          |
|                |  code.                        |                |                          |
+----------------+-------------------------------+----------------+--------------------------+
|policy_multi_CM |Similar to previous policy,    |CM0P/CM4/SysAP  |External memory support   |
|0_CM4_smif.json |Enables external memory support|are enabled     |for upgrade image         |
|                |for upgrade image location     |                |                          |
+----------------+-------------------------------+----------------+--------------------------+

This section covers the details of the fields in the default
policy_single_CM0_CM4 policy provided in the CySecureTools. 
The contents can be classified as follows:

-  Boot&Upgrade Policy 

-  Debug Policy

-  Cypress Bootloader

-  CySecureTools misc assets

Boot&Upgrade Policy
*******************

The Boot&Upgrade (BnU) policy defines the memory regions and keys
associated with images in the chip. This JSON field has further
sub-objects:

-  Cypress Secure BnU Policy 

-  CM0+ Image BnU Policy

-  CM4 Image BnU Policy 

-  Reprovisioning options

The following tables show the Cypress Bootloader settings in the default
policy_single_CM0_CM4.json. These fields will never need to be modified,
but they can be useful as a reference:

Table 1: Cypress Secure Bootloader Boot&Upgrade policy

+------------------------------------------+------------------------------------------+
|    **JSON Field**                        |    **Description**                       |
+==========================================+==========================================+
| {                                        |                                          |
+------------------------------------------+------------------------------------------+
| boot_auth: [5],                          | KeyID 5 used check image signature       |
+------------------------------------------+------------------------------------------+ 
| bootloader_keys: [                       | Defines key used for Bootloader          |
+------------------------------------------+------------------------------------------+
| {                                        |                                          |
+------------------------------------------+------------------------------------------+
| kid: 5,                                  | Specify KeyID = 5 for the key            |
+------------------------------------------+------------------------------------------+
| key: "../keys/oem_state.json"            | Path to key                              |
+------------------------------------------+------------------------------------------+
| }                                        |                                          |
+------------------------------------------+------------------------------------------+ 
| ],                                       |                                          |
+------------------------------------------+------------------------------------------+
| id: 0,                                   | Image Id '0', Indicates Cypress Secure   |
|                                          | Bootloader                               |
+------------------------------------------+------------------------------------------+
| launch: 1,                               | Next image to launch is identifier '1',  |
|                                          | indicates CM0+ image                     |
+------------------------------------------+------------------------------------------+
| acq_win: 20,                             | Defines acquire window for CypressBootlo |
|                                          | ader in msec                             |
+------------------------------------------+------------------------------------------+
| monotonic: 0,                            | The counter to protect the rollback      |
|                                          | during the upgrade process               |
+------------------------------------------+------------------------------------------+
| clock_flags: 578,                        | Clock flag - 0x0242; Listen window is    |
|                                          | 100ms; CM0+ clock set to 50MHz when      |
|                                          | executing CySecureBootloader             |
+------------------------------------------+------------------------------------------+
| protect_flags: 1,                        |                                          |
+------------------------------------------+------------------------------------------+
| upgrade: false,                          | Defines if the upgrade image from slot_1 |
|                                          | is allowed                               |
+------------------------------------------+------------------------------------------+
| resources: [                             | Defines Resources used by image          |
+------------------------------------------+------------------------------------------+
| {                                        |                                          |
+------------------------------------------+------------------------------------------+
| type: "FLASH_PC1_SPM",                   | Indicates Flash region to be protected   |
|                                          | at PC=1                                  |
+------------------------------------------+------------------------------------------+
| address: 269287424,                      | Address: 0x100D0000                      |
+------------------------------------------+------------------------------------------+
| size: 65536                              | Size: 64K                                |
+------------------------------------------+------------------------------------------+
| },                                       |                                          |
+------------------------------------------+------------------------------------------+
| {                                        |                                          |
+------------------------------------------+------------------------------------------+
| type: "SRAM_SPM_PRIV",                   | Indicates RAM region to be protected at  |
|                                          | PC=1                                     |
+------------------------------------------+------------------------------------------+
| address: 134348800,                      | Address: 0x08020000                      |
+------------------------------------------+------------------------------------------+
| size: 65536                              | Size: 64K                                |
+------------------------------------------+------------------------------------------+
| },                                       |                                          |
+------------------------------------------+------------------------------------------+        
| {                                        |                                          |
+------------------------------------------+------------------------------------------+
| type: "SRAM_DAP",                        | Indicates RAM reserved by DAP for        |
|                                          | debugging                                |
+------------------------------------------+------------------------------------------+
| address: 134397952,                      | Address: 0x0802C000                      |
+------------------------------------------+------------------------------------------+
| size: 16384                              | Size: 16K                                |
+------------------------------------------+------------------------------------------+
| }                                        |                                          |
+------------------------------------------+------------------------------------------+
| ]                                        |                                          |
+------------------------------------------+------------------------------------------+
| },                                       |                                          |
+------------------------------------------+------------------------------------------+


Table 2: CM0+ Application image Boot&Upgrade policy

+------------------------------------------+------------------------------------------+
|    **JSON Field**                        |    **Description**                       |
+==========================================+==========================================+
| {                                        |                                          |
+------------------------------------------+------------------------------------------+
| boot_auth: [8],                          | KeyID 8 used check image signature       |
+------------------------------------------+------------------------------------------+
| boot_keys: [                             | Defines key used by Cypress Bootloader   |
+------------------------------------------+------------------------------------------+
| {                                        |                                          |
+------------------------------------------+------------------------------------------+
| kid: 8,                                  | Specify KiD=8 for the below key          |
+------------------------------------------+------------------------------------------+
| key: "../keys/USERAPP_CM4_KEY.json"      | Path to Key                              |
+------------------------------------------+------------------------------------------+
| }                                        |                                          |
+------------------------------------------+------------------------------------------+
| ],                                       |                                          |
+------------------------------------------+------------------------------------------+
| id: 1,                                   | Image Id '1', Indicates CM0+  Image      |
+------------------------------------------+------------------------------------------+
| launch: 16,                              | Image id '16' will be launched by this   |
|                                          | image                                    |
+------------------------------------------+------------------------------------------+
| monotonic: 0,                            | The counter to protect the rollback      |
|                                          | during the upgrade process               |
+------------------------------------------+------------------------------------------+
| smif_id: 0,                              | No upgrade mage in external memory       |
+------------------------------------------+------------------------------------------+
| acq_win: 100,                            | Defines acquire window for this image in |
|                                          | msec                                     |
+------------------------------------------+------------------------------------------+
| upgrade: true,                           | Upgrade image from slot_1 is allowed     |
+------------------------------------------+------------------------------------------+
| version: "0.1",                          | Version of image, used by CySecuretools  |
|                                          | to form MCUBoot header                   |
+------------------------------------------+------------------------------------------+
| rollback_counter: 0,                     | One-way version counter of zero          |
+------------------------------------------+------------------------------------------+
| encrypt: false,                          | Encryption for upgrade slot is disabled  |
+------------------------------------------+------------------------------------------+
| encrypt_key_id: 1,                       | Kid:1(device private key) used for ECDH  |
|                                          | for deriving KeK of encrypted update     |
+------------------------------------------+------------------------------------------+
| encrypt_peer: "../keys/dev_pub_key.pem", | Path to public key to be used by         |
|                                          | cysecuretools to for the KeK             |
+------------------------------------------+------------------------------------------+
| resources: [                             | Defines Resources used by image          |
+------------------------------------------+------------------------------------------+
| {                                        |                                          |
+------------------------------------------+------------------------------------------+
| type: "BOOT",                            | Indicates Slot#0                         |
+------------------------------------------+------------------------------------------+
| address: 268435456,                      | Address: 0x10000000                      |
+------------------------------------------+------------------------------------------+
| size: 950272                             | Size: 928K                               |
+------------------------------------------+------------------------------------------+
| },                                       |                                          |
+------------------------------------------+------------------------------------------+
| {                                        |                                          |
+------------------------------------------+------------------------------------------+
| type: "UPGRADE",                         | Indicates Slot#1                         |
+------------------------------------------+------------------------------------------+
| address: 269385728,                      | Address: 0x100E8000                      |
+------------------------------------------+------------------------------------------+
| size: 950272                             | Size: 928K                               |
+------------------------------------------+------------------------------------------+
| }                                        |                                          |
+------------------------------------------+------------------------------------------+
| ]                                        |                                          |
+------------------------------------------+------------------------------------------+
| },                                       |                                          |
+------------------------------------------+------------------------------------------+

**Special Note:** In the single-image use case, most fields in the M4
Boot&Upgrade image policy are placeholders. All the required information
(except CM4 Boot address) is derived from the CM0+ policy which has the
combined firmware image for both cores.

Table 3: CM4 Application image Boot&Upgrade policy

+------------------------------------------+------------------------------------------+
|    **JSON Field**                        |    **Description**                       |
+==========================================+==========================================+
| {                                        |                                          |
+------------------------------------------+------------------------------------------+
| boot_auth: [8],                          | N/A                                      |
+------------------------------------------+------------------------------------------+
| boot_keys: [                             | N/A                                      |
+------------------------------------------+------------------------------------------+
| {                                        |                                          |
+------------------------------------------+------------------------------------------+
| kid: 8,                                  | N/A                                      |
+------------------------------------------+------------------------------------------+
| key: "../keys/USERAPP_CM4_KEY.json"      | N/A                                      |
+------------------------------------------+------------------------------------------+
| }                                        |                                          |
+------------------------------------------+------------------------------------------+
| ],                                       |                                          |
+------------------------------------------+------------------------------------------+
| id: 16,                                  | Image Id '16', Indicates CM4  Image      |
+------------------------------------------+------------------------------------------+
| monotonic: 8,                            | N/A                                      |
+------------------------------------------+------------------------------------------+
| smif_id: 0,                              | N/A                                      |
+------------------------------------------+------------------------------------------+
| upgrade: false,                          | N/A                                      |
+------------------------------------------+------------------------------------------+
| version: "0.1",                          | N/A                                      |
+------------------------------------------+------------------------------------------+
| rollback_counter: 0,                     | N/A                                      |
+------------------------------------------+------------------------------------------+
| encrypt: false,                          | N/A                                      |
+------------------------------------------+------------------------------------------+
| encrypt_key_id: 1,                       | N/A                                      |
+------------------------------------------+------------------------------------------+
| encrypt_peer: "../keys/dev_pub_key.pem", | N/A                                      |
+------------------------------------------+------------------------------------------+
| resources: [                             | Defines Resources used by image          |
+------------------------------------------+------------------------------------------+
| {                                        |                                          |
+------------------------------------------+------------------------------------------+
| type: "BOOT",                            | Indicates Launch address of CM4          |
+------------------------------------------+------------------------------------------+
| address: 268500992,                      | Address: 0x10010000                      |
+------------------------------------------+------------------------------------------+
| size: 884736                             | N/A                                      |
+------------------------------------------+------------------------------------------+
| }                                        |                                          |
+------------------------------------------+------------------------------------------+
| ],                                       |                                          |
+------------------------------------------+------------------------------------------+

Table 4: Reprovisioning Options

+------------------------------------------+------------------------------------------+
|    **JSON Field**                        |    **Description**                       |
+==========================================+==========================================+
| {                                        |                                          |
+------------------------------------------+------------------------------------------+
| boot_loader: true,                       | Bootloader can be reprovisioned          |
+------------------------------------------+------------------------------------------+
| keys_and_policies: true                  | Keys and Policies can be reprovisioned   |
+------------------------------------------+------------------------------------------+
| }                                        |                                          |
+------------------------------------------+------------------------------------------+

Debug Policy
*************

The Debug policy specifies how various access ports are configured for
the part.

Table 5: Debug policy

+------------------------------------------+------------------------------------------+
|    **JSON Field**                        |    **Description**                       |
+==========================================+==========================================+
| {                                        |                                          |
+------------------------------------------+------------------------------------------+
| "m0p": {                                 | Defines CM0P DAP Port behavior           |
+------------------------------------------+------------------------------------------+
| "permission": "enabled",                 | DAP Port enabled                         |
+------------------------------------------+------------------------------------------+
| "control": "firmware",                   | N/A                                      |
+------------------------------------------+------------------------------------------+
| "key": 5                                 | N/A                                      |
+------------------------------------------+------------------------------------------+
| },                                       |                                          |
+------------------------------------------+------------------------------------------+
| "m4": {                                  | Defines CM4 DAP Port behavior            |
+------------------------------------------+------------------------------------------+
| "permission": "allowed",                 | DAP Port enabled                         |
+------------------------------------------+------------------------------------------+
| "control": "firmware",                   | N/A                                      |
+------------------------------------------+------------------------------------------+
| "key": 5                                 | N/A                                      |
+------------------------------------------+------------------------------------------+
| },                                       |                                          |
+------------------------------------------+------------------------------------------+
| "system": {                              | Defines CM4 DAP Port behavior            |
+------------------------------------------+------------------------------------------+
| "permission": "enabled",                 | DAP Port enabled                         |
+------------------------------------------+------------------------------------------+
| "control": "firmware",                   | N/A                                      |
+------------------------------------------+------------------------------------------+
| "key": 5,                                | N/A                                      |
+------------------------------------------+------------------------------------------+
| "flashw": true,                          | Allow Flash Writes using SysAP port      |
+------------------------------------------+------------------------------------------+
| "flashr": true                           | Allow Flash Reads using SysAP port       |
+------------------------------------------+------------------------------------------+
| },                                       |                                          |
+------------------------------------------+------------------------------------------+
| "rma": {                                 | Defines RMA behavior                     |
+------------------------------------------+------------------------------------------+
| "permission": "allowed",                 | RMA mode is allowed                      |
+------------------------------------------+------------------------------------------+
| "destroy_fuses": [                       | Indicates eFuse region to be destroyed if|
|                                          | entering RMA mode                        |
+------------------------------------------+------------------------------------------+
| {                                        |                                          |
+------------------------------------------+------------------------------------------+
| "start": 888,                            | Start address of efuses to be destroyed  |
+------------------------------------------+------------------------------------------+
| "size": 136                              | Size in bits to be destroyed             |
+------------------------------------------+------------------------------------------+
| }                                        |                                          |
+------------------------------------------+------------------------------------------+
| ],                                       |                                          |
+------------------------------------------+------------------------------------------+
| "destroy_flash": [                       | Indicates Flash region to be destroyed if|
|                                          | entering RMA mode                        |
+------------------------------------------+------------------------------------------+
| {                                        |                                          |
+------------------------------------------+------------------------------------------+
| "start": 268435456,                      | Start Address of flash to be destroyed   |
|                                          | (0x10000000)                             |
+------------------------------------------+------------------------------------------+
| "size": 512                              | Size in bytes of flash to be destroyed   |
+------------------------------------------+------------------------------------------+
| }                                        |                                          |
+------------------------------------------+------------------------------------------+
| ],                                       |                                          |
+------------------------------------------+------------------------------------------+
| "key": 5                                 |KeyID used to validate a RMA request      |
+------------------------------------------+------------------------------------------+
| }                                        |                                          |
+------------------------------------------+------------------------------------------+
| }                                        |                                          |
+------------------------------------------+------------------------------------------+

Cypress Bootloader
*******************

:: 

  "cy_bootloader":
  {
      "mode" : "debug", -> CySecureBootloader will emit debug logs over UART
  }

CySecureTools Misc Assets
**************************

+------------------------------------------+------------------------------------------+
|    **JSON Field**                        |    **Description**                       |
+==========================================+==========================================+
| "provisioning": {                        | Defines provisioning packet paths        |
+------------------------------------------+------------------------------------------+
| "packet_dir": "../packets",              | Relative path of the packets folder used |
|                                          | for provisioning                         |
+------------------------------------------+------------------------------------------+
| "chain_of_trust": []                     | No chain-of-trust certificate objects    |
+------------------------------------------+------------------------------------------+
| },                                       |                                          |
+------------------------------------------+------------------------------------------+
| "pre_build": {                           | Defines pre-build asset locations needed |
|                                          | for provisioning                         |
+------------------------------------------+------------------------------------------+
| "oem_public_key": "../keys/oem_state.    | Relative path for OEM root public key    |
| json",                                   | location                                 |
+------------------------------------------+------------------------------------------+
| "oem_private_key": "../keys/oem_state.   | Relative path for OEM root private key   |
| json",                                   | location                                 |
+------------------------------------------+------------------------------------------+
| "hsm_public_key": "../keys/hsm_state.    | Relative path for HSM public key location|
| json",                                   |                                          |
+------------------------------------------+------------------------------------------+
| "hsm_private_key": "../keys/hsm_state.   | Relative path for HSM private key        |
| json",                                   | location                                 |
+------------------------------------------+------------------------------------------+
| "provision_group_private_key": false,    | No group private keys provisioned        |
+------------------------------------------+------------------------------------------+
| "group_private_key": "../keys/grp_priv\_ | Relative path for group private key      |
| key.json",                               | location                                 |
+------------------------------------------+------------------------------------------+
| "provision_device_private_key": false,   | No device private key provisioned        |
+------------------------------------------+------------------------------------------+
| "device_private_key": "../keys/dev_priv\_| Relative path for device private key     |
| key.json",                               | location                                 |
+------------------------------------------+------------------------------------------+
| "cy_auth": "../packets/cy_auth_1m_b0\_   | Relative path for cy_auth location       |
| sample.jwt"                              |                                          |
+------------------------------------------+------------------------------------------+

Provisioning JWT packet Reference
---------------------------------

1. prov_cmd.jwt
*****************

The prov_cmd.jwt is the final packet sent to the PSoC 64 Secure MCU to
finalize provisioning. The following shows this JWT structure:

**Structure:**

::

   {

     {

     "cy_auth": "………",

     "rot_auth": "………",

     "image_cert": "………",

     "prov_req": "………",

     "chain_of_trust": [],

     "complete": Boolean Value,

     "type": "HSM_PROV_CMD"

   } sig: HSM_PRIV_KEY

+----------------+------------------------------------------------------------------+
| Object         | Description                                                      |
+================+==================================================================+
| cy_auth        | Cypress Authorization JWT, authorizes the HSM public key.        |
+----------------+------------------------------------------------------------------+ 
| rot_auth       | OEM/User authorization JWT, authorizes the HSM public key.       |
+----------------+------------------------------------------------------------------+
| image_cert     | Cypress Secure Bootloader image JWT, used for sending a Cypress  |
|                | Secure Bootloader signature.                                     |
+----------------+------------------------------------------------------------------+
| chain_of_trust | Holds an array of X.509 certificates.                            |
+----------------+------------------------------------------------------------------+
| complete       | True - indicates if complete provisioning process must be        |
|                | complete and move chip life-cycle                                |
+----------------+------------------------------------------------------------------+
| type           | Specifies the JWT type as a string.                              |
+----------------+------------------------------------------------------------------+

2. prov_identity.jwt
*********************

The prov_identity.jwt is a the initial token which is sent to the chip
to create a unique identity

**Structure:**

::

   {

     {

     "create_identity": Boolean Value,

     "cy_auth": "………",

     "rot_auth": "………",

     "type": "HSM_PROV_CMD"

   } sig: HSM_PRIV_KEY

+----------------+------------------------------------------------------------------+
| Object         | Description                                                      |
+================+==================================================================+
| create_identity| If true, chip will form a unique identity and export the public  |
|                | key                                                              |
+----------------+------------------------------------------------------------------+
| cy_auth        | Cypress Authorization JWT, authorizes the HSM public key.        |
+----------------+------------------------------------------------------------------+
| rot_auth       | OEM/User authorization JWT, authorizes the HSM public key.       |
+----------------+------------------------------------------------------------------+
| type           | Specifies the JWT type as a string.                              |
+----------------+------------------------------------------------------------------+

3. cy_auth.jwt
***************

**Structure:**

:: 

   {

     "auth": {},

     "cy_pub_key": {Cypress root pub key},

     "hsm_pub_key": {HSM pub key},

     "exp": {Expiry time},

     "type": "CY_AUTH_HSM"

   } sig: CYPRESS_ROOT_PRIV_KEY

+----------------+------------------------------------------------------------------+
| Object         | Description                                                      |
+================+==================================================================+
| auth           | Can specify authorization limits based on device die_id.         |
+----------------+------------------------------------------------------------------+
| cy_pub_key     | Cypress Root Public key in the JWK format.                       |
+----------------+------------------------------------------------------------------+
| hsm_pub_key    | HSM Root Public key in the JWK format.                           |
+----------------+------------------------------------------------------------------+
| exp            | Specifies when the token expires in UNIX time.                   |
+----------------+------------------------------------------------------------------+
| type           | Specifies the JWT type as a string.                              |
+----------------+------------------------------------------------------------------+

4. rot_auth.jwt
****************

**Structure:**

::

   {

     "hsm_pub_key": {HSM pub key},

     "oem_pub_key": {OEM RoT pub key},

     "iat": {Issue time},

     "prod_id": {Product Name},

     "type": "OEM_ROT_AUTH"

   } sig: OEM_RoT_PRIV_KEY

+----------------+------------------------------------------------------------------+
| Object         | Description                                                      |
+================+==================================================================+
| hsm_pub_key    | HSM Root Public key in the JWK format.                           |
+----------------+------------------------------------------------------------------+
| oem_pub_key    | OEM RoT Public key in the JWK format.                            |
+----------------+------------------------------------------------------------------+
| iat            | Specifies when the token was issued                              |
+----------------+------------------------------------------------------------------+
| prod_id        | The product string, specified by the user. Note that this MUST   |
|                | match prod_id in the prov_req.JWT.                               |
+----------------+------------------------------------------------------------------+
| type           | Specifies the JWT type as a string.                              |
+----------------+------------------------------------------------------------------+

5. prov_req.jwt
****************

**Structure:**

:: 

   {

     "custom_pub_key": [{Key1}, ...],

     "boot_upgrade": {...},

     "debug": {...}

     "prod_id": "my_thing",

     "wounding": {}

   } sig: OEM_RoT_PRIV_KEY

+----------------+------------------------------------------------------------------+
| Object         | Description                                                      |
+================+==================================================================+
| custom_pub_key | The array of customer public keys to be injected in the JWK      |
|                | format.                                                          |
+----------------+------------------------------------------------------------------+
| boot_upgrade   | Boot and Upgrade Policy JSON.                                    |
+----------------+------------------------------------------------------------------+
| debug          | Debug policy JSON.                                               |
+----------------+------------------------------------------------------------------+
| prod_id        | The product string, specified by the user. Note that this MUST   |
|                | match prod_id in the rot_auth.JWT.                               |
+----------------+------------------------------------------------------------------+
| wounding       | Reserved.                                                        |
+----------------+------------------------------------------------------------------+

6. boot_upgrade.JSON
*********************

**Structure:**

::

   {

     "firmware": [

      {

         "id": [Integer Value],

         "boot_auth": [Integer Value],

         "launch": Integer Value,

         "monotonic": [Integer Value],

         "resources": [

          {

             "address": Integer Value,

             "size": Integer Value,

             "type": [STRING VALUE]

          },

         ],

         "smif_id": Integer Value,
  
         "upgrade": Boolean Value,

         "upgrade_auth": [Integer Value]

      }, ...

     ],

     "title": "upgrade_policy"

   }


+----------------+----------------------------------+-----------------------------------------+
| Object         | Description                      | Range of valid values                   |
+================+==================================+=========================================+
| id             | Image id. (0-16: Cypress reserve | A range of integers can be specified,   |
|                | d, >16: customer specific)       | "0” : The first firmware image started  |
|                |                                  | from RomBoot/FlashBoot (i.e. the boot   |
|                |                                  | loader).                                |
|                |                                  | | "1” : CM0+ Image                      |
|                |                                  | | “16”: CM4 Image                       |
+----------------+----------------------------------+-----------------------------------------+
| boot_auth      | Specifies key index to use for   || Can be any integer public key >3.      |
|                | validating the signature. These  || For Cypress Secure Bootloader, the auth|
|                | signatures are all verified      |  is “3”.                                |
|                | during boot.                     || For the M4 image, this can be any      |
|                |                                  |  number depending on key_id specified in|
|                |                                  |  the JWK format in the custom_pub_key   |
|                |                                  |  fields.                                |
+----------------+----------------------------------+-----------------------------------------+
| launch         | Specifies next image ‘id’ being  | “4” is the only valid value for Cypress |
|                | launched                         | Secure Bootloader and the Single image  |
|                |                                  | bootloader case.                        |
+----------------+----------------------------------+-----------------------------------------+
| monotonic      | Indicates the monotonic counter  |0~15. Counters can be rolled up by the   |
|                | number associated with this image|system firmware using SysCalls.          |
|                | . During secure boot this counter|                                         |
|                | value is compared with the       |                                         |
|                | current_version code in the image|                                         |
|                | being booted. During upgrade this|                                         |
|                | counter is incremented to the    |                                         |
|                | value from the image header of   |                                         |
|                | the upgrade image.               |                                         |
+----------------+----------------------------------+-----------------------------------------+ 
| resources:     | Specifies the start address of   | The valid flash range address. Only     |
| address        | the image                        | decimal values are allowed, e.g.: 268435|
|                |                                  | 456 -> 0x10000000                       |
+----------------+----------------------------------+-----------------------------------------+
| resources: size| Specifies the size of the image  | The valid flash range size in bytes.    |
|                |                                  | Only decimal values are allowed, e.g.:32|
|                |                                  | 7680 -> 0x50000 -> 320KB                |
+----------------+----------------------------------+-----------------------------------------+
| resources: type| Specifies type of image          | Only “BOOT” and “UPGRADE” are user-modif|
|                |                                  | table fields for the M4 image.          |
|                |                                  | | “BOOT” -> Slot#0                      |
|                |                                  | | “UPGRADE” -> Slot#1                   |
+----------------+----------------------------------+-----------------------------------------+
| smif_id        | Specifies if external memory is  | | “0” – SMIF is disabled.               |
|                | used for placing Slot#1 image    | | “1” – If the CY8CPROTO_064_SB target  |
|                |                                  |   is used.                              |
+----------------+----------------------------------+-----------------------------------------+
| upgrade        | Specifies if updating is allowed || ‘true’ -> Upgrades are allowed.        | 
|                | for this image id                || ‘false’ -> Upgrade is not allowed.     |
+----------------+----------------------------------+-----------------------------------------+
| upgrade_auth   | Specifies key index to use for   || Can be any integer public key >3.      |
|                | validating the signature of the  || For Cypress Secure Bootloader, the auth|
|                | upgrade. Allows upgrades to be   |  is “3”.                                |
|                | checked by a different key if    || For the M4 image, this can be any numbe|
|                | necessary.                       |  r depending on key_id specified in the |
|                |                                  |  JWK format in the custom_pub_key fields|
+----------------+----------------------------------+-----------------------------------------+

7. debug.JSON
**************

**Structure:**

::

   {

      "m0p" : {

         "permission" : " STRING VALUE ",

         "control" : " STRING VALUE ",

         "key" : [Integer Value]

      },

      "m4" : {

         "permission" : " STRING VALUE ",

         "control" : " STRING VALUE ",

         "key" : [Integer Value]

      },

      "system" : {

         "permission" : " STRING VALUE ",,

         "control" : " STRING VALUE ",,

         "key" : [Integer Value],

         "flashw": Boolean Value,

         "flashr": Boolean Value,

      },

      "rma" : {

         "permission" : "STRING VALUE ",

         "destroy_fuses" : [

            {

               "start" : Integer Value,

               "size" : Integer Value

            }

         ],

         "destroy_flash" : [

            {

               "start" : Integer Value,

               "size" : Integer Value

            },

         ],

         "key" : Integer Value

      }

   }

+----------------+----------------------------------+-----------------------------------------+
| Object         | Description                      | Range of valid values                   |
+================+==================================+=========================================+
|m0p/m4/system:  |Specifies the permission level for|| “Enabled” – The DAP port is open after |
|permission      |the associated DAP port.          |  bootup.                                |
|                |                                  || “Allowed” – The DAP port can be opened |
|                |                                  |  after bootup, see the “control” field. |
|                |                                  || “Disabled” – The DAP port is closed aft|
|                |                                  |  er bootup.                             |
+----------------+----------------------------------+-----------------------------------------+
|m0p/m4/system:  |Specifies how the DAP port can be || “firmware” – The code the user can cho |
|control         |opened after bootup. The field is |  one to open the DAP port depending on  |
|                |only valid if “permission” is     |  some custom code.                      |
|                |“Allowed”.                        || “certificate” – A signed token must be |
|                |                                  |  presented using a SysCall to open the  |
|                |                                  |  DAP port.                              |
+----------------+----------------------------------+-----------------------------------------+
|m0p/m4/system:  |Specifies which Key Id to use for |The key ID must be >3, point to the key  |
|key             |certificate validation in “contro |provisioned in the custom_pub_key field. |
|                |l” field                          |                                         |
+----------------+----------------------------------+-----------------------------------------+
|system: flashr  |Specifies which regions the SysAP || “true” -> Flash reads/writes via SysAP | 
|/flashw         |port is allowed to access         |  allowed.                               |
|                |                                  || “false” -> Flash reads/writes via SysAP|
|                |                                  |  not allowed.                           |
+----------------+----------------------------------+-----------------------------------------+
|rma: permission |Specifies if RMA is allowed       || “Disabled” – RMA is not allowed.       |
|                |                                  || “Allowed” – The RMA stage is available |
|                |                                  |  and can be entered by presenting a     |
|                |                                  |  certificate using key> to a SysCall    |
|                |                                  |  API. The system will destroy fuse and  |
|                |                                  |  flash contents as specified in <destroy|
|                |                                  |  _fuses> and <destroy_flash> before     |
|                |                                  |  transitioning to RMA stage.            |
+----------------+----------------------------------+-----------------------------------------+
|rma: destroy\_  |Starting fuse bit number for      |0~65536. Check the part datasheet for the|
|fuses: start    |region                            |eFuse allowed address.                   |
+----------------+----------------------------------+-----------------------------------------+
|rma: destroy\_  |Number of fuse bits in region     |0~65536. Check the part datasheet for the|
|fuses: size     |                                  |eFuse allowed size.                      |
+----------------+----------------------------------+-----------------------------------------+
|rma: destroy\_  |Starting byte address of region (w|0~0xFFFFFFFF. Check the part datasheet   |
|flash: start    |ill be rounded down to nearest    |for the flash allowed address.           |
|                |program/erase boundary)"          |                                         |
+----------------+----------------------------------+-----------------------------------------+
|rma: destroy\_  |Size in bytes of region (will be  |0~0xFFFFFFFF. Check the part datasheet   |
|flash: size     |rounded up so region is integral  |for the flash allowed size.              |
|                |number of program/erase units)    |                                         |
+----------------+----------------------------------+-----------------------------------------+
|rma: key        |The key slot number of the key    |The key ID must be >3, point to the key  |
|                |used to validate authorization  to|provisioned in the custom_pub_key field. |
|                |enter RMA stage                   |                                         |
+----------------+----------------------------------+-----------------------------------------+


.. |image1| image:: ../../_static/image/feature-guide/security/Secure_Boot_SDK_User_Guide/secure_boot1.png   
.. |image2| image:: ../../_static/image/feature-guide/security/Secure_Boot_SDK_User_Guide/secure_boot2.png   
.. |image3| image:: ../../_static/image/feature-guide/security/Secure_Boot_SDK_User_Guide/secure_boot3.png  
.. |image4| image:: ../../_static/image/feature-guide/security/Secure_Boot_SDK_User_Guide/secure_boot4.png   
.. |image5| image:: ../../_static/image/feature-guide/security/Secure_Boot_SDK_User_Guide/secure_boot5.png   
.. |image6| image:: ../../_static/image/feature-guide/security/Secure_Boot_SDK_User_Guide/secure_boot6.png  
.. |image7| image:: ../../_static/image/feature-guide/security/Secure_Boot_SDK_User_Guide/secure_boot7.png  
.. |image8| image:: ../../_static/image/feature-guide/security/Secure_Boot_SDK_User_Guide/secure_boot8.png  
.. |image9| image:: ../../_static/image/feature-guide/security/Secure_Boot_SDK_User_Guide/secure_boot9.png 
.. |image10| image:: ../../_static/image/feature-guide/security/Secure_Boot_SDK_User_Guide/secure_boot10.png  
.. |image11| image:: ../../_static/image/feature-guide/security/Secure_Boot_SDK_User_Guide/secure_boot11.png   
.. |image12| image:: ../../_static/image/feature-guide/security/Secure_Boot_SDK_User_Guide/secure_boot12.png   
.. |image13| image:: ../../_static/image/feature-guide/security/Secure_Boot_SDK_User_Guide/secure_boot13.png   
.. |image14| image:: ../../_static/image/feature-guide/security/Secure_Boot_SDK_User_Guide/secure_boot14.png   
.. |image15| image:: ../../_static/image/feature-guide/security/Secure_Boot_SDK_User_Guide/secure_boot15.png   
.. |image16| image:: ../../_static/image/feature-guide/security/Secure_Boot_SDK_User_Guide/secure_boot16.png   
.. |image17| image:: ../../_static/image/feature-guide/security/Secure_Boot_SDK_User_Guide/secure_boot17.png 
.. |image18| image:: ../../_static/image/feature-guide/security/Secure_Boot_SDK_User_Guide/secure_boot18.png   
.. |image19| image:: ../../_static/image/feature-guide/security/Secure_Boot_SDK_User_Guide/secure_boot19.png   
.. |image20| image:: ../../_static/image/feature-guide/security/Secure_Boot_SDK_User_Guide/secure_boot20.png   

   