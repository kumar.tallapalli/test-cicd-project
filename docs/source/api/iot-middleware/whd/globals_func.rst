==============
Functions
==============
 
.. rubric:: - w -
   :name: w--

-  whd_add_secondary_interface() :
   `whd_wifi_api.h <group__wifimanagement.html#ga553c009e9f6f856dfe243d745e21bbf9>`__
-  whd_arp_arpoe_get() :
   `whd_wifi_api.h <group__wifiutilities.html#ga22d76514755f2dfde0ee6acbc193787a>`__
-  whd_arp_arpoe_set() :
   `whd_wifi_api.h <group__wifiutilities.html#gaa30d9e2a761de17962312d818f941c05>`__
-  whd_arp_cache_clear() :
   `whd_wifi_api.h <group__wifiutilities.html#ga01ece00a357914d02fa5a181fbddbf8c>`__
-  whd_arp_features_get() :
   `whd_wifi_api.h <group__wifiutilities.html#ga54728d3d6a714ef9264d6e25df9a6722>`__
-  whd_arp_features_print() :
   `whd_wifi_api.h <group__wifiutilities.html#gaf70b6becc3e4ecc551f5bf5278d7ad93>`__
-  whd_arp_features_set() :
   `whd_wifi_api.h <group__wifiutilities.html#gab4ebcd7fdd7b328e2f4e10b79197cfca>`__
-  whd_arp_hostip_list_add() :
   `whd_wifi_api.h <group__wifiutilities.html#ga417b404b9892862a572802a4ea38eb79>`__
-  whd_arp_hostip_list_add_string() :
   `whd_wifi_api.h <group__wifiutilities.html#ga61135784226a59af098384f4a071af02>`__
-  whd_arp_hostip_list_clear() :
   `whd_wifi_api.h <group__wifiutilities.html#gaaf3cec4a4896670049ec3802c91a2d66>`__
-  whd_arp_hostip_list_clear_id() :
   `whd_wifi_api.h <group__wifiutilities.html#ga15cfc56bd481a4bc02c6dc93bfa6883c>`__
-  whd_arp_hostip_list_clear_id_string() :
   `whd_wifi_api.h <group__wifiutilities.html#gaa38f97cc9800e7355d96d833b32b1c1c>`__
-  whd_arp_hostip_list_get() :
   `whd_wifi_api.h <group__wifiutilities.html#ga4287cc85981a511399ea927c84f61330>`__
-  whd_arp_peerage_get() :
   `whd_wifi_api.h <group__wifiutilities.html#ga2cd55bdc13221177c61310ecaa6f4bd7>`__
-  whd_arp_peerage_set() :
   `whd_wifi_api.h <group__wifiutilities.html#gab61ae9878853c02b4cf194c2c3a7377f>`__
-  whd_arp_stats_clear() :
   `whd_wifi_api.h <group__wifiutilities.html#ga564e95d3343802ed33d1b2c6fb531aeb>`__
-  whd_arp_stats_get() :
   `whd_wifi_api.h <group__wifiutilities.html#ga9d65fe0a82dee5edf65eb6f1efe9df52>`__
-  whd_arp_stats_print() :
   `whd_wifi_api.h <group__wifiutilities.html#ga37714a68f350e5a9e24dd6aeaea0ae98>`__
-  whd_arp_version() :
   `whd_wifi_api.h <group__wifiutilities.html#gae6bf707d845518cba9a8256fb5681f4a>`__
-  whd_bus_sdio_attach() :
   `whd_wifi_api.h <group__busapi.html#ga9f08c5241843cbe2ef06ffa8bfb5118d>`__
-  whd_bus_sdio_detach() :
   `whd_wifi_api.h <group__busapi.html#gadeed66e792eed64cbb6a0deef1dbe2da>`__
-  whd_bus_spi_attach() :
   `whd_wifi_api.h <group__busapi.html#ga16b799bb65db52fb4518e1fbc203fe53>`__
-  whd_bus_spi_detach() :
   `whd_wifi_api.h <group__busapi.html#gae83aaeef8c3d922cbb839b75ab289d13>`__
-  whd_deinit() :
   `whd_wifi_api.h <group__wifimanagement.html#gab7d6e59874922e5646e08e52082e32f5>`__
-  whd_init() :
   `whd_wifi_api.h <group__wifimanagement.html#ga286dde2ee65ac3ea5ae67a6e6ef25f0a>`__
-  whd_network_get_bsscfgidx_from_ifp() :
   `whd_wifi_api.h <group__dbg.html#ga84376c0830fbdb04c67812243539d4c6>`__
-  whd_network_get_ifidx_from_ifp() :
   `whd_wifi_api.h <group__dbg.html#ga01abfdd80ff65c7cac64e0d7c2f93c60>`__
-  whd_network_send_ethernet_data() :
   `whd_network_types.h <group__netif.html#ga3642a8b0fe0df7449730bec92e09c7d4>`__
-  whd_pf_add_packet_filter() :
   `whd_wifi_api.h <group__wifiutilities.html#gae81aead624f8e374970e1aa3f330ea30>`__
-  whd_pf_disable_packet_filter() :
   `whd_wifi_api.h <group__wifiutilities.html#ga6af0c7e202709713fa5d06e71a585e28>`__
-  whd_pf_enable_packet_filter() :
   `whd_wifi_api.h <group__wifiutilities.html#ga3e70cd84bf811b869c955d75fa80590d>`__
-  whd_pf_get_packet_filter_mask_and_pattern() :
   `whd_wifi_api.h <group__wifiutilities.html#gacd9d57c8febb11cc240db23f5de92cd9>`__
-  whd_pf_get_packet_filter_stats() :
   `whd_wifi_api.h <group__wifiutilities.html#ga26172adb5ec181bb8f2009e47eb252cf>`__
-  whd_pf_remove_packet_filter() :
   `whd_wifi_api.h <group__wifiutilities.html#gaff414d31f3406ec5d1af54b889f4849a>`__
-  whd_print_stats() :
   `whd_wifi_api.h <group__dbg.html#gab6dca3a6d7a90a1f9d8fb18660048c18>`__
-  whd_tko_get_FW_connect() :
   `whd_wifi_api.h <group__wifiutilities.html#gacc2882e3a308f7b2b938bc5741ab43c3>`__
-  whd_tko_get_status() :
   `whd_wifi_api.h <group__wifiutilities.html#gae6b46bcc1a3c7d708abc71ae6a123481>`__
-  whd_tko_max_assoc() :
   `whd_wifi_api.h <group__wifiutilities.html#ga70cacca6b62ca658882d9070c49b6343>`__
-  whd_tko_param() :
   `whd_wifi_api.h <group__wifiutilities.html#ga482c8ffcb1166f2f161e4ad67f8ef9d5>`__
-  whd_tko_toggle() :
   `whd_wifi_api.h <group__wifiutilities.html#ga189ed043d5ce9cf8d1a0845484a96d6b>`__
-  whd_wifi_ap_get_max_assoc() :
   `whd_wifi_api.h <group__wifisoftap.html#ga32982684d093a173a6e578856b581d29>`__
-  whd_wifi_ap_set_beacon_interval() :
   `whd_wifi_api.h <group__wifisoftap.html#gaf2758fcc1028704d801b16d77b96a345>`__
-  whd_wifi_ap_set_dtim_interval() :
   `whd_wifi_api.h <group__wifisoftap.html#ga57c57ad4d5acf499e90ff8db020fa2d9>`__
-  whd_wifi_clear_packet_filter_stats() :
   `whd_wifi_api.h <group__wifiutilities.html#gab3e9371ccaa61f1f40e0246a66db42a6>`__
-  whd_wifi_deauth_sta() :
   `whd_wifi_api.h <group__wifisoftap.html#gad9ad2649c18db8773351a5938e7930e4>`__
-  whd_wifi_deregister_event_handler() :
   `whd_events.h <whd__events_8h.html#ae415f1f3fcbbfe506aa0e656bf8debb3>`__
-  whd_wifi_disable_powersave() :
   `whd_wifi_api.h <group__wifipowersave.html#gab9af7ef8dac544f2bb8871259f85c437>`__
-  whd_wifi_enable_powersave() :
   `whd_wifi_api.h <group__wifipowersave.html#ga05cfafe07ad2c208acfdee29f7c6e626>`__
-  whd_wifi_enable_powersave_with_throughput() :
   `whd_wifi_api.h <group__wifipowersave.html#ga6152d966f454fe8e8836472b802401c3>`__
-  whd_wifi_enable_sup_set_passphrase() :
   `whd_wifi_api.h <group__wifiutilities.html#gae118219bea1b82ae76a5c1ee5f7f302c>`__
-  whd_wifi_enable_supplicant() :
   `whd_wifi_api.h <group__wifiutilities.html#gad2daa1aba4dccd3f0506950ccee42113>`__
-  whd_wifi_get_acparams() :
   `whd_wifi_api.h <group__wifiutilities.html#gafe61d474712d3ce7e15ef371d87f54e5>`__
-  whd_wifi_get_ap_client_rssi() :
   `whd_wifi_api.h <group__wifiutilities.html#gab571b303f19131fd032fba4b744dbf20>`__
-  whd_wifi_get_ap_info() :
   `whd_wifi_api.h <group__wifisoftap.html#ga95c40af4be45d119b737c0113d9a038e>`__
-  whd_wifi_get_associated_client_list() :
   `whd_wifi_api.h <group__wifisoftap.html#ga9e974ab0fcc24698d01a5abc3e2c1dbb>`__
-  whd_wifi_get_bss_info() :
   `whd_wifi_api.h <group__dbg.html#gab4a8c97085b18321d8d682b1578edf7d>`__
-  whd_wifi_get_bssid() :
   `whd_wifi_api.h <group__wifiutilities.html#ga52d3190589758d9fd36a7b27a9d08f28>`__
-  whd_wifi_get_channel() :
   `whd_wifi_api.h <group__wifiutilities.html#gab81522c25d723fbe18b90871a278ba9d>`__
-  whd_wifi_get_clm_version() :
   `whd_wifi_api.h <group__dbg.html#ga1e8a85564910052e82332a89db4d9ff5>`__
-  whd_wifi_get_ioctl_buffer() :
   `whd_wifi_api.h <group__wifiioctl.html#gab1a4c2ae9163408ae92b3f292ee2807f>`__
-  whd_wifi_get_ioctl_value() :
   `whd_wifi_api.h <group__wifiioctl.html#ga817655c1372b8c87608b204105f06937>`__
-  whd_wifi_get_iovar_buffer_with_param() :
   `whd_wifi_api.h <group__wifiioctl.html#ga2b1cdfc36df80ca4700076fd2747b34e>`__
-  whd_wifi_get_listen_interval() :
   `whd_wifi_api.h <group__wifiutilities.html#ga5a62f0854ad148dde280b4f75596a309>`__
-  whd_wifi_get_mac_address() :
   `whd_wifi_api.h <group__wifiutilities.html#ga8a634822973adc75a61a05eb2b664ef5>`__
-  whd_wifi_get_powersave_mode() :
   `whd_wifi_api.h <group__wifipowersave.html#ga3eed267d42020440a077d7649cde0567>`__
-  whd_wifi_get_rssi() :
   `whd_wifi_api.h <group__wifiutilities.html#gaf82eda2ff9979b1bc053a6d7f1f4b125>`__
-  whd_wifi_get_wifi_version() :
   `whd_wifi_api.h <group__dbg.html#ga334e8f04bdbc06da6eadf29cea997b99>`__
-  whd_wifi_init_ap() :
   `whd_wifi_api.h <group__wifisoftap.html#ga647aebc1d86708017fd11029e2e1a51c>`__
-  whd_wifi_is_ready_to_transceive() :
   `whd_wifi_api.h <group__wifiutilities.html#ga07144bbc68755481708752962538ee6f>`__
-  whd_wifi_join() :
   `whd_wifi_api.h <group__wifijoin.html#gac767814ae445f735b51686db20c780be>`__
-  whd_wifi_join_specific() :
   `whd_wifi_api.h <group__wifijoin.html#gae41c4ab0b2f3ecc433a6c4e89238b066>`__
-  whd_wifi_leave() :
   `whd_wifi_api.h <group__wifijoin.html#ga971ed7d8e459c715aa9bf3c52b563d8b>`__
-  whd_wifi_manage_custom_ie() :
   `whd_wifi_api.h <group__wifiutilities.html#gaaf7be7d8099507426f52ecff8f716543>`__
-  whd_wifi_off() :
   `whd_wifi_api.h <group__wifimanagement.html#ga422587a9b104a871233a42aceb2141d2>`__
-  whd_wifi_on() :
   `whd_wifi_api.h <group__wifimanagement.html#ga1c8bf41b593cb947266f8690d495f381>`__
-  whd_wifi_print_whd_log() :
   `whd_wifi_api.h <group__dbg.html#ga6435c76ac1eae5af952053d76d9c67ab>`__
-  whd_wifi_read_wlan_log() :
   `whd_wifi_api.h <group__dbg.html#ga7f089a4c933c9c05a61e2cfc00fcb4f8>`__
-  whd_wifi_register_multicast_address() :
   `whd_wifi_api.h <group__wifiutilities.html#gab36ff07aa1d825c0366c82ce003dd9f6>`__
-  whd_wifi_sae_password() :
   `whd_wifi_api.h <group__wifiutilities.html#ga977fe2ff7c3f6966ffc40fda0e9694a8>`__
-  whd_wifi_scan() :
   `whd_wifi_api.h <group__wifijoin.html#ga82d8c980d15acf0e6e18b56423f4e52b>`__
-  whd_wifi_scan_synch() :
   `whd_wifi_api.h <group__wifijoin.html#gaef3dc76f682c6c193df94897e9d0b71f>`__
-  whd_wifi_send_action_frame() :
   `whd_wifi_api.h <group__wifiutilities.html#ga2cf8ecd7c319660833bd30a060990dca>`__
-  whd_wifi_set_channel() :
   `whd_wifi_api.h <group__wifiutilities.html#ga168fb83b839bab61d2c9d3b1f42a6782>`__
-  whd_wifi_set_coex_config() :
   `whd_wifi_api.h <group__wifiutilities.html#ga95237c0e6182bfb176519421ac9287b4>`__
-  whd_wifi_set_down() :
   `whd_wifi_api.h <group__wifimanagement.html#ga412c2d4653a7c36329c6cf78be344336>`__
-  whd_wifi_set_event_handler() :
   `whd_events.h <group__event.html#ga4175c3ef9bb16601fabc605fadeaf8d8>`__
-  whd_wifi_set_ioctl_buffer() :
   `whd_wifi_api.h <group__wifiioctl.html#ga5d7aec7622caacf4a0642cf1da1fa647>`__
-  whd_wifi_set_ioctl_value() :
   `whd_wifi_api.h <group__wifiioctl.html#gad54ac84589a14cc97429bf138fa32639>`__
-  whd_wifi_set_listen_interval() :
   `whd_wifi_api.h <group__wifiutilities.html#gac93f69169a725f277e955eec0bf528db>`__
-  whd_wifi_set_passphrase() :
   `whd_wifi_api.h <group__wifiutilities.html#ga893ccaef6ed5d0afe95a96bbe89e8a80>`__
-  whd_wifi_set_up() :
   `whd_wifi_api.h <group__wifimanagement.html#ga925da3b1ed914d7bb661d3cb2af50680>`__
-  whd_wifi_start_ap() :
   `whd_wifi_api.h <group__wifisoftap.html#ga3c9aa99add3f6a6d13e9092bd6e1246b>`__
-  whd_wifi_stop_ap() :
   `whd_wifi_api.h <group__wifisoftap.html#ga2c6c28512678dc57dabb641c41e30d41>`__
-  whd_wifi_stop_scan() :
   `whd_wifi_api.h <group__wifijoin.html#gacf8632bb68cef4d831083a66bee7b8fa>`__
-  whd_wifi_toggle_packet_filter() :
   `whd_wifi_api.h <group__wifiutilities.html#ga35860e8c45162735f5ef3d93ed262d90>`__
-  whd_wifi_unregister_multicast_address() :
   `whd_wifi_api.h <group__wifiutilities.html#gad092a543b73e91c7090875ccb940e01f>`__

