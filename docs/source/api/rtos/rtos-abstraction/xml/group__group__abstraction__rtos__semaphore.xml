<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__abstraction__rtos__semaphore" kind="group">
    <compoundname>group_abstraction_rtos_semaphore</compoundname>
    <title>Semaphore</title>
      <sectiondef kind="user-defined">
      <memberdef const="no" explicit="no" id="group__group__abstraction__rtos__semaphore_1ga93fbdafe5929ba0703df7d65e2765bfe" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t cy_rtos_init_semaphore</definition>
        <argsstring>(cy_semaphore_t *semaphore, uint32_t maxcount, uint32_t initcount)</argsstring>
        <name>cy_rtos_init_semaphore</name>
        <param>
          <type><ref kindref="member" refid="group__group__abstraction__rtos__port_1ga97c7350dd41b5850577cc7667aee73d2">cy_semaphore_t</ref> *</type>
          <declname>semaphore</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>maxcount</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>initcount</declname>
        </param>
        <briefdescription>
<para>Create a semaphore. </para>        </briefdescription>
        <detaileddescription>
<para>This is basically a counting semaphore.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="inout">semaphore</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to the semaphore handle to be initialized </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">maxcount</parametername>
</parameternamelist>
<parameterdescription>
<para>The maximum count for this semaphore </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">initcount</parametername>
</parameternamelist>
<parameterdescription>
<para>The initial count for this semaphore</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The status of the semaphore creation. [<ref kindref="member" refid="group__group__abstraction__rtos__common_1gaf58fac450d9fff4472f03ad68f6e546e">CY_RSLT_SUCCESS</ref>, <ref kindref="member" refid="group__group__abstraction__rtos__common_1ga516d500bef7d6e6291f08485d0bc003b">CY_RTOS_NO_MEMORY</ref>, <ref kindref="member" refid="group__group__abstraction__rtos__common_1ga89a0a071bd7a41b5d1f3edb969ee1647">CY_RTOS_GENERAL_ERROR</ref>] </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/include/cyabs_rtos.h" line="374" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__abstraction__rtos__semaphore_1ga4070def066b136082e73547d4e0c3771" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t cy_rtos_get_semaphore</definition>
        <argsstring>(cy_semaphore_t *semaphore, cy_time_t timeout_ms, bool in_isr)</argsstring>
        <name>cy_rtos_get_semaphore</name>
        <param>
          <type><ref kindref="member" refid="group__group__abstraction__rtos__port_1ga97c7350dd41b5850577cc7667aee73d2">cy_semaphore_t</ref> *</type>
          <declname>semaphore</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__abstraction__rtos__port_1ga141349b315f2266d5e0e8b8566eb608b">cy_time_t</ref></type>
          <declname>timeout_ms</declname>
        </param>
        <param>
          <type>bool</type>
          <declname>in_isr</declname>
        </param>
        <briefdescription>
<para>Get/Acquire a semaphore. </para>        </briefdescription>
        <detaileddescription>
<para>If the semaphore count is zero, waits until the semaphore count is greater than zero. Once the semaphore count is greater than zero, this function decrements the count and return. It may also return if the timeout is exceeded.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">semaphore</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to the semaphore handle </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">timeout_ms</parametername>
</parameternamelist>
<parameterdescription>
<para>Maximum number of milliseconds to wait while attempting to get the semaphore. Use the <ref kindref="member" refid="group__group__abstraction__rtos__common_1gab31d8535128df9506a4220c2b1f41a60">CY_RTOS_NEVER_TIMEOUT</ref> constant to wait forever. Must be zero is in_isr is true </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">in_isr</parametername>
</parameternamelist>
<parameterdescription>
<para>true if we are trying to get the semaphore from with an ISR </para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The status of get semaphore operation [<ref kindref="member" refid="group__group__abstraction__rtos__common_1gaf58fac450d9fff4472f03ad68f6e546e">CY_RSLT_SUCCESS</ref>, <ref kindref="member" refid="group__group__abstraction__rtos__common_1gafc6a1c6f563521d6c5555027d5d7bde8">CY_RTOS_TIMEOUT</ref>, <ref kindref="member" refid="group__group__abstraction__rtos__common_1ga516d500bef7d6e6291f08485d0bc003b">CY_RTOS_NO_MEMORY</ref>, <ref kindref="member" refid="group__group__abstraction__rtos__common_1ga89a0a071bd7a41b5d1f3edb969ee1647">CY_RTOS_GENERAL_ERROR</ref>] </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/include/cyabs_rtos.h" line="390" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__abstraction__rtos__semaphore_1gad53319297963096109f708d5f61bac91" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t cy_rtos_set_semaphore</definition>
        <argsstring>(cy_semaphore_t *semaphore, bool in_isr)</argsstring>
        <name>cy_rtos_set_semaphore</name>
        <param>
          <type><ref kindref="member" refid="group__group__abstraction__rtos__port_1ga97c7350dd41b5850577cc7667aee73d2">cy_semaphore_t</ref> *</type>
          <declname>semaphore</declname>
        </param>
        <param>
          <type>bool</type>
          <declname>in_isr</declname>
        </param>
        <briefdescription>
<para>Set/Release a semaphore. </para>        </briefdescription>
        <detaileddescription>
<para>Increments the semaphore count, up to the maximum count for this semaphore.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">semaphore</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to the semaphore handle </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">in_isr</parametername>
</parameternamelist>
<parameterdescription>
<para>Value of true indicates calling from interrupt context Value of false indicates calling from normal thread context </para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The status of set semaphore operation [<ref kindref="member" refid="group__group__abstraction__rtos__common_1gaf58fac450d9fff4472f03ad68f6e546e">CY_RSLT_SUCCESS</ref>, <ref kindref="member" refid="group__group__abstraction__rtos__common_1ga516d500bef7d6e6291f08485d0bc003b">CY_RTOS_NO_MEMORY</ref>, <ref kindref="member" refid="group__group__abstraction__rtos__common_1ga89a0a071bd7a41b5d1f3edb969ee1647">CY_RTOS_GENERAL_ERROR</ref>] </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/include/cyabs_rtos.h" line="402" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__abstraction__rtos__semaphore_1gadc7e84ac4f389499441a792b17147164" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t cy_rtos_get_count_semaphore</definition>
        <argsstring>(cy_semaphore_t *semaphore, size_t *count)</argsstring>
        <name>cy_rtos_get_count_semaphore</name>
        <param>
          <type><ref kindref="member" refid="group__group__abstraction__rtos__port_1ga97c7350dd41b5850577cc7667aee73d2">cy_semaphore_t</ref> *</type>
          <declname>semaphore</declname>
        </param>
        <param>
          <type>size_t *</type>
          <declname>count</declname>
        </param>
        <briefdescription>
<para>Get the count of a semaphore. </para>        </briefdescription>
        <detaileddescription>
<para>Gets the number of available tokens on the semaphore.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">semaphore</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to the semaphore handle </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="out">count</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to the return count </para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The status of get semaphore count operation [<ref kindref="member" refid="group__group__abstraction__rtos__common_1gaf58fac450d9fff4472f03ad68f6e546e">CY_RSLT_SUCCESS</ref>, <ref kindref="member" refid="group__group__abstraction__rtos__common_1ga89a0a071bd7a41b5d1f3edb969ee1647">CY_RTOS_GENERAL_ERROR</ref>] </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/include/cyabs_rtos.h" line="413" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__abstraction__rtos__semaphore_1ga71c073294cd60ae1c6e97a1f3407ae07" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t cy_rtos_deinit_semaphore</definition>
        <argsstring>(cy_semaphore_t *semaphore)</argsstring>
        <name>cy_rtos_deinit_semaphore</name>
        <param>
          <type><ref kindref="member" refid="group__group__abstraction__rtos__port_1ga97c7350dd41b5850577cc7667aee73d2">cy_semaphore_t</ref> *</type>
          <declname>semaphore</declname>
        </param>
        <briefdescription>
<para>Deletes a semaphore. </para>        </briefdescription>
        <detaileddescription>
<para>This function frees the resources associated with a semaphore.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">semaphore</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to the semaphore handle</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The status of semaphore deletion [<ref kindref="member" refid="group__group__abstraction__rtos__common_1gaf58fac450d9fff4472f03ad68f6e546e">CY_RSLT_SUCCESS</ref>, <ref kindref="member" refid="group__group__abstraction__rtos__common_1ga516d500bef7d6e6291f08485d0bc003b">CY_RTOS_NO_MEMORY</ref>, <ref kindref="member" refid="group__group__abstraction__rtos__common_1ga89a0a071bd7a41b5d1f3edb969ee1647">CY_RTOS_GENERAL_ERROR</ref>] </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/include/cyabs_rtos.h" line="424" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>APIs for acquiring and working with Semaphores. </para>    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>