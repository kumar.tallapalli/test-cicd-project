<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="indexpage" kind="group">
    <compoundname>index</compoundname>
    <title>RTOS Abstraction</title>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
<para><heading level="2">Overview</heading>
</para><para>The RTOS abstraction layer provides simple RTOS services like threads, semaphores, mutexes, queues, and timers. It is not intended to be a full features RTOS interface, but the provide just enough support to allow for RTOS independent drivers and middleware. This allows middleware applications to be as portable as possible within ModusToolbox. This library provides a unified API around the actual RTOS. This allows middleware libraries to be written once independent of the RTOS actually selected for the application. The abstraction layer provides access to all the standard RTOS resources listed in the feature section below.</para><para>While the primary purpose of the library is for middleware, the abstraction layer can be used by the application code. However, since this API does not provide all RTOS features and the application generally knows what RTOS is being used, this is typically an unnecessary overhead.</para><para>All the RTOS abstraction layer functions generally all work the same way. The basic process is:<orderedlist>
<listitem><para>Include the cyabs_rtos.h header file so that you have access to the RTOS functions.</para></listitem><listitem><para>Declare a variable of the right type (e.g. cy_mutex_t)</para></listitem><listitem><para>Call the appropriate create or initialize function (e.g. <ref kindref="member" refid="group__group__abstraction__rtos__mutex_1ga79fbca5c574d2bc617af90d4bff6c902">cy_rtos_init_mutex()</ref>). Provide it with a reference to the variable that was created in step 2.</para></listitem><listitem><para>Access the RTOS object using one of the access functions. e.g. <ref kindref="member" refid="group__group__abstraction__rtos__mutex_1ga7b6a9f5fc3910436675c00130399e06d">cy_rtos_set_mutex()</ref>.</para></listitem><listitem><para>If you don't need it anymore, free up the pointer with the appropriate de-init function (e.g. <ref kindref="member" refid="group__group__abstraction__rtos__mutex_1ga8cd21b8a116a54ac397209054f35ee45">cy_rtos_deinit_mutex()</ref>).</para></listitem></orderedlist>
</para><para>NOTE: All these functions need a pointer, so it is generally best to declare these "shared" resources as static global variables within the file that they are used.</para><para><heading level="2">Getting Started</heading>
</para><para>To use the RTOS Abstraction, simply include a reference to cyabs_rtos.h and update the application's makefile to include the appropriate component. e.g. one of:<itemizedlist>
<listitem><para>COMPONENTS+=RTX</para></listitem><listitem><para>COMPONENTS+=FREERTOS</para></listitem><listitem><para>COMPONENTS+=THREADX</para></listitem></itemizedlist>
</para><para><heading level="2">Features</heading>
</para><para><itemizedlist>
<listitem><para>APIs for interacting with common RTOS Features including:<itemizedlist>
<listitem><para>Threads</para></listitem><listitem><para>Mutexes</para></listitem><listitem><para>Semaphores</para></listitem><listitem><para>Timers</para></listitem><listitem><para>Queues</para></listitem><listitem><para>Events</para></listitem></itemizedlist>
</para></listitem><listitem><para>Implementations are provided for<itemizedlist>
<listitem><para>FreeRTOS</para></listitem><listitem><para>RTX (CMSIS RTOS)</para></listitem><listitem><para>ThreadX</para></listitem></itemizedlist>
</para></listitem></itemizedlist>
</para><para><heading level="2">RTOS Configuration Requirements</heading>
</para><para><bold>FreeRTOS</bold>
</para><para>To enable all functionality when using with FreeRTOS, the following configuration options must be enabled in FreeRTOSConfig.h:<itemizedlist>
<listitem><para>configUSE_TRACE_FACILITY</para></listitem><listitem><para>configUSE_MUTEXES</para></listitem><listitem><para>configUSE_RECURSIVE_MUTEXES</para></listitem><listitem><para>configUSE_COUNTING_SEMAPHORES</para></listitem><listitem><para>configSUPPORT_DYNAMIC_ALLOCATION</para></listitem><listitem><para>configSUPPORT_STATIC_ALLOCATION</para></listitem></itemizedlist>
</para><para>Enabling configSUPPORT_STATIC_ALLOCATION requires the application to provide implementations for vApplicationGetIdleTaskMemory and vApplicationGetTimerTaskMemoryfunctions. Weak implementations for these functions are provided as a part of this library. These can be overridden by the application if custom implementations of these functions are desired.<linebreak />
</para><para>This library provides an API vApplicationSleep which can be used to enable tickless support in FreeRTOS. In order to enable tickless mode with this API, the following changes need to be made in FreeRTOSConfig.h:<itemizedlist>
<listitem><para>Enables tickless mode with user specified portSUPPRESS_TICKS_AND_SLEEP implementation.<linebreak />
 #define configUSE_TICKLESS_IDLE 2</para></listitem><listitem><para>Hook portSUPPRESS_TICKS_AND_SLEEP macro to vApplicationSleep implementation.<linebreak />
 #define portSUPPRESS_TICKS_AND_SLEEP( xIdleTime ) vApplicationSleep( xIdleTime )</para></listitem></itemizedlist>
</para><para>For further details on Low power support in FreeRTOS please refer to documentation <ulink url="https://www.freertos.org/low-power-tickless-rtos.html">here</ulink></para><para><bold>RTX / ThreadX</bold>
</para><para>No specific requirements exist</para><para><heading level="2">Porting Notes</heading>
</para><para>In order to port to a new environment, the file cyabs_rtos_impl.h must be provided with definitions of some basic types for the abstraction layer. The types expected to be defined are:</para><para><itemizedlist>
<listitem><para>cy_thread_t : typedef from underlying RTOS thread type</para></listitem><listitem><para>cy_thread_arg_t : typedef from the RTOS type that is passed to the entry function of a thread.</para></listitem><listitem><para>cy_mutex_t : typedef from the underlying RTOS mutex type</para></listitem><listitem><para>cy_semaphore_t: typedef from the underlying RTOS semaphore type</para></listitem><listitem><para>cy_event_t : typedef from the underlying RTOS event type</para></listitem><listitem><para>cy_queue_t : typedef from the underlying RTOS queue type</para></listitem><listitem><para>cy_timer_callback_arg_t : typedef from the RTOS type that is passed to the timer callback function</para></listitem><listitem><para>cy_timer_t : typedef from the underlying RTOS timer type</para></listitem><listitem><para>cy_time_t : count of time in milliseconds</para></listitem><listitem><para>cy_rtos_error_t : typedef from the underlying RTOS error type</para></listitem></itemizedlist>
</para><para>The enum cy_thread_priority_t needs to have the following priority values defined and mapped to RTOS specific values:<itemizedlist>
<listitem><para>CY_RTOS_PRIORITY_MIN</para></listitem><listitem><para>CY_RTOS_PRIORITY_LOW</para></listitem><listitem><para>CY_RTOS_PRIORITY_BELOWNORMAL</para></listitem><listitem><para>CY_RTOS_PRIORITY_NORMAL</para></listitem><listitem><para>CY_RTOS_PRIORITY_ABOVENORMAL</para></listitem><listitem><para>CY_RTOS_PRIORITY_HIGH</para></listitem><listitem><para>CY_RTOS_PRIORITY_REALTIME</para></listitem><listitem><para>CY_RTOS_PRIORITY_MAX</para></listitem></itemizedlist>
</para><para>Finally, the following macros need to be defined for memory allocations:<itemizedlist>
<listitem><para>CY_RTOS_MIN_STACK_SIZE</para></listitem><listitem><para>CY_RTOS_ALIGNMENT</para></listitem><listitem><para>CY_RTOS_ALIGNMENT_MASK</para></listitem></itemizedlist>
</para><para><heading level="2">More information</heading>
</para><para><itemizedlist>
<listitem><para><ulink url="https://cypresssemiconductorco.github.io/abstraction-rtos/html/modules.html">API Reference Guide</ulink></para></listitem><listitem><para><ulink url="http://www.cypress.com">Cypress Semiconductor, an Infineon Technologies Company</ulink></para></listitem><listitem><para><ulink url="https://github.com/cypresssemiconductorco">Cypress Semiconductor GitHub</ulink></para></listitem><listitem><para><ulink url="https://www.cypress.com/products/modustoolbox-software-environment">ModusToolbox</ulink> <hruler />
 © Cypress Semiconductor Corporation, 2019-2020. </para></listitem></itemizedlist>
</para>    </detaileddescription>
  </compounddef>
</doxygen>