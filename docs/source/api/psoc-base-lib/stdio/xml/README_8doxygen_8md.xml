<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="README_8doxygen_8md" kind="file" language="Markdown">
    <compoundname>README.doxygen.md</compoundname>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
    <programlisting>
<codeline><highlight class="normal">#<sp />Retarget<sp />IO</highlight></codeline>
<codeline />
<codeline><highlight class="normal">###<sp />Overview</highlight></codeline>
<codeline />
<codeline><highlight class="normal">A<sp />utility<sp />library<sp />to<sp />retarget<sp />the<sp />standard<sp />input/output<sp />(STDIO)<sp />messages<sp />to<sp />a<sp />UART<sp />port.<sp />With<sp />this<sp />library,<sp />you<sp />can<sp />directly<sp />print<sp />messages<sp />on<sp />a<sp />UART<sp />terminal<sp />using<sp />`printf()`.<sp />You<sp />can<sp />specify<sp />the<sp />TX<sp />pin,<sp />RX<sp />pin,<sp />and<sp />the<sp />baud<sp />rate<sp />through<sp />the<sp />`cy_retarget_io_init()`<sp />function.<sp />The<sp />UART<sp />HAL<sp />object<sp />is<sp />externally<sp />accessible<sp />so<sp />that<sp />you<sp />can<sp />use<sp />it<sp />with<sp />other<sp />UART<sp />HAL<sp />functions.</highlight></codeline>
<codeline />
<codeline><highlight class="normal">**NOTE:**<sp />The<sp />standard<sp />library<sp />is<sp />not<sp />standard<sp />in<sp />how<sp />it<sp />treats<sp />an<sp />I/O<sp />stream.<sp />Some<sp />implement<sp />a<sp />data<sp />buffer<sp />by<sp />default.<sp />The<sp />buffer<sp />is<sp />not<sp />flushed<sp />until<sp />it<sp />is<sp />full.<sp />In<sp />that<sp />case<sp />it<sp />may<sp />appear<sp />that<sp />your<sp />I/O<sp />is<sp />not<sp />working.<sp />You<sp />should<sp />be<sp />aware<sp />of<sp />how<sp />the<sp />library<sp />buffers<sp />data,<sp />and<sp />you<sp />should<sp />identify<sp />a<sp />buffering<sp />strategy<sp />and<sp />buffer<sp />size<sp />for<sp />a<sp />specified<sp />stream.<sp />If<sp />you<sp />supply<sp />a<sp />buffer,<sp />it<sp />must<sp />exist<sp />until<sp />the<sp />stream<sp />is<sp />closed.<sp />The<sp />following<sp />line<sp />of<sp />code<sp />disables<sp />the<sp />buffer<sp />for<sp />the<sp />standard<sp />library<sp />that<sp />accompanies<sp />the<sp />GCC<sp />compiler:</highlight></codeline>
<codeline />
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />setvbuf(<sp />stdin,<sp />NULL,<sp />_IONBF,<sp />0<sp />);</highlight></codeline>
<codeline />
<codeline><highlight class="normal">**NOTE:**<sp />If<sp />the<sp />application<sp />is<sp />built<sp />using<sp />newlib-nano,<sp />by<sp />default,<sp />floating<sp />point<sp />format<sp />strings<sp />(%f)<sp />are<sp />not<sp />supported.<sp />To<sp />enable<sp />this<sp />support,<sp />you<sp />must<sp />add<sp />`-u<sp />_printf_float`<sp />to<sp />the<sp />linker<sp />command<sp />line.</highlight></codeline>
<codeline />
<codeline><highlight class="normal">###<sp />Quick<sp />Start</highlight></codeline>
<codeline><highlight class="normal">1.<sp />Add<sp />#`include<sp />"cy_retarget_io.h"`</highlight></codeline>
<codeline><highlight class="normal">2.<sp />Call<sp />`cy_retarget_io_init(CYBSP_DEBUG_UART_TX,<sp />CYBSP_DEBUG_UART_RX,<sp />CY_RETARGET_IO_BAUDRATE);`</highlight></codeline>
<codeline />
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />`CYBSP_DEBUG_UART_TX`<sp />and<sp />`CYBSP_DEBUG_UART_RX`<sp />pins<sp />are<sp />defined<sp />in<sp />the<sp />BSP<sp />and<sp />`CY_RETARGET_IO_BAUDRATE`<sp />is<sp />set<sp />to<sp />115200.<sp />You<sp />can<sp />use<sp />a<sp />different<sp />baud<sp />rate<sp />if<sp />you<sp />prefer.</highlight></codeline>
<codeline />
<codeline><highlight class="normal">3.<sp />Start<sp />printing<sp />using<sp />`printf()`</highlight></codeline>
<codeline />
<codeline><highlight class="normal">###<sp />Enabling<sp />Conversion<sp />of<sp />'\\n'<sp />into<sp />"\r\n"</highlight></codeline>
<codeline><highlight class="normal">If<sp />you<sp />want<sp />to<sp />use<sp />only<sp />'\\n'<sp />instead<sp />of<sp />"\r\n"<sp />for<sp />printing<sp />a<sp />new<sp />line<sp />using<sp />printf(),<sp />define<sp />the<sp />macro<sp />`CY_RETARGET_IO_CONVERT_LF_TO_CRLF`<sp />using<sp />the<sp />*DEFINES*<sp />variable<sp />in<sp />the<sp />application<sp />Makefile.<sp />The<sp />library<sp />will<sp />then<sp />append<sp />'\\r'<sp />before<sp />'\\n'<sp />character<sp />on<sp />the<sp />output<sp />direction<sp />(STDOUT).<sp />No<sp />conversion<sp />occurs<sp />if<sp />"\r\n"<sp />is<sp />already<sp />present.</highlight></codeline>
<codeline />
<codeline><highlight class="normal">###<sp />More<sp />information</highlight></codeline>
<codeline />
<codeline><highlight class="normal">*<sp />[API<sp />Reference<sp />Guide](https://cypresssemiconductorco.github.io/retarget-io/html/index.html)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[Cypress<sp />Semiconductor,<sp />an<sp />Infineon<sp />Technologies<sp />Company](http://www.cypress.com)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[Cypress<sp />Semiconductor<sp />GitHub](https://github.com/cypresssemiconductorco)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[ModusToolbox](https://www.cypress.com/products/modustoolbox-software-environment)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[PSoC<sp />6<sp />Code<sp />Examples<sp />using<sp />ModusToolbox<sp />IDE](https://github.com/cypresssemiconductorco/Code-Examples-for-ModusToolbox-Software)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[PSoC<sp />6<sp />Middleware](https://github.com/cypresssemiconductorco/psoc6-middleware)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[PSoC<sp />6<sp />Resources<sp />-<sp />KBA223067](https://community.cypress.com/docs/DOC-14644)</highlight></codeline>
<codeline />
<codeline><highlight class="normal">---</highlight></codeline>
<codeline><highlight class="normal">©<sp />Cypress<sp />Semiconductor<sp />Corporation,<sp />2019-2020.</highlight></codeline>
    </programlisting>
    <location file="bsp/libs/retarget-io/README.doxygen.md" />
  </compounddef>
</doxygen>