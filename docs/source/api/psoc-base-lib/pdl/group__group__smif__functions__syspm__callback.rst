===================
Low Power Callback
===================

.. doxygengroup:: group_smif_functions_syspm_callback
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: