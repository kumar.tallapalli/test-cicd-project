===================
Control and Status
===================  
   
.. doxygengroup:: group_crypto_lld_hw
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members:

API Reference
--------------
.. toctree::
   
   group__group__crypto__lld__hw__functions.rst
 