======================
External Memory Flags
======================

.. doxygengroup:: group_smif_macros_flags
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: