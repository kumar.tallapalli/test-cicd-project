==========================
Trigger Mutiplexer Outputs
==========================

.. doxygengroup:: group_trigmux_out_enums
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: