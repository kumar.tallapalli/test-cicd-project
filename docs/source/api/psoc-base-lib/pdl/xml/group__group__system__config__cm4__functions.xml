<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__system__config__cm4__functions" kind="group">
    <compoundname>group_system_config_cm4_functions</compoundname>
    <title>Cortex-M4 Control</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__system__config__cm4__functions_1ga9f6d2ab3f6fcae1b3ddfa8144307abb8" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>uint32_t</type>
        <definition>uint32_t Cy_SysGetCM4Status</definition>
        <argsstring>(void)</argsstring>
        <name>Cy_SysGetCM4Status</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>Returns the Cortex-M4 core power mode. </para>        </briefdescription>
        <detaileddescription>
<para><simplesect kind="return"><para><ref kindref="compound" refid="group__group__system__config__cm4__status__macro">Cortex-M4 Status</ref> </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="302" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/devices/templates/COMPONENT_MTB/COMPONENT_CM0P/system_psoc6_cm0plus.c" bodystart="294" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/devices/templates/COMPONENT_MTB/system_psoc6.h" line="578" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__system__config__cm4__functions_1gac44c12fdb0562403fc055e4e8966b557" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_SysEnableCM4</definition>
        <argsstring>(uint32_t vectorTableOffset)</argsstring>
        <name>Cy_SysEnableCM4</name>
        <param>
          <type>uint32_t</type>
          <declname>vectorTableOffset</declname>
        </param>
        <briefdescription>
<para>Sets vector table base address and enables the Cortex-M4 core. </para>        </briefdescription>
        <detaileddescription>
<para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;If the CPU is already enabled, it is reset and then enabled.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>vectorTableOffset</parametername>
</parameternamelist>
<parameterdescription>
<para>The offset of the vector table base address from memory address 0x00000000. The offset should be multiple to 1024 bytes. </para></parameterdescription>
</parameteritem>
</parameterlist>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="346" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/devices/templates/COMPONENT_MTB/COMPONENT_CM0P/system_psoc6_cm0plus.c" bodystart="317" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/devices/templates/COMPONENT_MTB/system_psoc6.h" line="579" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__system__config__cm4__functions_1ga4df7e46b2841b62a00ab856e5a6f098f" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_SysDisableCM4</definition>
        <argsstring>(void)</argsstring>
        <name>Cy_SysDisableCM4</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>Disables the Cortex-M4 core and waits for the mode to take the effect. </para>        </briefdescription>
        <detaileddescription>
<para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition warning"&gt;&lt;p class="admonition-title"&gt;warning&lt;/p&gt;&lt;p&gt;&lt;p&gt;Do not call the function while the Cortex-M4 is executing because such a call may corrupt/abort a pending bus-transaction by the CPU and cause unexpected behavior in the system including a deadlock. Call the function while the Cortex-M4 core is in the Sleep or Deep Sleep low-power mode. Use the &lt;a href="group__group__syspm.html#"&gt;SysPm (System Power Management)&lt;/a&gt; Power Management (syspm) API to put the CPU into the low-power modes. Use the &lt;a href="group__group__syspm__functions__power__status.html#group__group__syspm__functions__power__status_1ga2b25c32ff6bc5f899352e4a8102af23f"&gt;Cy_SysPm_ReadStatus()&lt;/a&gt; to get a status of the CPU. &lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="382" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/devices/templates/COMPONENT_MTB/COMPONENT_CM0P/system_psoc6_cm0plus.c" bodystart="364" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/devices/templates/COMPONENT_MTB/system_psoc6.h" line="580" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__system__config__cm4__functions_1ga339cc16be57bd7f25181a554eba100fa" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_SysRetainCM4</definition>
        <argsstring>(void)</argsstring>
        <name>Cy_SysRetainCM4</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>Retains the Cortex-M4 core and exists without waiting for the mode to take effect. </para>        </briefdescription>
        <detaileddescription>
<para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;The retained mode can be entered only from the enabled mode.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition warning"&gt;&lt;p class="admonition-title"&gt;warning&lt;/p&gt;&lt;p&gt;&lt;p&gt;Do not call the function while the Cortex-M4 is executing because such a call may corrupt/abort a pending bus-transaction by the CPU and cause unexpected behavior in the system including a deadlock. Call the function while the Cortex-M4 core is in the Sleep or Deep Sleep low-power mode. Use the &lt;a href="group__group__syspm.html#"&gt;SysPm (System Power Management)&lt;/a&gt; Power Management (syspm) API to put the CPU into the low-power modes. Use the &lt;a href="group__group__syspm__functions__power__status.html#group__group__syspm__functions__power__status_1ga2b25c32ff6bc5f899352e4a8102af23f"&gt;Cy_SysPm_ReadStatus()&lt;/a&gt; to get a status of the CPU. &lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="415" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/devices/templates/COMPONENT_MTB/COMPONENT_CM0P/system_psoc6_cm0plus.c" bodystart="402" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/devices/templates/COMPONENT_MTB/system_psoc6.h" line="581" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__system__config__cm4__functions_1ga7ec02431317f327f05a5460e0301c36c" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_SysResetCM4</definition>
        <argsstring>(void)</argsstring>
        <name>Cy_SysResetCM4</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>Resets the Cortex-M4 core and waits for the mode to take the effect. </para>        </briefdescription>
        <detaileddescription>
<para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;The reset mode can not be entered from the retained mode.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition warning"&gt;&lt;p class="admonition-title"&gt;warning&lt;/p&gt;&lt;p&gt;&lt;p&gt;Do not call the function while the Cortex-M4 is executing because such a call may corrupt/abort a pending bus-transaction by the CPU and cause unexpected behavior in the system including a deadlock. Call the function while the Cortex-M4 core is in the Sleep or Deep Sleep low-power mode. Use the &lt;a href="group__group__syspm.html#"&gt;SysPm (System Power Management)&lt;/a&gt; Power Management (syspm) API to put the CPU into the low-power modes. Use the &lt;a href="group__group__syspm__functions__power__status.html#group__group__syspm__functions__power__status_1ga2b25c32ff6bc5f899352e4a8102af23f"&gt;Cy_SysPm_ReadStatus()&lt;/a&gt; to get a status of the CPU. &lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="452" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/devices/templates/COMPONENT_MTB/COMPONENT_CM0P/system_psoc6_cm0plus.c" bodystart="434" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/devices/templates/COMPONENT_MTB/system_psoc6.h" line="582" />
      </memberdef>
      </sectiondef>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>