<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__sysclk__clk__slow__funcs" kind="group">
    <compoundname>group_sysclk_clk_slow_funcs</compoundname>
    <title>Functions</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__sysclk__clk__slow__funcs_1ga76036eca4604295f03ccf0aeff45b685" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_SysClk_ClkSlowSetDivider</definition>
        <argsstring>(uint8_t divider)</argsstring>
        <name>Cy_SysClk_ClkSlowSetDivider</name>
        <param>
          <type>uint8_t</type>
          <declname>divider</declname>
        </param>
        <briefdescription>
<para>Sets the clock divider for the slow clock. </para>        </briefdescription>
        <detaileddescription>
<para>The source of this clock is the peripheral clock (clkPeri), which is sourced from clkHf[0].</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>divider</parametername>
</parameternamelist>
<parameterdescription>
<para>Divider value between 0 and 255. Causes integer division of (divider value + 1), or division by 1 to 256.</para></parameterdescription>
</parameteritem>
</parameterlist>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;Call &lt;a href="group__group__system__config__system__functions.html#group__group__system__config__system__functions_1gae0c36a9591fe6e9c45ecb21a794f0f0f"&gt;SystemCoreClockUpdate&lt;/a&gt; after this function calling.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />HFCLK0<sp />is<sp />configured<sp />and<sp />enabled.<sp />The<sp />Slow<sp />clock<sp />sourcing<sp />the</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />CM0+<sp />core<sp />must<sp />run<sp />at<sp />a<sp />frequency<sp />that<sp />is<sp />1/32<sp />of<sp />HFCLK0.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Slow<sp />clock<sp />is<sp />sourced<sp />from<sp />Peri<sp />clock.<sp />Set<sp />the<sp />divider<sp />to<sp />1<sp />(freq<sp />=<sp />HFCLK0)<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sysclk__clk__peri__funcs_1ga43eae2e50299c3fe43d3fa9f0442af20">Cy_SysClk_ClkPeriSetDivider</ref>(1u);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal">(32u<sp />!=<sp /><ref kindref="member" refid="group__group__sysclk__clk__slow__funcs_1gaf8bf13eae5628c9c831549090030cac5">Cy_SysClk_ClkSlowGetDivider</ref>())</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sysclk__clk__slow__funcs_1ga76036eca4604295f03ccf0aeff45b685">Cy_SysClk_ClkSlowSetDivider</ref>(32u);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Get<sp />the<sp />CM0+<sp />clock<sp />source<sp />frequency<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />clkSlowfreq<sp />=<sp /><ref kindref="member" refid="group__group__sysclk__clk__slow__funcs_1gaca16e734419a0c8c11830e774877342d">Cy_SysClk_ClkSlowGetFrequency</ref>();</highlight></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="3093" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sysclk.h" bodystart="3086" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sysclk.h" line="3042" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__sysclk__clk__slow__funcs_1gaf8bf13eae5628c9c831549090030cac5" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE uint8_t</type>
        <definition>__STATIC_INLINE uint8_t Cy_SysClk_ClkSlowGetDivider</definition>
        <argsstring>(void)</argsstring>
        <name>Cy_SysClk_ClkSlowGetDivider</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>Reports the divider value for the slow clock. </para>        </briefdescription>
        <detaileddescription>
<para><simplesect kind="return"><para>The divider value. The integer division done is by (divider value + 1), or division by 1 to 256.</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />HFCLK0<sp />is<sp />configured<sp />and<sp />enabled.<sp />The<sp />Slow<sp />clock<sp />sourcing<sp />the</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />CM0+<sp />core<sp />must<sp />run<sp />at<sp />a<sp />frequency<sp />that<sp />is<sp />1/32<sp />of<sp />HFCLK0.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Slow<sp />clock<sp />is<sp />sourced<sp />from<sp />Peri<sp />clock.<sp />Set<sp />the<sp />divider<sp />to<sp />1<sp />(freq<sp />=<sp />HFCLK0)<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sysclk__clk__peri__funcs_1ga43eae2e50299c3fe43d3fa9f0442af20">Cy_SysClk_ClkPeriSetDivider</ref>(1u);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal">(32u<sp />!=<sp /><ref kindref="member" refid="group__group__sysclk__clk__slow__funcs_1gaf8bf13eae5628c9c831549090030cac5">Cy_SysClk_ClkSlowGetDivider</ref>())</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sysclk__clk__slow__funcs_1ga76036eca4604295f03ccf0aeff45b685">Cy_SysClk_ClkSlowSetDivider</ref>(32u);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Get<sp />the<sp />CM0+<sp />clock<sp />source<sp />frequency<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />clkSlowfreq<sp />=<sp /><ref kindref="member" refid="group__group__sysclk__clk__slow__funcs_1gaca16e734419a0c8c11830e774877342d">Cy_SysClk_ClkSlowGetFrequency</ref>();</highlight></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="3112" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sysclk.h" bodystart="3109" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sysclk.h" line="3043" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__sysclk__clk__slow__funcs_1gaca16e734419a0c8c11830e774877342d" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE uint32_t</type>
        <definition>__STATIC_INLINE uint32_t Cy_SysClk_ClkSlowGetFrequency</definition>
        <argsstring>(void)</argsstring>
        <name>Cy_SysClk_ClkSlowGetFrequency</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>Reports the frequency of the slow clock. </para>        </briefdescription>
        <detaileddescription>
<para><simplesect kind="return"><para>The frequency, in Hz.</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />HFCLK0<sp />is<sp />configured<sp />and<sp />enabled.<sp />The<sp />Slow<sp />clock<sp />sourcing<sp />the</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />CM0+<sp />core<sp />must<sp />run<sp />at<sp />a<sp />frequency<sp />that<sp />is<sp />1/32<sp />of<sp />HFCLK0.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Slow<sp />clock<sp />is<sp />sourced<sp />from<sp />Peri<sp />clock.<sp />Set<sp />the<sp />divider<sp />to<sp />1<sp />(freq<sp />=<sp />HFCLK0)<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sysclk__clk__peri__funcs_1ga43eae2e50299c3fe43d3fa9f0442af20">Cy_SysClk_ClkPeriSetDivider</ref>(1u);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal">(32u<sp />!=<sp /><ref kindref="member" refid="group__group__sysclk__clk__slow__funcs_1gaf8bf13eae5628c9c831549090030cac5">Cy_SysClk_ClkSlowGetDivider</ref>())</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sysclk__clk__slow__funcs_1ga76036eca4604295f03ccf0aeff45b685">Cy_SysClk_ClkSlowSetDivider</ref>(32u);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Get<sp />the<sp />CM0+<sp />clock<sp />source<sp />frequency<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />clkSlowfreq<sp />=<sp /><ref kindref="member" refid="group__group__sysclk__clk__slow__funcs_1gaca16e734419a0c8c11830e774877342d">Cy_SysClk_ClkSlowGetFrequency</ref>();</highlight></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="3066" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sysclk.h" bodystart="3059" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sysclk.h" line="3044" />
      </memberdef>
      </sectiondef>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>