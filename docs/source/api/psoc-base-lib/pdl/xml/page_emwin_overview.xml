<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="page_emwin_overview" kind="group">
    <compoundname>page_emwin_overview</compoundname>
    <title>emWin Overview</title>
    <detaileddescription>
<para><heading level="1">What is emWin?</heading>
</para><para>emWin is an embedded graphic library and graphical user interface (GUI) framework designed to provide an efficient, processor- and LCD controller-independent GUI for any application that operates with a graphical display. It is compatible with single-task and multitask environments. Developed by SEGGER Microcontroller, emWin is extremely popular in the embedded industry. Cypress has licensed the emWin library from SEGGER and offers it for free to customers. The emWin User &amp; Reference Guide located <ulink url="../../../_static/file/api/psoc-base-lib/pdl/UM03001_emWin5.pdf">here</ulink>.</para><para><heading level="1">Supported Features</heading>
</para><para><itemizedlist>
<listitem><para>2-D Graphics Library</para></listitem><listitem><para>Displaying bitmap files</para></listitem><listitem><para>Fonts</para></listitem><listitem><para>Memory Devices</para></listitem><listitem><para>Multitask (RTOS)</para></listitem><listitem><para>Window Manager</para></listitem><listitem><para>Window Objects (Widgets)</para></listitem><listitem><para>Virtual Screens / Virtual Pages</para></listitem><listitem><para>Pointer Input Devices<itemizedlist>
<listitem><para>Touch screen support (user defined)</para></listitem><listitem><para>Sprites and Cursors</para></listitem></itemizedlist>
</para></listitem><listitem><para>Antialiasing</para></listitem><listitem><para>Language Support<itemizedlist>
<listitem><para>Multi-codepages support</para></listitem><listitem><para>Unicode support</para></listitem><listitem><para>Right-to-left and bidirectional text support</para></listitem></itemizedlist>
</para></listitem><listitem><para>Drivers<itemizedlist>
<listitem><para>Compact Color</para></listitem><listitem><para>Flex Color</para></listitem><listitem><para>BitPlains</para></listitem><listitem><para>Control - Cypress custom driver for GraphicLCDCtrl Component to support displays without controller and VRAM.</para></listitem></itemizedlist>
</para></listitem></itemizedlist>
</para><para><heading level="1">Available Options</heading>
</para><para>emWin has the following component options:</para><para><itemizedlist>
<listitem><para>Core - defines RTOS or Touchscreen support. Available options: <verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Option &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;OS &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Touch Screen  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;nOSnTS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;NO &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;NO &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;nOSTS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;NO &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;YES &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;OSnTS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;YES &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;NO &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;OSTS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;YES &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;YES &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;Additionaly, in the ModusToolbox you can select SoftFP or HardFP versions of all specified options.</verbatim></para><para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;When emWin core with RTOS support is selected, an GUI_X_RTOS.c configuration file is added to the project. By default, GUI_X_RTOS.c is configured to work with FreeRTOS. Modify this file to use emWin with any other RTOS.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;For multitasking support, emWin uses a FreeRTOS mutex resource. To enable FreeRTOS mutex support, set configUSE_MUTEXES to 1 in the FreeRTOSConfig.h file.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para></listitem><listitem><para>Display Driver - defines different display support. Available options: FlexColor, CompactColor_16, BitPlains, Control.</para></listitem></itemizedlist>
</para><para><heading level="1">Using emWin in a PSoC Creator project</heading>
</para><para><orderedlist>
<listitem><para>Enable emWin in the PDL pack settings, and choose the following options: OS and Touch support, Display Driver. <image name="emWin_build_settings.png" type="html">Figure 1. emWin settings</image>
</para></listitem><listitem><para>Choose a display interface for communicating with the display. For PSoC Creator, you can place the desired interface Component like GraphicLCDIntr, GraphicLCDCntrl, SPI, or I2C in the TopDesign schematic and configure it with the Component customizer. Alternatively, you can implement a custom interface.</para></listitem><listitem><para>Generate the application code. All configuration files will be added to your project.</para></listitem></orderedlist>
</para><para><heading level="1">Using emWin in a ModusToolbox project</heading>
</para><para><orderedlist>
<listitem><para>Open the Middleware Selector from the required mainapp project. Select emWin Core and emWin Display Driver components.</para></listitem><listitem><para>Click OK. The Middleware Selector adds the emWin config files to the selected project.</para></listitem></orderedlist>
</para><para><heading level="1">emWin Basic Configuration</heading>
</para><para><orderedlist>
<listitem><para>Specify the LCD resolution, color conversion, and other required LCD settings in the LCDConfig.c file.</para></listitem><listitem><para>Modify the emWin config files for the required display driver interface access functions. For details on the display access API, refer to Chapter 33 <bold>Display Drivers</bold> of <ulink url="../../../_static/file/api/psoc-base-lib/pdl/UM03001_emWin5.pdf">emWin User &amp; Reference Guide</ulink>:<itemizedlist>
<listitem><para><bold>CompactColor_16 driver:</bold> specify the LCD_WRITE... and LCD_READ... macros of the LCDConf_CompactColor_16.h file. For example, to initialize the GraphicLCDIntf Component in 8-bit mode: <programlisting><codeline><highlight class="normal">#define<sp />LCD_WRITEM_A1(p,<sp />Num)<sp /><sp /><sp /><sp /><sp /><sp /><sp />GraphicLCDIntf_1_WriteM8_A1(p,<sp />Num)</highlight></codeline>
<codeline><highlight class="normal">#define<sp />LCD_WRITEM_A0(p,<sp />Num)<sp /><sp /><sp /><sp /><sp /><sp /><sp />GraphicLCDIntf_1_WriteM8_A0(p,<sp />Num)</highlight></codeline>
<codeline><highlight class="normal">#define<sp />LCD_READM_A1(p,<sp />Num)<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />GraphicLCDIntf_1_ReadM8_A1(p,<sp />Num)</highlight></codeline>
<codeline><highlight class="normal">#define<sp />LCD_WRITE_A0(Data)<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />GraphicLCDIntf_1_Write8_A0(Data)</highlight></codeline>
<codeline><highlight class="normal">#define<sp />LCD_WRITE_A1(Data)<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />GraphicLCDIntf_1_Write8_A1(Data)</highlight></codeline>
</programlisting></para></listitem><listitem><para><bold>FlexColor driver:</bold> Specify the list of the LCD interface access functions in the LCD_X_Config() function (LCDConfig.c file). For this, modify the GUI_PORT_API PortApi structure passed as the pointer to GUIDRV_FlexColor_SetFunc(). For example, to initialize the GraphicLCDIntf Component in 8-bit mode: <programlisting><codeline><highlight class="normal">GUI_PORT_API<sp />PortAPI<sp />=<sp />{0};</highlight></codeline>
<codeline><highlight class="normal">…</highlight></codeline>
<codeline><highlight class="normal">PortAPI.pfWrite8_A0<sp /><sp />=<sp />GraphicLCDIntf_1_Write8_A0;</highlight></codeline>
<codeline><highlight class="normal">PortAPI.pfWrite8_A1<sp /><sp />=<sp />GraphicLCDIntf_1_Write8_A1;</highlight></codeline>
<codeline><highlight class="normal">PortAPI.pfWriteM8_A1<sp />=<sp />GraphicLCDIntf_1_WriteM8_A1;</highlight></codeline>
<codeline><highlight class="normal">PortAPI.pfRead8_A1<sp /><sp /><sp />=<sp />GraphicLCDIntf_1_Read8_A1;</highlight></codeline>
<codeline><highlight class="normal">PortAPI.pfReadM8_A1<sp /><sp />=<sp />GraphicLCDIntf_1_ReadM8_A1;</highlight></codeline>
<codeline><highlight class="normal">GUIDRV_FlexColor_SetFunc(pDevice,<sp />&amp;PortAPI,<sp />LCD_CONTROLLER,<sp />GUIDRV_FLEXCOLOR_M16C0B8);</highlight></codeline>
</programlisting></para></listitem><listitem><para><bold>BitPlains driver</bold>: only manage the content of the bit plains. It does not contain any display controller specific code. Implement your own display access and update routines.</para></listitem><listitem><para><bold>Control driver</bold>: No modification is needed. Instead, set all required display parameters in GraphicLCDCntrl Component customizer. <verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;This driver is available only in PSoC Creator.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para></listitem></itemizedlist>
</para></listitem><listitem><para>Insert the initialization code for the display interface and the display driver IC in the _InitController() function of the LCDConf.c configuration file.</para></listitem><listitem><para>Include GUI.h header in your source file.</para></listitem><listitem><para>Call the GUI_Init() function to start emWin.</para></listitem><listitem><para>Call the drawing function.</para></listitem></orderedlist>
</para><para><heading level="1">emWin Example Projects</heading>
</para><para>The following code examples demonstrate how to use emWin, and work without modification:<itemizedlist>
<listitem><para>CE223726 - EmWin Graphics Library - TFT Display</para></listitem><listitem><para>CE223727 - EmWin Graphics Library - EInk Display</para></listitem></itemizedlist>
</para><para>Segger also provides many examples of emWin usage. These examples are in the <emphasis>/libraries/psoc6sw-1.0/components/psoc6mw/emWin/sample</emphasis> directory. To run an example:<orderedlist>
<listitem><para>Add one of the demo sample files to your project.</para></listitem><listitem><para>Configure emWin as described in the "emWin Basic Configuration" section.</para></listitem><listitem><para>Edit the main source file (main.c) to include GUI.h. Enable interrupts and then call MainTask() as shown in the code that follows. All of the emWin examples use MainTask() as the entry point. <programlisting><codeline><highlight class="normal">#include<sp />"project.h"</highlight></codeline>
<codeline><highlight class="normal">#include<sp />"GUI.h"<sp /></highlight></codeline>
<codeline />
<codeline><highlight class="normal">int<sp />main()</highlight></codeline>
<codeline><highlight class="normal">{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />__enable_irq();</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />MainTask();</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />for(;;)</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal">}</highlight></codeline>
</programlisting></para></listitem><listitem><para>Build and run the project. </para></listitem></orderedlist>
</para>    </detaileddescription>
  </compounddef>
</doxygen>