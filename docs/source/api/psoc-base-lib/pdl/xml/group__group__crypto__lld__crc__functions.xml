<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__crypto__lld__crc__functions" kind="group">
    <compoundname>group_crypto_lld_crc_functions</compoundname>
    <title>Functions</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__crypto__lld__crc__functions_1gab46a10d2a00389c46d395ec11558fd2a" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE <ref kindref="member" refid="group__group__crypto__enums_1ga3a65158da3dc5cb6aa016ae599542aad">cy_en_crypto_status_t</ref></type>
        <definition>__STATIC_INLINE cy_en_crypto_status_t Cy_Crypto_Core_Crc_Init</definition>
        <argsstring>(CRYPTO_Type *base, uint32_t polynomial, uint32_t dataReverse, uint32_t dataXor, uint32_t remReverse, uint32_t remXor)</argsstring>
        <name>Cy_Crypto_Core_Crc_Init</name>
        <param>
          <type>CRYPTO_Type *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>polynomial</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>dataReverse</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>dataXor</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>remReverse</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>remXor</declname>
        </param>
        <briefdescription>
<para>Initializes the CRC calculation. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the CRYPTO instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>polynomial</parametername>
</parameternamelist>
<parameterdescription>
<para>The polynomial (specified using 32 bits) used in the computing CRC.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>dataReverse</parametername>
</parameternamelist>
<parameterdescription>
<para>The order in which data bytes are processed. 0 - MSB first; 1- LSB first.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>dataXor</parametername>
</parameternamelist>
<parameterdescription>
<para>The byte mask for XORing data.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>remReverse</parametername>
</parameternamelist>
<parameterdescription>
<para>A remainder reverse: 0 means the remainder is not reversed. 1 means reversed.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>remXor</parametername>
</parameternamelist>
<parameterdescription>
<para>Specifies the mask with which the LFSR32 register is XORed to produce a remainder.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="member" refid="group__group__crypto__enums_1ga3a65158da3dc5cb6aa016ae599542aad">cy_en_crypto_status_t</ref> </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="108" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_crypto_core_crc.h" bodystart="89" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_crypto_core_crc.h" line="89" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__crypto__lld__crc__functions_1gafcb164de5cf02e92d139848ed3d06b9b" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE <ref kindref="member" refid="group__group__crypto__enums_1ga3a65158da3dc5cb6aa016ae599542aad">cy_en_crypto_status_t</ref></type>
        <definition>__STATIC_INLINE cy_en_crypto_status_t Cy_Crypto_Core_Crc</definition>
        <argsstring>(CRYPTO_Type *base, uint32_t *crc, void const *data, uint32_t dataSize, uint32_t lfsrInitState)</argsstring>
        <name>Cy_Crypto_Core_Crc</name>
        <param>
          <type>CRYPTO_Type *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t *</type>
          <declname>crc</declname>
        </param>
        <param>
          <type>void const *</type>
          <declname>data</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>dataSize</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>lfsrInitState</declname>
        </param>
        <briefdescription>
<para>Performs the CRC calculation on a message. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the CRYPTO instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>crc</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to a computed CRC value. Must be 4-byte aligned.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>data</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the message whose CRC is being computed.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>dataSize</parametername>
</parameternamelist>
<parameterdescription>
<para>The size of a message in bytes.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>lfsrInitState</parametername>
</parameternamelist>
<parameterdescription>
<para>The initial state of the LFSR.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="member" refid="group__group__crypto__enums_1ga3a65158da3dc5cb6aa016ae599542aad">cy_en_crypto_status_t</ref> </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="153" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_crypto_core_crc.h" bodystart="135" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_crypto_core_crc.h" line="135" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__crypto__lld__crc__functions_1ga881e13920b637d25be9d8f98f5b10c91" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE <ref kindref="member" refid="group__group__crypto__enums_1ga3a65158da3dc5cb6aa016ae599542aad">cy_en_crypto_status_t</ref></type>
        <definition>__STATIC_INLINE cy_en_crypto_status_t Cy_Crypto_Core_Crc_CalcInit</definition>
        <argsstring>(CRYPTO_Type *base, uint32_t width, uint32_t polynomial, uint32_t dataReverse, uint32_t dataXor, uint32_t remReverse, uint32_t remXor, uint32_t lfsrInitState)</argsstring>
        <name>Cy_Crypto_Core_Crc_CalcInit</name>
        <param>
          <type>CRYPTO_Type *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>width</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>polynomial</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>dataReverse</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>dataXor</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>remReverse</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>remXor</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>lfsrInitState</declname>
        </param>
        <briefdescription>
<para>Initializes the CRC calculation. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the CRYPTO instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>width</parametername>
</parameternamelist>
<parameterdescription>
<para>The CRC width in bits.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>polynomial</parametername>
</parameternamelist>
<parameterdescription>
<para>The polynomial (specified using 32 bits) used in the computing CRC.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>dataReverse</parametername>
</parameternamelist>
<parameterdescription>
<para>The order in which data bytes are processed. 0 - MSB first; 1 - LSB first.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>dataXor</parametername>
</parameternamelist>
<parameterdescription>
<para>The byte mask for XORing data</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>remReverse</parametername>
</parameternamelist>
<parameterdescription>
<para>A remainder reverse: 0 means the remainder is not reversed. 1 means reversed.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>remXor</parametername>
</parameternamelist>
<parameterdescription>
<para>Specifies the mask with which the LFSR32 register is XORed to produce a remainder.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>lfsrInitState</parametername>
</parameternamelist>
<parameterdescription>
<para>The initial state of the LFSR.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="member" refid="group__group__crypto__enums_1ga3a65158da3dc5cb6aa016ae599542aad">cy_en_crypto_status_t</ref> </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="212" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_crypto_core_crc.h" bodystart="189" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_crypto_core_crc.h" line="189" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__crypto__lld__crc__functions_1ga0938a3c6dbdad2335b596484993f792f" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE <ref kindref="member" refid="group__group__crypto__enums_1ga3a65158da3dc5cb6aa016ae599542aad">cy_en_crypto_status_t</ref></type>
        <definition>__STATIC_INLINE cy_en_crypto_status_t Cy_Crypto_Core_Crc_CalcStart</definition>
        <argsstring>(CRYPTO_Type *base, uint32_t width, uint32_t lfsrInitState)</argsstring>
        <name>Cy_Crypto_Core_Crc_CalcStart</name>
        <param>
          <type>CRYPTO_Type *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>width</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>lfsrInitState</declname>
        </param>
        <briefdescription>
<para>Prepares the CRC calculation by setting an initial seed value. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the CRYPTO instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>width</parametername>
</parameternamelist>
<parameterdescription>
<para>The CRC width in bits.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>lfsrInitState</parametername>
</parameternamelist>
<parameterdescription>
<para>The initial state of the LFSR.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="member" refid="group__group__crypto__enums_1ga3a65158da3dc5cb6aa016ae599542aad">cy_en_crypto_status_t</ref> </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="248" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_crypto_core_crc.h" bodystart="233" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_crypto_core_crc.h" line="233" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__crypto__lld__crc__functions_1gabe76f7cfc5b707af5258b501a23694ea" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE <ref kindref="member" refid="group__group__crypto__enums_1ga3a65158da3dc5cb6aa016ae599542aad">cy_en_crypto_status_t</ref></type>
        <definition>__STATIC_INLINE cy_en_crypto_status_t Cy_Crypto_Core_Crc_CalcPartial</definition>
        <argsstring>(CRYPTO_Type *base, void const *data, uint32_t dataSize)</argsstring>
        <name>Cy_Crypto_Core_Crc_CalcPartial</name>
        <param>
          <type>CRYPTO_Type *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>void const *</type>
          <declname>data</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>dataSize</declname>
        </param>
        <briefdescription>
<para>Performs the CRC calculation of a message part. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the CRYPTO instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>data</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the message whose CRC is being computed.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>dataSize</parametername>
</parameternamelist>
<parameterdescription>
<para>The size of a message in bytes.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="member" refid="group__group__crypto__enums_1ga3a65158da3dc5cb6aa016ae599542aad">cy_en_crypto_status_t</ref> </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="284" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_crypto_core_crc.h" bodystart="269" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_crypto_core_crc.h" line="269" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__crypto__lld__crc__functions_1ga631f9fe05a740971456d754637d481af" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE <ref kindref="member" refid="group__group__crypto__enums_1ga3a65158da3dc5cb6aa016ae599542aad">cy_en_crypto_status_t</ref></type>
        <definition>__STATIC_INLINE cy_en_crypto_status_t Cy_Crypto_Core_Crc_CalcFinish</definition>
        <argsstring>(CRYPTO_Type *base, uint32_t width, uint32_t *crc)</argsstring>
        <name>Cy_Crypto_Core_Crc_CalcFinish</name>
        <param>
          <type>CRYPTO_Type *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>width</declname>
        </param>
        <param>
          <type>uint32_t *</type>
          <declname>crc</declname>
        </param>
        <briefdescription>
<para>Finalizes the CRC calculation. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the CRYPTO instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>width</parametername>
</parameternamelist>
<parameterdescription>
<para>The CRC width in bits.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>crc</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to a computed CRC value. Must be 4-byte aligned.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="member" refid="group__group__crypto__enums_1ga3a65158da3dc5cb6aa016ae599542aad">cy_en_crypto_status_t</ref> </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="319" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_crypto_core_crc.h" bodystart="305" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_crypto_core_crc.h" line="305" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__crypto__lld__crc__functions_1ga4f97802fcf9f38711103e24f1ad9c584" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE <ref kindref="member" refid="group__group__crypto__enums_1ga3a65158da3dc5cb6aa016ae599542aad">cy_en_crypto_status_t</ref></type>
        <definition>__STATIC_INLINE cy_en_crypto_status_t Cy_Crypto_Core_Crc_Calc</definition>
        <argsstring>(CRYPTO_Type *base, uint32_t width, uint32_t *crc, void const *data, uint32_t dataSize)</argsstring>
        <name>Cy_Crypto_Core_Crc_Calc</name>
        <param>
          <type>CRYPTO_Type *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>width</declname>
        </param>
        <param>
          <type>uint32_t *</type>
          <declname>crc</declname>
        </param>
        <param>
          <type>void const *</type>
          <declname>data</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>dataSize</declname>
        </param>
        <briefdescription>
<para>Performs the CRC calculation on a message. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the CRYPTO instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>width</parametername>
</parameternamelist>
<parameterdescription>
<para>The CRC width in bits.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>crc</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to a computed CRC value. Must be 4-byte aligned.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>data</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the message whose CRC is being computed.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>dataSize</parametername>
</parameternamelist>
<parameterdescription>
<para>The size of a message in bytes.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="member" refid="group__group__crypto__enums_1ga3a65158da3dc5cb6aa016ae599542aad">cy_en_crypto_status_t</ref> </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="362" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_crypto_core_crc.h" bodystart="346" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_crypto_core_crc.h" line="346" />
      </memberdef>
      </sectiondef>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>