<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__rtc__interrupt__functions" kind="group">
    <compoundname>group_rtc_interrupt_functions</compoundname>
    <title>Interrupt</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__rtc__interrupt__functions_1ga47cd2af8969302fd8fc5a7e661eba819" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_RTC_Interrupt</definition>
        <argsstring>(cy_stc_rtc_dst_t const *dstTime, bool mode)</argsstring>
        <name>Cy_RTC_Interrupt</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__rtc__dst__t">cy_stc_rtc_dst_t</ref> const *</type>
          <declname>dstTime</declname>
        </param>
        <param>
          <type>bool</type>
          <declname>mode</declname>
        </param>
        <briefdescription>
<para>The interrupt handler function which should be called in user provided RTC interrupt function. </para>        </briefdescription>
        <detaileddescription>
<para>This is the handler of the RTC interrupt in CPU NVIC. The handler checks which RTC interrupt was asserted and calls the respective RTC interrupt handler functions: <ref kindref="member" refid="group__group__rtc__interrupt__functions_1ga5c2c4659f4165a5ed5fa2b0cafd2c06c">Cy_RTC_Alarm1Interrupt()</ref>, <ref kindref="member" refid="group__group__rtc__interrupt__functions_1ga7aa58fabe8faf1b1b21dbe8bdcd1e919">Cy_RTC_Alarm2Interrupt()</ref> or <ref kindref="member" refid="group__group__rtc__interrupt__functions_1gab9511169906c3a9f1130fdcd826a15e7">Cy_RTC_DstInterrupt()</ref>, and <ref kindref="member" refid="group__group__rtc__interrupt__functions_1gadac651af6dcd8ad0affb2243af2c005c">Cy_RTC_CenturyInterrupt()</ref>.</para><para>The order of the RTC handler functions execution is incremental. <ref kindref="member" refid="group__group__rtc__interrupt__functions_1ga5c2c4659f4165a5ed5fa2b0cafd2c06c">Cy_RTC_Alarm1Interrupt()</ref> is run as the first one and <ref kindref="member" refid="group__group__rtc__interrupt__functions_1gadac651af6dcd8ad0affb2243af2c005c">Cy_RTC_CenturyInterrupt()</ref> is called as the last one.</para><para>This function clears the RTC interrupt every time when it is called.</para><para><ref kindref="member" refid="group__group__rtc__interrupt__functions_1gab9511169906c3a9f1130fdcd826a15e7">Cy_RTC_DstInterrupt()</ref> function is called instead of <ref kindref="member" refid="group__group__rtc__interrupt__functions_1ga7aa58fabe8faf1b1b21dbe8bdcd1e919">Cy_RTC_Alarm2Interrupt()</ref> in condition that the mode parameter is true.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>dstTime</parametername>
</parameternamelist>
<parameterdescription>
<para>The daylight saving time configuration structure, see <ref kindref="compound" refid="structcy__stc__rtc__dst__t">cy_stc_rtc_dst_t</ref>.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>mode</parametername>
</parameternamelist>
<parameterdescription>
<para>False - if the DST is disabled. True - if DST is enabled.</para></parameterdescription>
</parameteritem>
</parameterlist>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;This function is required to be called in user interrupt handler. &lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1416" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_rtc.c" bodystart="1388" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_rtc.h" line="574" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__rtc__interrupt__functions_1ga5c2c4659f4165a5ed5fa2b0cafd2c06c" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_RTC_Alarm1Interrupt</definition>
        <argsstring>(void)</argsstring>
        <name>Cy_RTC_Alarm1Interrupt</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>A blank weak interrupt handler function which indicates assert of the RTC alarm 1 interrupt. </para>        </briefdescription>
        <detaileddescription>
<para>Function implementation should be defined in user source code in condition that such event handler is required. If such event is not required user should not do any actions.</para><para>This function is called in the general RTC interrupt handler <ref kindref="member" refid="group__group__rtc__interrupt__functions_1ga47cd2af8969302fd8fc5a7e661eba819">Cy_RTC_Interrupt()</ref> function. </para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1083" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_rtc.c" bodystart="1080" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_rtc.h" line="575" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__rtc__interrupt__functions_1ga7aa58fabe8faf1b1b21dbe8bdcd1e919" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_RTC_Alarm2Interrupt</definition>
        <argsstring>(void)</argsstring>
        <name>Cy_RTC_Alarm2Interrupt</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>A blank weak interrupt handler function which indicates assert of the RTC alarm 2 interrupt. </para>        </briefdescription>
        <detaileddescription>
<para>Function implementation should be defined in user source code in condition that such event handler is required. If such event is not required user should not do any actions.</para><para>This function is called in the general RTC interrupt handler <ref kindref="member" refid="group__group__rtc__interrupt__functions_1ga47cd2af8969302fd8fc5a7e661eba819">Cy_RTC_Interrupt()</ref> function. <ref kindref="member" refid="group__group__rtc__interrupt__functions_1ga7aa58fabe8faf1b1b21dbe8bdcd1e919">Cy_RTC_Alarm2Interrupt()</ref> function is ignored in <ref kindref="member" refid="group__group__rtc__interrupt__functions_1ga47cd2af8969302fd8fc5a7e661eba819">Cy_RTC_Interrupt()</ref> function if DST is enabled. Refer to <ref kindref="member" refid="group__group__rtc__interrupt__functions_1ga47cd2af8969302fd8fc5a7e661eba819">Cy_RTC_Interrupt()</ref> description. </para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1106" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_rtc.c" bodystart="1103" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_rtc.h" line="576" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__rtc__interrupt__functions_1gab9511169906c3a9f1130fdcd826a15e7" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_RTC_DstInterrupt</definition>
        <argsstring>(cy_stc_rtc_dst_t const *dstTime)</argsstring>
        <name>Cy_RTC_DstInterrupt</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__rtc__dst__t">cy_stc_rtc_dst_t</ref> const *</type>
          <declname>dstTime</declname>
        </param>
        <briefdescription>
<para>This is a processing handler against the DST event. </para>        </briefdescription>
        <detaileddescription>
<para>It adjusts the current time using the DST start/stop parameters and registers the next DST event time into the ALARM2 interrupt.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>dstTime</parametername>
</parameternamelist>
<parameterdescription>
<para>The DST configuration structure, see <ref kindref="compound" refid="structcy__stc__rtc__dst__t">cy_stc_rtc_dst_t</ref>. </para></parameterdescription>
</parameteritem>
</parameterlist>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1226" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_rtc.c" bodystart="1120" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_rtc.h" line="577" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__rtc__interrupt__functions_1gadac651af6dcd8ad0affb2243af2c005c" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_RTC_CenturyInterrupt</definition>
        <argsstring>(void)</argsstring>
        <name>Cy_RTC_CenturyInterrupt</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>This is a weak function and it should be redefined in user source code in condition that such event handler is required. </para>        </briefdescription>
        <detaileddescription>
<para>By calling this function, it indicates the year reached 2100. It should add an adjustment to avoid the Y2K problem.</para><para>Function implementation should be defined in user source code in condition that such event handler is required. If such event is not required user should not do any actions. </para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1246" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_rtc.c" bodystart="1243" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_rtc.h" line="578" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__rtc__interrupt__functions_1ga6e4de226161848076faa5cf9c82e950b" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>uint32_t</type>
        <definition>uint32_t Cy_RTC_GetInterruptStatus</definition>
        <argsstring>(void)</argsstring>
        <name>Cy_RTC_GetInterruptStatus</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>Returns a status of RTC interrupt requests. </para>        </briefdescription>
        <detaileddescription>
<para><simplesect kind="return"><para>Bit mapping information, see <ref kindref="compound" refid="group__group__rtc__macros__interrupts">RTC Interrupt sources</ref>. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1262" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_rtc.c" bodystart="1259" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_rtc.h" line="579" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__rtc__interrupt__functions_1ga5512fd0e9b074a42b4150ab329689eac" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>uint32_t</type>
        <definition>uint32_t Cy_RTC_GetInterruptStatusMasked</definition>
        <argsstring>(void)</argsstring>
        <name>Cy_RTC_GetInterruptStatusMasked</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>Returns an interrupt request register masked by the interrupt mask. </para>        </briefdescription>
        <detaileddescription>
<para>Returns a result of the bitwise AND operation between the corresponding interrupt request and mask bits.</para><para><simplesect kind="return"><para>Bit mapping information, see <ref kindref="compound" refid="group__group__rtc__macros__interrupts">RTC Interrupt sources</ref>. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1280" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_rtc.c" bodystart="1277" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_rtc.h" line="580" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__rtc__interrupt__functions_1ga33b55eee29b5f8a3fffdf9908b3488d5" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>uint32_t</type>
        <definition>uint32_t Cy_RTC_GetInterruptMask</definition>
        <argsstring>(void)</argsstring>
        <name>Cy_RTC_GetInterruptMask</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>Returns an interrupt mask. </para>        </briefdescription>
        <detaileddescription>
<para><simplesect kind="return"><para>Bit mapping information, see <ref kindref="compound" refid="group__group__rtc__macros__interrupts">RTC Interrupt sources</ref>. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1296" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_rtc.c" bodystart="1293" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_rtc.h" line="581" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__rtc__interrupt__functions_1ga1851ce22703f0e81e5de8c9ee00fd8fa" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_RTC_ClearInterrupt</definition>
        <argsstring>(uint32_t interruptMask)</argsstring>
        <name>Cy_RTC_ClearInterrupt</name>
        <param>
          <type>uint32_t</type>
          <declname>interruptMask</declname>
        </param>
        <briefdescription>
<para>Clears RTC interrupts by setting each bit. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>interruptMask</parametername>
</parameternamelist>
<parameterdescription>
<para>The bit mask of interrupts to set, see <ref kindref="compound" refid="group__group__rtc__macros__interrupts">RTC Interrupt sources</ref>. </para></parameterdescription>
</parameteritem>
</parameterlist>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1335" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_rtc.c" bodystart="1328" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_rtc.h" line="582" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__rtc__interrupt__functions_1gaa4885acca86684398be45233b292f038" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_RTC_SetInterrupt</definition>
        <argsstring>(uint32_t interruptMask)</argsstring>
        <name>Cy_RTC_SetInterrupt</name>
        <param>
          <type>uint32_t</type>
          <declname>interruptMask</declname>
        </param>
        <briefdescription>
<para>Sets a software interrupt request. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>interruptMask</parametername>
</parameternamelist>
<parameterdescription>
<para>Bit mask, see <ref kindref="compound" refid="group__group__rtc__macros__interrupts">RTC Interrupt sources</ref>. </para></parameterdescription>
</parameteritem>
</parameterlist>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1314" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_rtc.c" bodystart="1309" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_rtc.h" line="583" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__rtc__interrupt__functions_1gaf2ab28f85a042dc1c68b35e0c809eade" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_RTC_SetInterruptMask</definition>
        <argsstring>(uint32_t interruptMask)</argsstring>
        <name>Cy_RTC_SetInterruptMask</name>
        <param>
          <type>uint32_t</type>
          <declname>interruptMask</declname>
        </param>
        <briefdescription>
<para>Configures which bits of the interrupt request register that triggers an interrupt event. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>interruptMask</parametername>
</parameternamelist>
<parameterdescription>
<para>The bit mask of interrupts to set, see <ref kindref="compound" refid="group__group__rtc__macros__interrupts">RTC Interrupt sources</ref>. </para></parameterdescription>
</parameteritem>
</parameterlist>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1354" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_rtc.c" bodystart="1349" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_rtc.h" line="584" />
      </memberdef>
      </sectiondef>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>