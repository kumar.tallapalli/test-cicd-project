========================================
INTERCONNECT (Internal digital routing)
========================================

.. doxygengroup:: group_hal_interconnect
   :project: hal
   :members:
   :protected-members:
   :private-members:
   :undoc-members:


   

.. toctree::
   :hidden:

   group__group__hal__results__interconnect.rst
   

