====================================
TRNG (True Random Number Generator)
====================================

.. doxygengroup:: group_hal_trng
   :project: hal
   :members:
   :protected-members:
   :private-members:
   :undoc-members:


   

.. toctree::
   :hidden:

   group__group__hal__results__trng.rst
   

