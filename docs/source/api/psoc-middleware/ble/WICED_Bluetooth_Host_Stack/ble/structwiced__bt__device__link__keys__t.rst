=============================================
wiced_bt_device_link_keys_t struct
=============================================

.. doxygenstruct:: wiced_bt_device_link_keys_t
   :project: WICED_Bluetooth_Host_Stack-ble
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
