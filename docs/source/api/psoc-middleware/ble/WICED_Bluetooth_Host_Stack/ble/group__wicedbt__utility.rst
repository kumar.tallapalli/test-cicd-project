=========
Utilities
=========


.. doxygengroup:: wicedbt_utility
   :project: WICED_Bluetooth_Host_Stack-ble
   :members:
   :protected-members:
   :private-members:
   :undoc-members: