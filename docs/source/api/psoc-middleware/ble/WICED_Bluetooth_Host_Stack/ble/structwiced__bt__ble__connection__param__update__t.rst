=============================================
wiced_bt_ble_connection_param_update_t struct
=============================================

.. doxygenstruct:: wiced_bt_ble_connection_param_update_t
   :project: WICED_Bluetooth_Host_Stack-ble
   :members:
   :protected-members:
   :private-members:
   :undoc-members: