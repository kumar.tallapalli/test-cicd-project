=============================================
wiced_bt_lq_stats_result_t struct
=============================================

.. doxygenstruct:: wiced_bt_lq_stats_result_t
   :project: WICED_Bluetooth_Host_Stack-ble
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
