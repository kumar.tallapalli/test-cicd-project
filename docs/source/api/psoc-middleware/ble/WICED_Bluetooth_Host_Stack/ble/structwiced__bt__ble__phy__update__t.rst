=============================================
wiced_bt_ble_phy_update_t struct
=============================================

.. doxygenstruct:: wiced_bt_ble_phy_update_t
   :project: WICED_Bluetooth_Host_Stack-ble
   :members:
   :protected-members:
   :private-members:
   :undoc-members: