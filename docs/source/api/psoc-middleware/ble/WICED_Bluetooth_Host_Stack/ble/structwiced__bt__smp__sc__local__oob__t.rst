==========================================
wiced_bt_smp_sc_local_oob_t Struct
==========================================

.. doxygenstruct:: wiced_bt_smp_sc_local_oob_t
   :project: WICED_Bluetooth_Host_Stack-ble
   :members:
   :protected-members:
   :private-members:
   :undoc-members: