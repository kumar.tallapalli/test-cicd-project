==============================================================
wiced_bt_dev_vendor_specific_command_complete_params_t Struct
==============================================================

.. doxygenstruct:: wiced_bt_dev_vendor_specific_command_complete_params_t
   :project: WICED_Bluetooth_Host_Stack-ble
   :members:
   :protected-members:
   :private-members:
   :undoc-members: