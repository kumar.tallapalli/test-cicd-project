=============================================
wiced_bt_dev_enabled_t struct
=============================================

.. doxygenstruct:: wiced_bt_dev_enabled_t
   :project: WICED_Bluetooth_Host_Stack-ble
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
