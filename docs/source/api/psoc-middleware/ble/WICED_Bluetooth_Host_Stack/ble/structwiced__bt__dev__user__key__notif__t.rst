==========================================
wiced_bt_dev_user_key_notif_t Struct
==========================================

.. doxygenstruct:: wiced_bt_dev_user_key_notif_t
   :project: WICED_Bluetooth_Host_Stack-ble
   :members:
   :protected-members:
   :private-members:
   :undoc-members: