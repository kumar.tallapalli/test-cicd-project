=======================================
wiced_bt_l2cap_fixed_chnl_reg_t Struct
=======================================

.. doxygenstruct:: wiced_bt_l2cap_fixed_chnl_reg_t
   :project: WICED_Bluetooth_Host_Stack-ble
   :members:
   :protected-members:
   :private-members:
   :undoc-members: