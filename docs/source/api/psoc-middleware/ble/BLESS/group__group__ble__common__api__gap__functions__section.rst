==============
GAP Functions
==============

.. doxygengroup:: group_ble_common_api_gap_functions_section
   :project: bless
   
.. toctree::
   :hidden:

   group__group__ble__common__api__gap__functions.rst
   group__group__ble__common__api__gap__central__functions.rst
   group__group__ble__common__api__gap__peripheral__functions.rst