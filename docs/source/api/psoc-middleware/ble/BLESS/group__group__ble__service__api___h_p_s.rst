=========================
HTTP Proxy Service (HPS)
=========================

.. doxygengroup:: group_ble_service_api_HPS
   :project: bless
   
.. toctree::
   :hidden:

   group__group__ble__service__api___h_p_s__server__client.rst
   group__group__ble__service__api___h_p_s__server.rst
   group__group__ble__service__api___h_p_s__client.rst
   group__group__ble__service__api___h_p_s__definitions.rst