===============================
Body Composition Service (BCS)
===============================

.. doxygengroup:: group_ble_service_api_BCS
   :project: bless
   
.. toctree::
   :hidden:

   group__group__ble__service__api___b_c_s__server__client.rst
   group__group__ble__service__api___b_c_s__server.rst
   group__group__ble__service__api___b_c_s__client.rst
   group__group__ble__service__api___b_c_s__definitions.rst