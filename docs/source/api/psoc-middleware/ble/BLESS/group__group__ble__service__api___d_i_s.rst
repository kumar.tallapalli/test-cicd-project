=================================
Device Information Service (DIS)
=================================

.. doxygengroup:: group_ble_service_api_DIS
   :project: bless
   
.. toctree::
   :hidden:

   group__group__ble__service__api___d_i_s__server__client.rst
   group__group__ble__service__api___d_i_s__server.rst
   group__group__ble__service__api___d_i_s__client.rst
   group__group__ble__service__api___d_i_s__definitions.rst