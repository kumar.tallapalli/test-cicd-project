<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__ble__service__api___d_i_s__client" kind="group">
    <compoundname>group_ble_service_api_DIS_client</compoundname>
    <title>DIS Client Functions</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__ble__service__api___d_i_s__client_1ga0c01855447ff6fddb704ee34ecc7588d" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref></type>
        <definition>cy_en_ble_api_result_t Cy_BLE_DISC_GetCharacteristicValue</definition>
        <argsstring>(cy_stc_ble_conn_handle_t connHandle, cy_en_ble_dis_char_index_t charIndex)</argsstring>
        <name>Cy_BLE_DISC_GetCharacteristicValue</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__ble__conn__handle__t">cy_stc_ble_conn_handle_t</ref></type>
          <declname>connHandle</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ble__service__api___d_i_s__definitions_1gaa053c77c47943c1452b220d9cf1b2db9">cy_en_ble_dis_char_index_t</ref></type>
          <declname>charIndex</declname>
        </param>
        <briefdescription>
<para>This function is used to read the characteristic Value from a server which is identified by charIndex. </para>        </briefdescription>
        <detaileddescription>
<para>The Read Response returns the characteristic value in the Attribute Value parameter. The Read Response only contains the characteristic value that is less than or equal to (MTU - 1) octets in length. If the characteristic value is greater than (MTU - 1) octets in length, a Read Long Characteristic Value procedure may be used if the rest of the characteristic value is required.</para><para>This function call can result in generation of the following events based on the response from the server device.</para><para><itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51a40fd23b7e3d776e7f6e96b04c8f6d26d">CY_BLE_EVT_DISC_READ_CHAR_RESPONSE</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a89dd75cdee5a4cf9131a12ad37363fd3">CY_BLE_EVT_GATTC_ERROR_RSP</ref></para></listitem></itemizedlist>
</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>connHandle</parametername>
</parameternamelist>
<parameterdescription>
<para>The connection handle.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>charIndex</parametername>
</parameternamelist>
<parameterdescription>
<para>The index of a service characteristic of type <ref kindref="member" refid="group__group__ble__service__api___d_i_s__definitions_1gaa053c77c47943c1452b220d9cf1b2db9">cy_en_ble_dis_char_index_t</ref>.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>A return value of type <ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref>.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Error Codes &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The Read Request was sent successfully. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_PARAMETER &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Validation of the input parameter failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_OPERATION &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Operation is invalid for this characteristic. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_STATE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Connection with the server is not established. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_MEMORY_ALLOCATION_FAILED &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Memory allocation failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_GATT_DB_INVALID_ATTR_HANDLE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The peer device doesn't have the particular characteristic. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim><simplesect kind="par"><title>Events</title><para>If execution is successful (return value = <ref kindref="member" refid="group__group__ble__common__api__definitions_1gga63f6be7f53f5ad7d82c394e4cdfa619ea26e476ee364252903ce212deeb6ec064">CY_BLE_SUCCESS</ref>) these events can appear: <linebreak />
If the DIS service-specific callback is registered (with <ref kindref="member" refid="group__group__ble__service__api___d_i_s__server__client_1ga732171c25637b7c95c814b48c1c6d115">Cy_BLE_DIS_RegisterAttrCallback</ref>):<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51a40fd23b7e3d776e7f6e96b04c8f6d26d">CY_BLE_EVT_DISC_READ_CHAR_RESPONSE</ref> - if the requested attribute is successfully read on the peer device, the details (charIndex, value, etc.) are provided with an event parameter structure of type <ref kindref="compound" refid="structcy__stc__ble__dis__char__value__t">cy_stc_ble_dis_char_value_t</ref>.</para></listitem></itemizedlist>
Otherwise (if a DIS service-specific callback is not registered):<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a9f66de88a47e0eb5cd2026421ffcea24">CY_BLE_EVT_GATTC_READ_RSP</ref> - if the requested attribute is successfully read on the peer device, the details (handle, value, etc.) are provided with the event parameters structure <ref kindref="compound" refid="structcy__stc__ble__gattc__read__rsp__param__t">cy_stc_ble_gattc_read_rsp_param_t</ref>.</para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a89dd75cdee5a4cf9131a12ad37363fd3">CY_BLE_EVT_GATTC_ERROR_RSP</ref> - If an error occurred with the requested attribute on the peer device, the details are provided with the event parameters structure <ref kindref="compound" refid="structcy__stc__ble__gatt__err__param__t">cy_stc_ble_gatt_err_param_t</ref>. </para></listitem></itemizedlist>
</para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="482" bodyfile="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_dis.c" bodystart="444" column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_dis.h" line="125" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>API unique to DIS designs configured as a GATT Client role. </para>    </briefdescription>
    <detaileddescription>
<para>A letter 'c' is appended to the API name: Cy_BLE_DISC_ </para>    </detaileddescription>
  </compounddef>
</doxygen>