=========================
BLE Service-Specific API
=========================

.. doxygengroup:: group_ble_service_api
   :project: bless
   
   
   
.. toctree::
   :hidden:

   group__group__ble__service__api__events.rst
   group__group__ble__service__api___a_n_c_s.rst
   group__group__ble__service__api___a_n_s.rst
   group__group__ble__service__api___a_i_o_s.rst
   group__group__ble__service__api___b_a_s.rst
   group__group__ble__service__api___b_c_s.rst
   group__group__ble__service__api___b_l_s.rst
   group__group__ble__service__api___b_m_s.rst
   group__group__ble__service__api___c_g_m_s.rst
   group__group__ble__service__api___c_p_s.rst
   group__group__ble__service__api___c_s_c_s.rst
   group__group__ble__service__api___c_t_s.rst
   group__group__ble__service__api___d_i_s.rst
   group__group__ble__service__api___e_s_s.rst
   group__group__ble__service__api___g_l_s.rst
   group__group__ble__service__api___h_i_d_s.rst
   group__group__ble__service__api___h_r_s.rst
   group__group__ble__service__api___h_p_s.rst
   group__group__ble__service__api___h_t_s.rst
   group__group__ble__service__api___i_a_s.rst
   group__group__ble__service__api___i_p_s.rst
   group__group__ble__service__api___l_l_s.rst
   group__group__ble__service__api___l_n_s.rst
   group__group__ble__service__api___n_d_c_s.rst
   group__group__ble__service__api___p_a_s_s.rst
   group__group__ble__service__api___p_l_x_s.rst
   group__group__ble__service__api___r_s_c_s.rst
   group__group__ble__service__api___r_t_u_s.rst
   group__group__ble__service__api___s_c_p_s.rst
   group__group__ble__service__api___t_p_s.rst
   group__group__ble__service__api___u_d_s.rst
   group__group__ble__service__api___w_p_t_s.rst
   group__group__ble__service__api___w_s_s.rst
   group__group__ble__service__api__custom.rst