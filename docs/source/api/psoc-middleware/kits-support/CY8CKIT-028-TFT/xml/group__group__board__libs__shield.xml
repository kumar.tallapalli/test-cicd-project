<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__board__libs__shield" kind="group">
    <compoundname>group_board_libs_shield</compoundname>
    <title>Shield</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__board__libs__shield_1gad949b7534e1066ef0b22347fe533c9a8" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t cy8ckit_028_tft_init</definition>
        <argsstring>(cyhal_i2c_t *i2c_inst, cyhal_adc_t *adc_inst, cyhal_pdm_pcm_cfg_t *pdm_pcm_cfg, cyhal_clock_t *audio_clock_inst)</argsstring>
        <name>cy8ckit_028_tft_init</name>
        <param>
          <type>cyhal_i2c_t *</type>
          <declname>i2c_inst</declname>
        </param>
        <param>
          <type>cyhal_adc_t *</type>
          <declname>adc_inst</declname>
        </param>
        <param>
          <type>cyhal_pdm_pcm_cfg_t *</type>
          <declname>pdm_pcm_cfg</declname>
        </param>
        <param>
          <type>cyhal_clock_t *</type>
          <declname>audio_clock_inst</declname>
        </param>
        <briefdescription>
<para>Initialize the shield board and all peripherals contained on it. </para>        </briefdescription>
        <detaileddescription>
<para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;This will reserve all CY8CKIT_028_TFT_PIN_DISPLAY pins for the display. &lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">i2c_inst</parametername>
</parameternamelist>
<parameterdescription>
<para>An optional I2C instance to use for communicating with the motion sensor on the shield. If NULL, a new instance will be allocated using the <ref kindref="member" refid="group__group__board__libs__pins_1ga8833a8d6a49addf6e4a3511e04a6a754">CY8CKIT_028_TFT_PIN_IMU_I2C_SCL</ref> &amp; <ref kindref="member" refid="group__group__board__libs__pins_1gac8c2026d7ae0aa1c3c7a04db2b542d6b">CY8CKIT_028_TFT_PIN_IMU_I2C_SDA</ref> pins. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">adc_inst</parametername>
</parameternamelist>
<parameterdescription>
<para>An optional adc instance to be used with the light sensor. If NULL, a new instance will be allocated. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">pdm_pcm_cfg</parametername>
</parameternamelist>
<parameterdescription>
<para>The configuration for the PDM object used with the microphone. . If NULL, the PDM object will not be initialized. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">audio_clock_inst</parametername>
</parameternamelist>
<parameterdescription>
<para>The audio clock used with the microphone. . If NULL, the PDM object will not be initialized. </para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>CY_RSLT_SUCCESS if properly initialized, else an error indicating what went wrong. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="120" bodyfile="output/libs/COMPONENT_CY8CKIT-028-TFT/CY8CKIT-028-TFT/cy8ckit_028_tft.c" bodystart="58" column="1" file="output/libs/COMPONENT_CY8CKIT-028-TFT/CY8CKIT-028-TFT/cy8ckit_028_tft.h" line="98" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__board__libs__shield_1gab2e27e60c331cca1daf9bedf6c3a4fd3" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>mtb_light_sensor_t *</type>
        <definition>mtb_light_sensor_t* cy8ckit_028_tft_get_light_sensor</definition>
        <argsstring>(void)</argsstring>
        <name>cy8ckit_028_tft_get_light_sensor</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>Gives the user access to the light sensor object. </para>        </briefdescription>
        <detaileddescription>
<para><simplesect kind="return"><para>A reference to the light sensor object on this shield. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="129" bodyfile="output/libs/COMPONENT_CY8CKIT-028-TFT/CY8CKIT-028-TFT/cy8ckit_028_tft.c" bodystart="126" column="1" file="output/libs/COMPONENT_CY8CKIT-028-TFT/CY8CKIT-028-TFT/cy8ckit_028_tft.h" line="108" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__board__libs__shield_1ga2f404f95d001a872c563379490416041" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>mtb_bmi160_t *</type>
        <definition>mtb_bmi160_t* cy8ckit_028_tft_get_motion_sensor</definition>
        <argsstring>(void)</argsstring>
        <name>cy8ckit_028_tft_get_motion_sensor</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>Gives the user access to the motion sensor object. </para>        </briefdescription>
        <detaileddescription>
<para><simplesect kind="return"><para>A reference to the motion sensor object on this shield. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="138" bodyfile="output/libs/COMPONENT_CY8CKIT-028-TFT/CY8CKIT-028-TFT/cy8ckit_028_tft.c" bodystart="135" column="1" file="output/libs/COMPONENT_CY8CKIT-028-TFT/CY8CKIT-028-TFT/cy8ckit_028_tft.h" line="114" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__board__libs__shield_1ga8df0d7e3d3eebdb436333ed69ba9204e" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cyhal_pdm_pcm_t *</type>
        <definition>cyhal_pdm_pcm_t* cy8ckit_028_tft_get_pdm</definition>
        <argsstring>(void)</argsstring>
        <name>cy8ckit_028_tft_get_pdm</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>Gives the user access to the PDM object used with the microphone. </para>        </briefdescription>
        <detaileddescription>
<para><simplesect kind="return"><para>A reference to the PDM microphone object on this shield. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="147" bodyfile="output/libs/COMPONENT_CY8CKIT-028-TFT/CY8CKIT-028-TFT/cy8ckit_028_tft.c" bodystart="144" column="1" file="output/libs/COMPONENT_CY8CKIT-028-TFT/CY8CKIT-028-TFT/cy8ckit_028_tft.h" line="120" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__board__libs__shield_1ga03dbc47617086493d3c561a690780266" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void cy8ckit_028_tft_free</definition>
        <argsstring>(void)</argsstring>
        <name>cy8ckit_028_tft_free</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>Frees up any resources allocated as part of <ref kindref="member" refid="group__group__board__libs__shield_1gad949b7534e1066ef0b22347fe533c9a8">cy8ckit_028_tft_init()</ref>. </para>        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="175" bodyfile="output/libs/COMPONENT_CY8CKIT-028-TFT/CY8CKIT-028-TFT/cy8ckit_028_tft.c" bodystart="153" column="1" file="output/libs/COMPONENT_CY8CKIT-028-TFT/CY8CKIT-028-TFT/cy8ckit_028_tft.h" line="125" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>Basic set of APIs for interacting with the CY8CKIT-028-TFT shield board. </para>    </briefdescription>
    <detaileddescription>
<para>This provides pin definitions and initialization code for the shield. Initialization of the shield configures the internal peripherals to allow them to be used. </para>    </detaileddescription>
  </compounddef>
</doxygen>