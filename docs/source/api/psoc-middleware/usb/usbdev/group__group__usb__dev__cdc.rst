==========
CDC Class
==========

.. doxygengroup:: group_usb_dev_cdc
   :project: usbdev


.. toctree::

   group__group__usb__dev__cdc__macros.rst
   group__group__usb__dev__cdc__functions.rst
   group__group__usb__dev__cdc__data__structures.rst
   group__group__usb__dev__cdc__enums.rst