<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="mtb__thermistor__ntc__gpio_8h" kind="file" language="C++">
    <compoundname>mtb_thermistor_ntc_gpio.h</compoundname>
    <includes local="yes">cy_result.h</includes>
    <includes local="yes">cyhal_gpio.h</includes>
    <includes local="yes">cyhal_adc.h</includes>
    <includedby local="yes" refid="mtb__thermistor__ntc__gpio_8c">mtb_thermistor_ntc_gpio.c</includedby>
    <incdepgraph>
      <node id="4">
        <label>cy_result.h</label>
      </node>
      <node id="6">
        <label>cyhal_adc.h</label>
      </node>
      <node id="3">
        <label>mtb_thermistor_ntc_gpio.h</label>
        <link refid="mtb__thermistor__ntc__gpio_8h" />
        <childnode refid="4" relation="include">
        </childnode>
        <childnode refid="5" relation="include">
        </childnode>
        <childnode refid="6" relation="include">
        </childnode>
      </node>
      <node id="5">
        <label>cyhal_gpio.h</label>
      </node>
    </incdepgraph>
    <innerclass prot="public" refid="structmtb__thermistor__ntc__gpio__cfg__t">mtb_thermistor_ntc_gpio_cfg_t</innerclass>
    <innerclass prot="public" refid="structmtb__thermistor__ntc__gpio__t">mtb_thermistor_ntc_gpio_t</innerclass>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__board__libs_1gad8550ff1adedfd91648b41f29bc8ba6c" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t mtb_thermistor_ntc_gpio_init</definition>
        <argsstring>(mtb_thermistor_ntc_gpio_t *obj, cyhal_adc_t *adc, cyhal_gpio_t gnd, cyhal_gpio_t vdd, cyhal_gpio_t out, mtb_thermistor_ntc_gpio_cfg_t *cfg)</argsstring>
        <name>mtb_thermistor_ntc_gpio_init</name>
        <param>
          <type><ref kindref="compound" refid="structmtb__thermistor__ntc__gpio__t">mtb_thermistor_ntc_gpio_t</ref> *</type>
          <declname>obj</declname>
        </param>
        <param>
          <type>cyhal_adc_t *</type>
          <declname>adc</declname>
        </param>
        <param>
          <type>cyhal_gpio_t</type>
          <declname>gnd</declname>
        </param>
        <param>
          <type>cyhal_gpio_t</type>
          <declname>vdd</declname>
        </param>
        <param>
          <type>cyhal_gpio_t</type>
          <declname>out</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structmtb__thermistor__ntc__gpio__cfg__t">mtb_thermistor_ntc_gpio_cfg_t</ref> *</type>
          <declname>cfg</declname>
        </param>
        <briefdescription>
<para>Initialize the ADC Channel and Pins to communicate with the thermistor. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="inout">obj</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to a thermistor object containing the set of pins and adc channel that are associated with the thermistor. Note: The caller must allocate the memory for this object but the init function will initialize its contents. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="out">obj</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to a Thermistor object. The caller must allocate the memory for this object but the init function will initialize its contents. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">adc</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to an already initialized adc object </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">gnd</parametername>
</parameternamelist>
<parameterdescription>
<para>Ground reference pin </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">vdd</parametername>
</parameternamelist>
<parameterdescription>
<para>VDD reference pin </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">out</parametername>
</parameternamelist>
<parameterdescription>
<para>Voltage output pin </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">cfg</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to the cfg object containing the thermistor constants </para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>CY_RSLT_SUCCESS if properly initialized, else an error indicating what went wrong. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="64" bodyfile="output/libs/COMPONENT_THERMISTOR/thermistor/mtb_thermistor_ntc_gpio.c" bodystart="37" column="1" file="output/libs/COMPONENT_THERMISTOR/thermistor/mtb_thermistor_ntc_gpio.h" line="79" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__board__libs_1ga03a63ba0021b2039c22414336629aa91" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>float</type>
        <definition>float mtb_thermistor_ntc_gpio_get_temp</definition>
        <argsstring>(mtb_thermistor_ntc_gpio_t *obj)</argsstring>
        <name>mtb_thermistor_ntc_gpio_get_temp</name>
        <param>
          <type><ref kindref="compound" refid="structmtb__thermistor__ntc__gpio__t">mtb_thermistor_ntc_gpio_t</ref> *</type>
          <declname>obj</declname>
        </param>
        <briefdescription>
<para>Gets the temperature reading, in degrees C, from the thermistor. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">obj</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to a thermistor object containing the set of pins and adc channel that are associated with the thermistor. </para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The temperature reading, in degrees C, from the hardware. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="89" bodyfile="output/libs/COMPONENT_THERMISTOR/thermistor/mtb_thermistor_ntc_gpio.c" bodystart="66" column="1" file="output/libs/COMPONENT_THERMISTOR/thermistor/mtb_thermistor_ntc_gpio.h" line="87" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__board__libs_1ga9d024bc3ee1a2b9a0c15f63c1bbb5711" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void mtb_thermistor_ntc_gpio_free</definition>
        <argsstring>(mtb_thermistor_ntc_gpio_t *obj)</argsstring>
        <name>mtb_thermistor_ntc_gpio_free</name>
        <param>
          <type><ref kindref="compound" refid="structmtb__thermistor__ntc__gpio__t">mtb_thermistor_ntc_gpio_t</ref> *</type>
          <declname>obj</declname>
        </param>
        <briefdescription>
<para>Frees up the ADC channel and Pins allocated for the thermistor. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">obj</parametername>
</parameternamelist>
<parameterdescription>
<para>The set of pins and adc channel that are associated with the thermistor </para></parameterdescription>
</parameteritem>
</parameterlist>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="102" bodyfile="output/libs/COMPONENT_THERMISTOR/thermistor/mtb_thermistor_ntc_gpio.c" bodystart="91" column="1" file="output/libs/COMPONENT_THERMISTOR/thermistor/mtb_thermistor_ntc_gpio.h" line="93" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>Description: This file is the public interface of mtb_thermistor_ntc_gpio.c source file. </para>    </briefdescription>
    <detaileddescription>
<para><simplesect kind="copyright"><para>Copyright 2018-2020 Cypress Semiconductor Corporation SPDX-License-Identifier: Apache-2.0</para></simplesect>
Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at <verbatim>http://www.apache.org/licenses/LICENSE-2.0
</verbatim></para><para>Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License. </para>    </detaileddescription>
    <programlisting>
<codeline lineno="1"><highlight class="comment">/**************************************************************************/</highlight></codeline>
<codeline lineno="25"><highlight class="preprocessor">#pragma<sp />once</highlight><highlight class="normal" /></codeline>
<codeline lineno="26"><highlight class="normal" /></codeline>
<codeline lineno="36"><highlight class="preprocessor">#include<sp />"cy_result.h"</highlight><highlight class="normal" /></codeline>
<codeline lineno="37"><highlight class="normal" /><highlight class="preprocessor">#include<sp />"cyhal_gpio.h"</highlight><highlight class="normal" /></codeline>
<codeline lineno="38"><highlight class="normal" /><highlight class="preprocessor">#include<sp />"cyhal_adc.h"</highlight><highlight class="normal" /></codeline>
<codeline lineno="39"><highlight class="normal" /></codeline>
<codeline lineno="40"><highlight class="normal" /><highlight class="preprocessor">#if<sp />defined(__cplusplus)</highlight><highlight class="normal" /></codeline>
<codeline lineno="41"><highlight class="normal" /><highlight class="keyword">extern</highlight><highlight class="normal"><sp /></highlight><highlight class="stringliteral">"C"</highlight><highlight class="normal" /></codeline>
<codeline lineno="42"><highlight class="normal">{</highlight></codeline>
<codeline lineno="43"><highlight class="normal" /><highlight class="preprocessor">#endif</highlight><highlight class="normal" /></codeline>
<codeline lineno="44"><highlight class="normal" /></codeline>
<codeline lineno="46" refid="structmtb__thermistor__ntc__gpio__cfg__t" refkind="compound"><highlight class="keyword">typedef</highlight><highlight class="normal"><sp /></highlight><highlight class="keyword">struct</highlight><highlight class="normal" /></codeline>
<codeline lineno="47"><highlight class="normal">{</highlight></codeline>
<codeline lineno="48"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordtype">float</highlight><highlight class="normal"><sp />r_ref;<sp /><sp /><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline lineno="49"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordtype">float</highlight><highlight class="normal"><sp />b_const;<sp /><sp /><sp /><sp /></highlight></codeline>
<codeline lineno="50"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordtype">float</highlight><highlight class="normal"><sp />r_infinity;<sp /></highlight></codeline>
<codeline lineno="51"><highlight class="normal">}<sp /><ref kindref="compound" refid="structmtb__thermistor__ntc__gpio__cfg__t">mtb_thermistor_ntc_gpio_cfg_t</ref>;</highlight></codeline>
<codeline lineno="52"><highlight class="normal" /></codeline>
<codeline lineno="55" refid="structmtb__thermistor__ntc__gpio__t" refkind="compound"><highlight class="keyword">typedef</highlight><highlight class="normal"><sp /></highlight><highlight class="keyword">struct</highlight><highlight class="normal" /></codeline>
<codeline lineno="56"><highlight class="normal">{</highlight></codeline>
<codeline lineno="57"><highlight class="normal"><sp /><sp /><sp /><sp />cyhal_adc_channel_t<sp />channel;<sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline lineno="58"><highlight class="normal"><sp /><sp /><sp /><sp />cyhal_gpio_t<sp />gnd;<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline lineno="59"><highlight class="normal"><sp /><sp /><sp /><sp />cyhal_gpio_t<sp />vdd;<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline lineno="60"><highlight class="normal"><sp /><sp /><sp /><sp />cyhal_gpio_t<sp />out;<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline lineno="61"><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="compound" refid="structmtb__thermistor__ntc__gpio__cfg__t">mtb_thermistor_ntc_gpio_cfg_t</ref><sp />*cfg;<sp /></highlight></codeline>
<codeline lineno="62"><highlight class="normal">}<sp /><ref kindref="compound" refid="structmtb__thermistor__ntc__gpio__t">mtb_thermistor_ntc_gpio_t</ref>;</highlight></codeline>
<codeline lineno="63"><highlight class="normal" /></codeline>
<codeline lineno="79"><highlight class="normal">cy_rslt_t<sp /><ref kindref="member" refid="group__group__board__libs_1gad8550ff1adedfd91648b41f29bc8ba6c">mtb_thermistor_ntc_gpio_init</ref>(<ref kindref="compound" refid="structmtb__thermistor__ntc__gpio__t">mtb_thermistor_ntc_gpio_t</ref><sp />*obj,<sp />cyhal_adc_t<sp />*adc,<sp />cyhal_gpio_t<sp />gnd,<sp />cyhal_gpio_t<sp />vdd,<sp />cyhal_gpio_t<sp />out,<sp /><ref kindref="compound" refid="structmtb__thermistor__ntc__gpio__cfg__t">mtb_thermistor_ntc_gpio_cfg_t</ref><sp />*cfg);</highlight></codeline>
<codeline lineno="80"><highlight class="normal" /></codeline>
<codeline lineno="87"><highlight class="keywordtype">float</highlight><highlight class="normal"><sp /><ref kindref="member" refid="group__group__board__libs_1ga03a63ba0021b2039c22414336629aa91">mtb_thermistor_ntc_gpio_get_temp</ref>(<ref kindref="compound" refid="structmtb__thermistor__ntc__gpio__t">mtb_thermistor_ntc_gpio_t</ref><sp />*obj);</highlight></codeline>
<codeline lineno="88"><highlight class="normal" /></codeline>
<codeline lineno="93"><highlight class="keywordtype">void</highlight><highlight class="normal"><sp /><ref kindref="member" refid="group__group__board__libs_1ga9d024bc3ee1a2b9a0c15f63c1bbb5711">mtb_thermistor_ntc_gpio_free</ref>(<ref kindref="compound" refid="structmtb__thermistor__ntc__gpio__t">mtb_thermistor_ntc_gpio_t</ref><sp />*obj);</highlight></codeline>
<codeline lineno="94"><highlight class="normal" /></codeline>
<codeline lineno="95"><highlight class="normal" /><highlight class="preprocessor">#if<sp />defined(__cplusplus)</highlight><highlight class="normal" /></codeline>
<codeline lineno="96"><highlight class="normal">}</highlight></codeline>
<codeline lineno="97"><highlight class="normal" /><highlight class="preprocessor">#endif</highlight><highlight class="normal" /></codeline>
<codeline lineno="98"><highlight class="normal" /></codeline>
    </programlisting>
    <location file="output/libs/COMPONENT_THERMISTOR/thermistor/mtb_thermistor_ntc_gpio.h" />
  </compounddef>
</doxygen>