<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__em__eeprom__functions" kind="group">
    <compoundname>group_em_eeprom_functions</compoundname>
    <title>Functions</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__em__eeprom__functions_1gab9f608d788f1a854b10c652c3bb00fdd" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__em__eeprom__enums_1ga00cb0e6ed68329cd7aba18e3a93e13d2">cy_en_em_eeprom_status_t</ref></type>
        <definition>cy_en_em_eeprom_status_t Cy_Em_EEPROM_Init</definition>
        <argsstring>(const cy_stc_eeprom_config_t *config, cy_stc_eeprom_context_t *context)</argsstring>
        <name>Cy_Em_EEPROM_Init</name>
        <param>
          <type>const <ref kindref="compound" refid="structcy__stc__eeprom__config__t">cy_stc_eeprom_config_t</ref> *</type>
          <declname>config</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__eeprom__context__t">cy_stc_eeprom_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Initializes the Emulated EEPROM library by filling the context structure. </para>        </briefdescription>
        <detaileddescription>
<para>This function is called by the application program prior to calling any other function of the Em_EEPROM middleware.</para><para>Do not modify the context structure after it is filled with this function. Modification of the context structure may cause unexpected behavior of the Cy_Em_EEPROM functions that rely on this context structure.</para><para>This function does the following:<orderedlist>
<listitem><para>Checks the provided configuration for correctness.</para></listitem><listitem><para>Copies the provided configuration into the context structure.</para></listitem></orderedlist>
</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>config</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the configuration structure. See <ref kindref="compound" refid="structcy__stc__eeprom__config__t">cy_stc_eeprom_config_t</ref>.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the Em_EEPROM context structure to be filled by the function. See <ref kindref="compound" refid="structcy__stc__eeprom__context__t">cy_stc_eeprom_context_t</ref>.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Returns the status of the operation <ref kindref="member" refid="group__group__em__eeprom__enums_1ga00cb0e6ed68329cd7aba18e3a93e13d2">cy_en_em_eeprom_status_t</ref> :<itemizedlist>
<listitem><para>CY_EM_EEPROM_SUCCESS - The operation is successfully completed.</para></listitem><listitem><para>CY_EM_EEPROM_BAD_PARAM - The input parameter is invalid.</para></listitem><listitem><para>CY_EM_EEPROM_BAD_DATA - The configuration is incorrect. </para></listitem></itemizedlist>
</para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="153" bodyfile="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-emeeprom/cy_em_eeprom.c" bodystart="119" column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-emeeprom/cy_em_eeprom.h" line="954" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__em__eeprom__functions_1ga9e14084e3054f6f147004d2914f9216d" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__em__eeprom__enums_1ga00cb0e6ed68329cd7aba18e3a93e13d2">cy_en_em_eeprom_status_t</ref></type>
        <definition>cy_en_em_eeprom_status_t Cy_Em_EEPROM_Read</definition>
        <argsstring>(uint32_t addr, void *eepromData, uint32_t size, cy_stc_eeprom_context_t *context)</argsstring>
        <name>Cy_Em_EEPROM_Read</name>
        <param>
          <type>uint32_t</type>
          <declname>addr</declname>
        </param>
        <param>
          <type>void *</type>
          <declname>eepromData</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>size</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__eeprom__context__t">cy_stc_eeprom_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Reads data from a specified location. </para>        </briefdescription>
        <detaileddescription>
<para>This function takes a logical Em_EEPROM address, converts it to an actual physical address where data is stored and returns the data to the user.</para><para>This function uses a buffer of the flash row size to perform the read operation. For the size of the row, refer to the specific PSoC device datasheet.</para><para>There are restrictions on using the read-while-write (RWW) feature for EEPROM emulation. There are also multiple constraints for blocking and nonblocking flash operations, relating to interrupts, power modes, IPC usage, etc. Refer to the "Flash (Flash System Routine)" section of the PSoC 6 Peripheral Driver Library (psoc6pdl) API Reference Manual.<linebreak />
</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>addr</parametername>
</parameternamelist>
<parameterdescription>
<para>The logical start address in the Em_EEPROM storage to start reading data from.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>eepromData</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to a user array to write data to.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>size</parametername>
</parameternamelist>
<parameterdescription>
<para>The amount of data to read in bytes.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the Em_EEPROM context structure <ref kindref="compound" refid="structcy__stc__eeprom__context__t">cy_stc_eeprom_context_t</ref>.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Returns the status of the operation <ref kindref="member" refid="group__group__em__eeprom__enums_1ga00cb0e6ed68329cd7aba18e3a93e13d2">cy_en_em_eeprom_status_t</ref> :<itemizedlist>
<listitem><para>CY_EM_EEPROM_SUCCESS - The operation is successfully completed.</para></listitem><listitem><para>CY_EM_EEPROM_BAD_CHECKSUM - One of the row the data read from contains bad checksum.</para></listitem><listitem><para>CY_EM_EEPROM_REDUNDANT_COPY_USED - Some data were read from redundant copy.</para></listitem><listitem><para>CY_EM_EEPROM_BAD_PARAM - The function input parameter is invalid. </para></listitem></itemizedlist>
</para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="216" bodyfile="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-emeeprom/cy_em_eeprom.c" bodystart="194" column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-emeeprom/cy_em_eeprom.h" line="958" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__em__eeprom__functions_1gaa792ff05d2b6ee71b44f1faeb2b68886" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__em__eeprom__enums_1ga00cb0e6ed68329cd7aba18e3a93e13d2">cy_en_em_eeprom_status_t</ref></type>
        <definition>cy_en_em_eeprom_status_t Cy_Em_EEPROM_Write</definition>
        <argsstring>(uint32_t addr, const void *eepromData, uint32_t size, cy_stc_eeprom_context_t *context)</argsstring>
        <name>Cy_Em_EEPROM_Write</name>
        <param>
          <type>uint32_t</type>
          <declname>addr</declname>
        </param>
        <param>
          <type>const void *</type>
          <declname>eepromData</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>size</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__eeprom__context__t">cy_stc_eeprom_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Writes data to a specified location. </para>        </briefdescription>
        <detaileddescription>
<para>This function takes the logical Em_EEPROM address and converts it to an actual physical address and writes data there. If wear leveling is enabled, the writing process uses the wear leveling techniques.</para><para>This is a blocking function and it does not return until the write operation is completed. The user's application program cannot enter Hibernate mode until the write is completed. The write operation is allowed in CPU Sleep and System Deep Sleep modes. Do not reset your device during the flash operation, including the XRES pin, a software reset, and watchdog reset sources. Also, configure low-voltage detect circuits to generate an interrupt instead of a reset. Otherwise, portions of flash may undergo unexpected changes.</para><para>This function uses a buffer of the flash row size to perform the write operation. For the size of the row, refer to the specific PSoC device datasheet.</para><para>If the blocking write option is used, and write or erase operations are performed by CM4, the user's code on CM0P and CM4 is blocked until the operations are completed. If the operations are performed by CM0P, the user's code on CM4 is not blocked and the user code's on CM0P is blocked until the operation is completed. Plan your task allocation accordingly.</para><para>There are restrictions on using the read-while-write (RWW) feature for EEPROM emulation. There are also multiple constraints for blocking and nonblocking flash operations, relating to interrupts, Power modes, IPC usage, etc. Refer to the "Flash (Flash System Routine)" section of the PSoC 6 Peripheral Driver Library (psoc6pdl) API Reference Manual.<linebreak />
</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>addr</parametername>
</parameternamelist>
<parameterdescription>
<para>The logical start address in the Em_EEPROM storage to start writing data to.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>eepromData</parametername>
</parameternamelist>
<parameterdescription>
<para>Data to write to Em_EEPROM.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>size</parametername>
</parameternamelist>
<parameterdescription>
<para>The amount of data to write to Em_EEPROM in bytes.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the Em_EEPROM context structure <ref kindref="compound" refid="structcy__stc__eeprom__context__t">cy_stc_eeprom_context_t</ref>.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Returns the status of the operation <ref kindref="member" refid="group__group__em__eeprom__enums_1ga00cb0e6ed68329cd7aba18e3a93e13d2">cy_en_em_eeprom_status_t</ref> :<itemizedlist>
<listitem><para>CY_EM_EEPROM_SUCCESS - The operation is successfully completed.</para></listitem><listitem><para>CY_EM_EEPROM_BAD_CHECKSUM - One of the row the data read from contains bad checksum.</para></listitem><listitem><para>CY_EM_EEPROM_REDUNDANT_COPY_USED - Some data were read from redundant copy.</para></listitem><listitem><para>CY_EM_EEPROM_WRITE_FAIL - The write operation is failed.</para></listitem><listitem><para>CY_EM_EEPROM_BAD_PARAM - The function input parameter is invalid. </para></listitem></itemizedlist>
</para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="528" bodyfile="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-emeeprom/cy_em_eeprom.c" bodystart="507" column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-emeeprom/cy_em_eeprom.h" line="964" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__em__eeprom__functions_1gad61dbc3c01d2bbf96e0c0d5b6d2e0b61" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__em__eeprom__enums_1ga00cb0e6ed68329cd7aba18e3a93e13d2">cy_en_em_eeprom_status_t</ref></type>
        <definition>cy_en_em_eeprom_status_t Cy_Em_EEPROM_Erase</definition>
        <argsstring>(cy_stc_eeprom_context_t *context)</argsstring>
        <name>Cy_Em_EEPROM_Erase</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__eeprom__context__t">cy_stc_eeprom_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>This function erases the entire content of Em_EEPROM. </para>        </briefdescription>
        <detaileddescription>
<para>Erased values are all zeros.</para><para>In the <ref kindref="member" refid="index_1section_em_eeprom_mode_simple">Simple Mode</ref> the function just erases the entire content of Em_EEPROM.</para><para>When Simple Mode is disabled, the function first performs one write operation to keep the number of writes completed so far and then erases all rest content of Em_EEPROM memory. So, the <ref kindref="member" refid="group__group__em__eeprom__functions_1gad7fb39640526ce706a7867d955d2b275">Cy_Em_EEPROM_NumWrites()</ref> function returns the correct value. Refer to the <ref kindref="member" refid="group__group__em__eeprom__functions_1gaa792ff05d2b6ee71b44f1faeb2b68886">Cy_Em_EEPROM_Write()</ref> function for the side effect of the write operation. If the write operation is failed then corresponding status is returned and no erase operation is executed.</para><para>This function uses a buffer of the flash row size to perform the erase operation. For the size of the row, refer to the specific PSoC device datasheet.</para><para>This is a blocking function and it does not return until the erase operation is completed. The user's application program cannot enter Hibernate mode until the write is completed. The write operation is allowed in CPU Sleep and System Deep Sleep modes. Do not reset your device during the flash operation, including the XRES pin, a software reset, and watchdog reset sources. Also, configure low-voltage detect circuits to generate an interrupt instead of a reset. Otherwise, portions of flash may undergo unexpected changes.</para><para>If the blocking write option is used, and write or erase operations are performed by CM4, the user's code on CM0P and CM4 is blocked until the operations are completed. If the operations are performed by CM0P, the user's code on CM4 is not blocked and the user code's on CM0P is blocked until the operation is completed. Plan your task allocation accordingly.</para><para>There are restrictions on using the read-while-write (RWW) feature for EEPROM emulation. There are also multiple constraints for blocking and nonblocking flash operations, relating to interrupts, power mode, IPC usage, etc. Refer to the "Flash (Flash System Routine)" section of the PSoC 6 Peripheral Driver Library (psoc6pdl) API Reference Manual.<linebreak />
 Also, refer to the <ref kindref="member" refid="index_1section_em_eeprom_miscellaneous">Limitations and Restrictions</ref> section for the different Em_EEPROM middleware restrictions and limitations.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the Em_EEPROM context structure <ref kindref="compound" refid="structcy__stc__eeprom__context__t">cy_stc_eeprom_context_t</ref>.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Returns the status of the operation <ref kindref="member" refid="group__group__em__eeprom__enums_1ga00cb0e6ed68329cd7aba18e3a93e13d2">cy_en_em_eeprom_status_t</ref> :<itemizedlist>
<listitem><para>CY_EM_EEPROM_SUCCESS - The operation is successfully completed.</para></listitem><listitem><para>CY_EM_EEPROM_WRITE_FAIL - The either Write or Erase operation is failed. </para></listitem></itemizedlist>
</para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="845" bodyfile="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-emeeprom/cy_em_eeprom.c" bodystart="767" column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-emeeprom/cy_em_eeprom.h" line="970" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__em__eeprom__functions_1gad7fb39640526ce706a7867d955d2b275" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>uint32_t</type>
        <definition>uint32_t Cy_Em_EEPROM_NumWrites</definition>
        <argsstring>(cy_stc_eeprom_context_t *context)</argsstring>
        <name>Cy_Em_EEPROM_NumWrites</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__eeprom__context__t">cy_stc_eeprom_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Returns the number of the Em_EEPROM Writes completed so far. </para>        </briefdescription>
        <detaileddescription>
<para>This function returns zero in the <ref kindref="member" refid="index_1section_em_eeprom_mode_simple">Simple Mode</ref> since the number of writes is not available in this case.</para><para>Use this function to monitor the flash memory endurance. The higher the value, the less the flash memory endurance. Refer to the datasheet write endurance spec to determine the maximum number of write cycles. If the Wear Leveling feature is enabled, then the maximum of write cycles is increased by <ref kindref="member" refid="structcy__stc__eeprom__config__t_1a19b32e2183e33f98f02e15e2bce4f207">cy_stc_eeprom_config_t::wearLevelingFactor</ref>.</para><para>The returned number does not include the number of writes into the redundant copy if enabled.</para><para>This function does the following:<orderedlist>
<listitem><para>Returns the number of writes stored in the the last written row if the checksum of the row is correct.</para></listitem><listitem><para>If the checksum of the last written row is invalid and the redundant copy feature is enabled, then the row checksum of the redundant copy is verified for correctness and corresponding number of writes is returned.</para></listitem><listitem><para>If the checksum of the last written row is incorrect, then a special algorithm of searching the last written is executed and a corresponding number of writes is reported.</para></listitem><listitem><para>If the algorithm is unable to find the last written row (there is no row with the correct checksum), then zero is returned.</para></listitem></orderedlist>
</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the Em_EEPROM context structure <ref kindref="compound" refid="structcy__stc__eeprom__context__t">cy_stc_eeprom_context_t</ref>.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The number of writes performed to the Em_EEPROM. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="893" bodyfile="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-emeeprom/cy_em_eeprom.c" bodystart="886" column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-emeeprom/cy_em_eeprom.h" line="973" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>This section describes the Emulated EEPROM Function Prototypes. </para>    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>